<ul class="list-group" user-list>
    @php
        $key = 0;
        $maxWinner = 0
    @endphp
    @foreach($players as $player)
        @php $key ++ @endphp

            <li class="list-group-item p-4 @if($key<=$maxWinner) bg-primary text-white @endif">
                <div class="row align-items-center">
                    <div class="col-1 col-md-1 col-lg-1 text-center">
                        <span class="d-inline-block  @if($key<=$maxWinner) text-white @else text-primary @endif"><h4 class="d-inline">{{ $key }}</h4></span>
                    </div>
                    <div class="col">
                        <div class="row align-items-center">
                            <div class="col d-flex">
                                <span>
                                    <img src="{{ Avatar::create($player->users->name)->toBase64() }}" width="30px" class=" border rounded-pill border-white float-left" />
                                </span>
                                <div class="d-inline-block float-left col">
                                    <h5 class="d-block d-md-inline m-0">{{ $player->users->name }}</h5>
                                    <small class="ml-lg-3 ">{{ $player->users->email }}</small>
                                </div>
                            </div>
{{--                            <div class="col-md-3">--}}
{{--                                @if($key<=11)--}}
{{--                                    <p class="@if($key<=$maxWinner) text-white-80 @else text-primary @endif m-0"> {{ config('app.gifts')[$key-1] }} </p>--}}
{{--                                @endif--}}
{{--                            </div>--}}
                            <div class="col-md-1 text-md-right">
                                <span class="font-weight-bold @if($key<=$maxWinner) text-white @else text-primary @endif">
                                    <big>{{ $player->voteCount }}</big>
                                    @if($key<=$maxWinner) <i class="fas fa-trophy ml-2 text-warning"></i> @else <i class="fas fa-trophy ml-2 invisible"></i>  @endif
                                </span>
                            </div>
                        </div>
                    </div>

                </div>

            </li>
    @endforeach

</ul>
