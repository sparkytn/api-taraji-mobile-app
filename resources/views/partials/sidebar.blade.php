<!-- Sidebar -->
<div class="border-right bg-white " id="sidebar-wrapper">
    <div class="d-flex flex-column h-100">
        <div class="sidebar-heading text-center">
            <img src="{{ asset('images/10ans.png') }}" width="55px" class="mr-3" alt="">
            <h6 class="text-primary d-inline">{{ config('app.name') }}</h6>
        </div>
        <div class="list-group list-group-flush
">
            <a href="{{ route('home') }}" class="list-group-item list-group-item-action {{ (request()->is('accueil')) ? 'active' : '' }}"><i class="fas fa-home"> </i> Accueil</a>
            <a href="{{ route('game') }}" class="list-group-item list-group-item-action {{ (request()->is('jeu')) ? 'active' : '' }}"><i class="fas fa-gamepad"></i> Jeu de la semaine</a>
            <a href="{{ route('ranking') }}" class="list-group-item list-group-item-action {{ (request()->is('classement')) ? 'active' : '' }}"><i class="fas fa-gamepad"></i> Classement</a>
            <a class="list-group-item list-group-item-action bg-danger text-white" href="/logout">
                <i class="fas fa-sign-out-alt"></i> Déconnexion
            </a>
        </div>

    </div>

</div>
<!-- /#sidebar-wrapper -->
