<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- Scripts -->
    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        {{--FB_APP_ID   = '{{ env('FACEBOOK_APP_ID') }}';--}}
        APP_ENV     = '{{ app()->environment() }}';
        @auth
            BLADE = '{{ Auth::user()->id }}';
        @endauth
    </script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=El+Messiri:400,500,600,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="@yield('class')">

<!--  Loader -->
<div class="loader">
    <div class="spinner">
        <small>   Veuillez Patienter </small>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<!--  Loader -->
<div class="progress-wrapper hide">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 box">
                <h3 class="title">  Veuillez Patienter <br> <small>Vos données sont en cours d'envoi </small> </h3>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 7%;">
                        00%
                    </div>
                </div>
                <br>
                <div class="stats-box">2Mo envoyés sur 10 Mo</div>
            </div>
        </div>
    </div>
</div>


    <div id="app" class="d-flex">

        @auth
            @include('partials.sidebar')
        @endauth

        <main id="content-wrapper" class="d-flex flex-column">
            @include('partials.navbar')

            <div class="p-3 p-md-5 body-content">

                @yield('content')

            </div>
            <footer class="mt-auto p-2">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <a href="https://www.facebook.com/BanqueZitouna/" class="mx-2" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a>
                            <a href="https://www.instagram.com/banquezitouna/" class="mx-2" target="_blank"><i class="fab fa-instagram"></i> Instagram</a>
                            <a href="https://linkedin.com/company/zitouna-bank" class="mx-2" target="_blank"><i class="fab fa-linkedin"></i> LinkedIn</a>
                            <a href="https://www.youtube.com/channel/UCEs-CrPNuN0-M5Jd_8XRoBg" class="mx-2" target="_blank"><i class="fab fa-youtube"></i> Youtube</a>
                        </div>
                        <div class="col-md-6 text-muted text-md-right">
                            <a href="{{ asset('reglement.pdf') }}" target="_blank" class="mx-2">Règlement du jeu</a>
                            <span class="mx-3">
                                © 2020 Banque Zitouna
                            </span>
                            <img src="{{ asset('images/bz-logo-footer-2.png') }}" class="mx-2" height="40px" alt="">
                        </div>
                    </div>

                </div>
            </footer>
        </main>
    </div>
    <script src="{{ asset('js/app.js?s=2') }}" defer></script>
</body>
</html>
