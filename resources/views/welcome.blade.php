<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>taraji Mobile</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/styles/dark.min.css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">
            <div class="content">
                <div class="title m-b-md">
                    Hey there
                </div>


            </div>
        </div>
@php
    dd('
    Taraji Actif Filleul:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21690353183</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>0</responseCode>
                <responseMessage>OK</responseMessage>
                <isTaraji>1</isTaraji>
                <isFilleul>1</isFilleul>
                <activationDate>20230823</activationDate>
                <serviceFeeExpiryDate>20231123</serviceFeeExpiryDate>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>

    Taraji Grace:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21690335560</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>0</responseCode>
                <responseMessage>OK</responseMessage>
                <isTaraji>1</isTaraji>
                <isFilleul>1</isFilleul>
                <activationDate>20230823</activationDate>
                <serviceFeeExpiryDate>20230922</serviceFeeExpiryDate>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>

    Taraji Actif not Filleul:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21690361544</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>0</responseCode>
                <responseMessage>OK</responseMessage>
                <isTaraji>1</isTaraji>
                <isFilleul>0</isFilleul>
                <activationDate>20221021</activationDate>
                <serviceFeeExpiryDate>20251010</serviceFeeExpiryDate>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>

    Installed:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21698561788</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>0</responseCode>
                <responseMessage>OK</responseMessage>
                <isTaraji>0</isTaraji>
                <isFilleul>0</isFilleul>
                <activationDate/>
                <serviceFeeExpiryDate/>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>

    Not Found:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21690999999</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>102</responseCode>
                <responseMessage>Subscriber not found</responseMessage>
                <isTaraji>0</isTaraji>
                <isFilleul>0</isFilleul>
                <activationDate/>
                <serviceFeeExpiryDate/>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>

    Desactivé:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21692764789</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>126</responseCode>
                <responseMessage>Subscriber not activ</responseMessage>
                <isTaraji>0</isTaraji>
                <isFilleul>0</isFilleul>
                <activationDate/>
                <serviceFeeExpiryDate/>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>

    Technical error:

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:tar="http://services.taraji.tuntel.com/taraji/">
       <soapenv:Header>
          <oas:Security>
             <oas:UsernameToken>
                <oas:Username>taraji</oas:Username>
                <oas:Password>taraji_1909</oas:Password>
             </oas:UsernameToken>
          </oas:Security>
       </soapenv:Header>
       <soapenv:Body>
          <tar:getSubscriberDetailsRequest>
             <msisdn>21692764789</msisdn>
          </tar:getSubscriberDetailsRequest>
       </soapenv:Body>
    </soapenv:Envelope>

    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://services.taraji.tuntel.com/taraji/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
       <soap:Body>
          <tns:getSubscriberDetailsResponse>
             <result>
                <responseCode>999</responseCode>
                <responseMessage>Service temporairement indisponible, veuillez réessayer plus tard.</responseMessage>
             </result>
          </tns:getSubscriberDetailsResponse>
       </soap:Body>
    </soap:Envelope>'
    );
@endphp

                    </code>
                    </pre>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>