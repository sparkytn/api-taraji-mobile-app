@extends('admin.layouts.app')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
{{--        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--}}
    </div>

    <div class="row justify-content-center">

        <!-- Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Users</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$usersCount}}</div>
                                </div>
                                <div class="col">
                                   Utilisateurs
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Games</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $playes }}</div>
                                </div>
                                <div class="col">
                                    Jeux
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-file-image fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


    <div class="row">
        <div class="col-md-9">
            <h3 class="text-info mt-2 mb-4">Liste des Joueurs</h3>
        </div>
        <div class="col-md-3 text-right">
            <a href="{{ route('admin.resetScore') }}" class="btn btn-danger" onclick="return confirm('Etes-vous sûr de mettre a zéro les scores ? ');">Reset Socre</a>
            <a href="{{ route('admin.export') }}" class="btn btn-info" >Export CSV</a>
        </div>
    </div>

    <div class="row justify-content-center">

        <div class="card w-100">
            @foreach($users as $player)
                <li class="list-group-item p-4">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="row align-items-center">
                                <div class="col-md-9 d-flex">
                                    <span class=" mr-4 text-gray-500">
                                        {{ $player->rank }}
                                    </span>
                                    <span>
                                        <img src="{{ Avatar::create($player->name)->toBase64() }}" width="30px" class=" border rounded-pill border-white float-left" />
                                    </span>
                                    <div class="d-inline-block float-left col">
                                        <h5 class="d-block m-0">{{ $player->name }}</h5>
                                        <small class=""><i class="fas fa-phone"></i> {{ $player->phone  }} | <i class="fas fa-envelope"></i> {{ $player->email }} |  <i class="fas fa-calendar"></i> {{ $player->created_at  }}</small>
                                    </div>
                                </div>
                                <div class="col-md-3 text-right">
                                    <h2 class="text-primary">{{ $player->user_score }}</h2>
                                </div>

                            </div>
                        </div>

                    </div>

                </li>
            @endforeach
        </div>
        <div class="pagenation mt-5">
            {{ $users->links() }}
        </div>

    </div>


@endsection
