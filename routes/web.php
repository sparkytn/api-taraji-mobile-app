<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@home');
Route::get('/api-resp', 'HomeController@index');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('login', 'Auth\LoginController@showAdminLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@adminLogin');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::get('/users', 'AdminController@users')->name('users');
    Route::get('/reset-score', 'AdminController@resetScore')->name('resetScore');
    Route::get('/export', 'AdminController@export')->name('export');
});
