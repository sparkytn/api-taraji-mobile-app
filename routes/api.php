<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('check-registration', 'AuthController@checkRegistredUser');
Route::post('check-phone', 'AuthController@checkPhone');

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

Route::post('referral', 'ReferralController@store');

Route::post('save-photo', 'PhotoController@store');
Route::post('confirm-share', 'PhotoController@confirmShare');


Route::get('user', 'AuthController@getAuthUser');

Route::apiResource('play', 'GameController');


Route::get('soap', 'HomeController@soap_test');



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
