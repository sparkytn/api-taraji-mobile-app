<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
//        $this->call('AdminsTableSeeder');
        DB::table('admins')->insert([
            'name' => 'Super Admin',
            'username' => 'tttmadmin',
            'email' => 'tttmadmin@tt.tn',
            'password' => Hash::make('efTTr?@2020++')
        ]);
    }
}
