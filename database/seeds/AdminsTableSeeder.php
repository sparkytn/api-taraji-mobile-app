<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        Admin::create([
            'full_name' => 'Super Admin',
            'username' => 'admin',
            'email' => 'tttm-admin@tt.tn',
            'password' => bcrypt('efTTr?@2020++')
        ]);
    }
}
