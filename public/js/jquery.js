/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/webpack/buildin/amd-options.js":
/*!****************************************!*\
  !*** (webpack)/buildin/amd-options.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(this, {}))

/***/ }),

/***/ "./public/game-6/js/board.js":
/*!***********************************!*\
  !*** ./public/game-6/js/board.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var BOARD_CANVAS_CONTEXT = null;

function initBoard() {
  var canvas = document.getElementById('canvas-board');
  canvas.setAttribute('width', '550');
  canvas.setAttribute('height', '550');

  if (canvas.getContext) {
    BOARD_CANVAS_CONTEXT = canvas.getContext('2d');
  }
}

function getBoardCanevasContext() {
  return BOARD_CANVAS_CONTEXT;
}

function drawBoard(alternate) {
  var ctx = getBoardCanevasContext();

  if (alternate && alternate === true) {
    ctx.strokeStyle = "#fff";
  } else {
    ctx.strokeStyle = "#193fff";
  }

  ctx.lineWidth = "2";
  ctx.beginPath(); // UP 

  ctx.moveTo(0, 231);
  ctx.lineTo(97, 231);
  ctx.arcTo(99, 231, 99, 228, 5);
  ctx.lineTo(99, 179);
  ctx.arcTo(99, 176, 97, 176, 5);
  ctx.lineTo(12, 176);
  ctx.arcTo(1, 176, 1, 165, 12);
  ctx.lineTo(1, 10);
  ctx.arcTo(1, 1, 10, 1, 12);
  ctx.lineTo(536, 1);
  ctx.arcTo(547, 1, 547, 12, 12);
  ctx.lineTo(547, 165);
  ctx.arcTo(547, 176, 536, 176, 12);
  ctx.lineTo(452, 176);
  ctx.arcTo(449, 176, 449, 179, 5);
  ctx.lineTo(449, 228);
  ctx.arcTo(449, 231, 451, 231, 5);
  ctx.lineTo(550, 231);
  ctx.stroke();
  ctx.moveTo(0, 238);
  ctx.lineTo(98, 238);
  ctx.arcTo(106, 238, 106, 232, 12);
  ctx.lineTo(106, 179);
  ctx.arcTo(106, 169, 101, 169, 12);
  ctx.lineTo(15, 169);
  ctx.arcTo(9, 169, 9, 158, 5);
  ctx.lineTo(9, 15);
  ctx.arcTo(9, 7, 18, 7, 5);
  ctx.lineTo(259, 7);
  ctx.arcTo(266, 7, 266, 11, 5);
  ctx.lineTo(266, 73);
  ctx.arcTo(266, 78, 269, 78, 5);
  ctx.lineTo(280, 78);
  ctx.arcTo(283, 78, 283, 75, 5);
  ctx.lineTo(283, 15);
  ctx.arcTo(283, 7, 286, 7, 5);
  ctx.lineTo(533, 7);
  ctx.arcTo(540, 7, 540, 11, 5);
  ctx.lineTo(540, 165);
  ctx.arcTo(540, 169, 533, 169, 5);
  ctx.lineTo(452, 169);
  ctx.arcTo(442, 169, 442, 172, 12);
  ctx.lineTo(442, 230);
  ctx.arcTo(442, 238, 445, 238, 12);
  ctx.lineTo(550, 238);
  ctx.stroke(); // DOWN 

  ctx.moveTo(0, 283);
  ctx.lineTo(97, 283);
  ctx.arcTo(99, 283, 99, 286, 5);
  ctx.lineTo(99, 335);
  ctx.arcTo(99, 338, 96, 338, 5);
  ctx.lineTo(9, 338);
  ctx.arcTo(1, 338, 1, 341, 12);
  ctx.lineTo(1, 540);
  ctx.arcTo(1, 548, 9, 548, 12);
  ctx.lineTo(539, 548);
  ctx.arcTo(547, 548, 547, 540, 12);
  ctx.lineTo(547, 345);
  ctx.arcTo(547, 337, 539, 337, 12);
  ctx.lineTo(453, 337);
  ctx.arcTo(450, 337, 450, 334, 5);
  ctx.lineTo(450, 286);
  ctx.arcTo(450, 283, 453, 283, 5);
  ctx.lineTo(550, 283);
  ctx.stroke();
  ctx.moveTo(0, 276);
  ctx.lineTo(99, 276);
  ctx.arcTo(107, 276, 107, 282, 12);
  ctx.lineTo(107, 335);
  ctx.arcTo(107, 345, 101, 345, 12);
  ctx.lineTo(12, 345);
  ctx.arcTo(9, 345, 9, 348, 5);
  ctx.lineTo(9, 432);
  ctx.arcTo(9, 435, 12, 435, 5);
  ctx.lineTo(44, 435);
  ctx.arcTo(47, 435, 47, 438, 5);
  ctx.lineTo(47, 448);
  ctx.arcTo(47, 451, 44, 451, 5);
  ctx.lineTo(12, 451);
  ctx.arcTo(9, 451, 9, 454, 5);
  ctx.lineTo(9, 538);
  ctx.arcTo(9, 541, 12, 541, 5);
  ctx.lineTo(536, 541);
  ctx.arcTo(539, 541, 539, 538, 5);
  ctx.lineTo(539, 455);
  ctx.arcTo(539, 451, 536, 451, 5);
  ctx.lineTo(505, 451);
  ctx.arcTo(502, 451, 502, 448, 5);
  ctx.lineTo(502, 439);
  ctx.arcTo(502, 436, 505, 436, 5);
  ctx.lineTo(536, 436);
  ctx.arcTo(539, 433, 539, 430, 5);
  ctx.lineTo(539, 349);
  ctx.arcTo(539, 345, 536, 345, 5);
  ctx.lineTo(455, 345);
  ctx.arcTo(442, 345, 442, 342, 12);
  ctx.lineTo(442, 288);
  ctx.arcTo(442, 276, 448, 276, 12);
  ctx.lineTo(550, 276);
  ctx.stroke(); // LEFT

  ctx.roundRect(50, 45, 107, 79, 5);
  ctx.stroke();
  ctx.roundRect(50, 116, 107, 132, 5);
  ctx.stroke();
  ctx.roundRect(148, 45, 224, 79, 5);
  ctx.stroke();
  ctx.roundRect(148, 382, 224, 398, 5);
  ctx.stroke();
  ctx.roundRect(148, 276, 165, 345, 5);
  ctx.stroke();
  ctx.moveTo(56, 504);
  ctx.lineTo(220, 504);
  ctx.arcTo(223, 504, 223, 501, 5);
  ctx.lineTo(223, 492);
  ctx.arcTo(223, 489, 220, 489, 5);
  ctx.lineTo(168, 489);
  ctx.arcTo(165, 489, 165, 486, 5);
  ctx.lineTo(165, 439);
  ctx.arcTo(165, 436, 162, 436, 5);
  ctx.lineTo(152, 436);
  ctx.arcTo(149, 436, 149, 439, 5);
  ctx.lineTo(149, 486);
  ctx.arcTo(149, 489, 146, 489, 5);
  ctx.lineTo(54, 489);
  ctx.arcTo(51, 489, 51, 492, 5);
  ctx.lineTo(51, 501);
  ctx.arcTo(51, 504, 54, 504, 5);
  ctx.stroke();
  ctx.moveTo(55, 382);
  ctx.lineTo(104, 382);
  ctx.arcTo(107, 382, 107, 385, 5);
  ctx.lineTo(107, 447);
  ctx.arcTo(107, 450, 104, 450, 5);
  ctx.lineTo(93, 450);
  ctx.arcTo(90, 450, 90, 447, 5);
  ctx.lineTo(90, 401);
  ctx.arcTo(90, 398, 87, 398, 5);
  ctx.lineTo(55, 398);
  ctx.arcTo(52, 398, 52, 395, 5);
  ctx.lineTo(52, 385);
  ctx.arcTo(52, 382, 55, 382, 5);
  ctx.stroke();
  ctx.moveTo(148, 121);
  ctx.lineTo(148, 236);
  ctx.arcTo(148, 239, 151, 239, 5);
  ctx.lineTo(162, 239);
  ctx.arcTo(165, 239, 165, 236, 5);
  ctx.lineTo(165, 188);
  ctx.arcTo(165, 185, 168, 185, 5);
  ctx.lineTo(221, 185);
  ctx.arcTo(224, 185, 224, 182, 5);
  ctx.lineTo(224, 173);
  ctx.arcTo(224, 170, 221, 170, 5);
  ctx.lineTo(168, 170);
  ctx.arcTo(165, 170, 165, 169, 5);
  ctx.lineTo(165, 120);
  ctx.arcTo(165, 117, 162, 117, 5);
  ctx.lineTo(151, 117);
  ctx.arcTo(148, 117, 148, 120, 5);
  ctx.stroke(); // RIGHT

  ctx.roundRect(442, 45, 498, 79, 5);
  ctx.stroke();
  ctx.roundRect(442, 116, 498, 132, 5);
  ctx.stroke();
  ctx.roundRect(324, 45, 400, 79, 5);
  ctx.stroke();
  ctx.roundRect(324, 382, 400, 398, 5);
  ctx.stroke();
  ctx.roundRect(383, 276, 400, 345, 5);
  ctx.stroke();
  ctx.moveTo(330, 504);
  ctx.lineTo(494, 504);
  ctx.arcTo(497, 504, 497, 501, 5);
  ctx.lineTo(497, 492);
  ctx.arcTo(497, 489, 494, 489, 5);
  ctx.lineTo(403, 489);
  ctx.arcTo(400, 489, 400, 486, 5);
  ctx.lineTo(400, 441);
  ctx.arcTo(397, 436, 394, 436, 5);
  ctx.lineTo(386, 436);
  ctx.arcTo(383, 436, 383, 439, 5);
  ctx.lineTo(383, 486);
  ctx.arcTo(383, 489, 380, 489, 5);
  ctx.lineTo(328, 489);
  ctx.arcTo(325, 489, 325, 492, 5);
  ctx.lineTo(325, 500);
  ctx.arcTo(325, 504, 328, 504, 5);
  ctx.stroke();
  ctx.moveTo(495, 382);
  ctx.lineTo(445, 382);
  ctx.arcTo(442, 382, 442, 385, 5);
  ctx.lineTo(442, 447);
  ctx.arcTo(442, 450, 445, 450, 5);
  ctx.lineTo(456, 450);
  ctx.arcTo(459, 450, 459, 447, 5);
  ctx.lineTo(459, 401);
  ctx.arcTo(459, 398, 462, 398, 5);
  ctx.lineTo(495, 398);
  ctx.arcTo(498, 398, 498, 395, 5);
  ctx.lineTo(498, 385);
  ctx.arcTo(498, 382, 495, 382, 5);
  ctx.stroke();
  ctx.moveTo(400, 121);
  ctx.lineTo(400, 236);
  ctx.arcTo(400, 239, 397, 239, 5);
  ctx.lineTo(386, 239);
  ctx.arcTo(383, 239, 383, 236, 5);
  ctx.lineTo(383, 188);
  ctx.arcTo(383, 185, 380, 185, 5);
  ctx.lineTo(328, 185);
  ctx.arcTo(325, 185, 325, 182, 5);
  ctx.lineTo(325, 173);
  ctx.arcTo(325, 170, 328, 170, 5);
  ctx.lineTo(380, 170);
  ctx.arcTo(383, 170, 383, 169, 5);
  ctx.lineTo(383, 120);
  ctx.arcTo(383, 117, 385, 117, 5);
  ctx.lineTo(397, 117);
  ctx.arcTo(400, 117, 400, 120, 5);
  ctx.stroke(); // CENTER

  ctx.moveTo(212, 117);
  ctx.lineTo(338, 117);
  ctx.arcTo(341, 120, 341, 123, 5);
  ctx.lineTo(341, 129);
  ctx.arcTo(341, 132, 338, 132, 5);
  ctx.lineTo(286, 132);
  ctx.arcTo(283, 132, 283, 135, 5);
  ctx.lineTo(283, 182);
  ctx.arcTo(283, 185, 280, 185, 5);
  ctx.lineTo(269, 185);
  ctx.arcTo(266, 185, 266, 182, 5);
  ctx.lineTo(266, 135);
  ctx.arcTo(266, 132, 262, 132, 5);
  ctx.lineTo(211, 132);
  ctx.arcTo(208, 132, 208, 129, 5);
  ctx.lineTo(208, 120);
  ctx.arcTo(208, 117, 211, 117, 5);
  ctx.stroke();
  ctx.moveTo(212, 329);
  ctx.lineTo(338, 329);
  ctx.arcTo(341, 332, 341, 335, 5);
  ctx.lineTo(341, 341);
  ctx.arcTo(341, 344, 338, 344, 5);
  ctx.lineTo(286, 344);
  ctx.arcTo(283, 344, 283, 347, 5);
  ctx.lineTo(283, 394);
  ctx.arcTo(283, 397, 280, 397, 5);
  ctx.lineTo(269, 397);
  ctx.arcTo(266, 397, 266, 394, 5);
  ctx.lineTo(266, 347);
  ctx.arcTo(266, 344, 262, 344, 5);
  ctx.lineTo(211, 344);
  ctx.arcTo(208, 344, 208, 129, 5);
  ctx.lineTo(208, 332);
  ctx.arcTo(208, 329, 211, 329, 5);
  ctx.stroke();
  ctx.moveTo(212, 436);
  ctx.lineTo(338, 436);
  ctx.arcTo(341, 439, 341, 442, 5);
  ctx.lineTo(341, 448);
  ctx.arcTo(341, 451, 338, 451, 5);
  ctx.lineTo(286, 451);
  ctx.arcTo(283, 451, 283, 454, 5);
  ctx.lineTo(283, 501);
  ctx.arcTo(283, 503, 280, 503, 5);
  ctx.lineTo(269, 503);
  ctx.arcTo(266, 503, 266, 501, 5);
  ctx.lineTo(266, 454);
  ctx.arcTo(266, 451, 262, 451, 5);
  ctx.lineTo(211, 451);
  ctx.arcTo(208, 451, 208, 236, 5);
  ctx.lineTo(208, 439);
  ctx.arcTo(208, 436, 211, 436, 5);
  ctx.stroke();
  ctx.moveTo(254, 223);
  ctx.lineTo(207, 223);
  ctx.lineTo(207, 292);
  ctx.lineTo(342, 292);
  ctx.lineTo(342, 223);
  ctx.lineTo(296, 223);
  ctx.lineTo(296, 230);
  ctx.lineTo(334, 230);
  ctx.lineTo(334, 284);
  ctx.lineTo(214, 284);
  ctx.lineTo(214, 230);
  ctx.lineTo(254, 230);
  ctx.lineTo(254, 223);
  ctx.stroke();
  ctx.closePath();
}

function drawBoardDoor() {
  var ctx = getBoardCanevasContext();
  ctx.strokeStyle = "white";
  ctx.lineWidth = "5";
  ctx.beginPath();
  ctx.moveTo(255, 226);
  ctx.lineTo(295, 226);
  ctx.stroke();
  ctx.closePath();
}

function eraseBoardDoor() {
  var ctx = getBoardCanevasContext(); //ctx.translate(FRUITS_POSITION_X - (FRUITS_SIZE / 2), FRUITS_POSITION_Y - (FRUITS_SIZE / 2));
  //ctx.save();
  //ctx.globalCompositeOperation = "destination-out";
  //ctx.beginPath();
  //ctx.translate(FRUITS_POSITION_X - (FRUITS_SIZE / 2), FRUITS_POSITION_Y - (FRUITS_SIZE / 2));

  ctx.clearRect(255, 220, 40, 10); //ctx.fill();
  //ctx.closePath();
  //ctx.restore();
}

/***/ }),

/***/ "./public/game-6/js/bubbles.js":
/*!*************************************!*\
  !*** ./public/game-6/js/bubbles.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var BUBBLES_ARRAY = new Array();
var BUBBLES_CANVAS_CONTEXT = null;
var BUBBLES_X_START = 30;
var BUBBLES_X_END = 518;
var BUBBLES_GAP = (BUBBLES_X_END - BUBBLES_X_START) / 25;
var BUBBLES_Y_START = 26;
var BUBBLES_Y_END = 522;
var BUBBLES_SIZE = 3;
var BUBBLES_COUNTER = 0;
var SUPER_BUBBLES = [];
var SUPER_BUBBLES_SIZE = 8;
var SUPER_BUBBLES_BLINK = false;
var SUPER_BUBBLES_BLINK_STATE = 0;
var SUPER_BUBBLES_BLINK_TIMER = -1;
var SUPER_BUBBLES_BLINK_SPEED = 525;

function initBubbles() {
  BUBBLES_COUNTER = 0;
  BUBBLES_ARRAY.length = 0;
  var canvas = document.getElementById('canvas-bubbles');
  canvas.setAttribute('width', '550');
  canvas.setAttribute('height', '550');

  if (canvas.getContext) {
    BUBBLES_CANVAS_CONTEXT = canvas.getContext('2d');
  }
}

function getBubblesCanevasContext() {
  return BUBBLES_CANVAS_CONTEXT;
}

function drawBubbles() {
  var ctx = getBubblesCanevasContext();
  ctx.fillStyle = "#dca5be";

  for (var line = 1, linemax = 29, i = 0, s = 0; line <= linemax; line++) {
    var y = getYFromLine(line);

    for (var x = BUBBLES_X_START, xmax = BUBBLES_X_END, bubble = 1; x < xmax; bubble++, x += BUBBLES_GAP) {
      if (canAddBubble(line, bubble)) {
        var type = "";
        var size = "";

        if (isSuperBubble(line, bubble)) {
          type = "s";
          size = SUPER_BUBBLES_SIZE;
          SUPER_BUBBLES[s] = line + ";" + bubble + ";" + parseInt(correctionX(x, bubble)) + "," + parseInt(y) + ";0";
          s++;
        } else {
          type = "b";
          size = BUBBLES_SIZE;
        }

        BUBBLES_COUNTER++;
        ctx.beginPath();
        ctx.arc(correctionX(x, bubble), y, size, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.closePath();
        BUBBLES_ARRAY.push(parseInt(correctionX(x, bubble)) + "," + parseInt(y) + ";" + line + ";" + bubble + ";" + type + ";0");
        i++;
      }
    }
  }
}

function stopBlinkSuperBubbles() {
  clearInterval(SUPER_BUBBLES_BLINK_TIMER);
  SUPER_BUBBLES_BLINK_TIMER = -1;
  SUPER_BUBBLES_BLINK = false;
}

function blinkSuperBubbles() {
  if (SUPER_BUBBLES_BLINK === false) {
    SUPER_BUBBLES_BLINK = true;
    SUPER_BUBBLES_BLINK_TIMER = setInterval('blinkSuperBubbles()', SUPER_BUBBLES_BLINK_SPEED);
  } else {
    if (SUPER_BUBBLES_BLINK_STATE === 0) {
      SUPER_BUBBLES_BLINK_STATE = 1;
    } else {
      SUPER_BUBBLES_BLINK_STATE = 0;
    }

    var clone = SUPER_BUBBLES.slice(0);

    for (var i = 0, imax = clone.length; i < imax; i++) {
      var s = clone[i];

      if (s.split(";")[3] === "0") {
        var sx = parseInt(s.split(";")[2].split(",")[0]);
        var sy = parseInt(s.split(";")[2].split(",")[1]);

        if (SUPER_BUBBLES_BLINK_STATE === 1) {
          eraseBubble("s", sx, sy);
        } else {
          var ctx = getBubblesCanevasContext();
          ctx.fillStyle = "#dca5be";
          ctx.beginPath();
          ctx.arc(sx, sy, SUPER_BUBBLES_SIZE, 0, 2 * Math.PI, false);
          ctx.fill();
          ctx.closePath();
        }
      }
    }
  }
}

function setSuperBubbleOnXY(x, y, eat) {
  for (var i = 0, imax = SUPER_BUBBLES.length; i < imax; i++) {
    var bubbleParams = SUPER_BUBBLES[i].split(";");
    var testX = parseInt(bubbleParams[2].split(",")[0]);
    var testY = parseInt(bubbleParams[2].split(",")[1]);

    if (testX === x && testY === y) {
      SUPER_BUBBLES[i] = SUPER_BUBBLES[i].substr(0, SUPER_BUBBLES[i].length - 1) + "1";
      break;
    }
  }
}

function getBubbleOnXY(x, y) {
  var bubble = null;

  for (var i = 0, imax = BUBBLES_ARRAY.length; i < imax; i++) {
    var bubbleParams = BUBBLES_ARRAY[i].split(";");
    var testX = parseInt(bubbleParams[0].split(",")[0]);
    var testY = parseInt(bubbleParams[0].split(",")[1]);

    if (testX === x && testY === y) {
      bubble = BUBBLES_ARRAY[i];
      break;
    }
  }

  return bubble;
}

function eraseBubble(t, x, y) {
  var ctx = getBubblesCanevasContext();
  var size = "";

  if (t === "s") {
    size = SUPER_BUBBLES_SIZE;
  } else {
    size = BUBBLES_SIZE;
  }

  ctx.clearRect(x - size, y - size, (size + 1) * 2, (size + 1) * 2);
}

function isSuperBubble(line, bubble) {
  if ((line === 23 || line === 4) && (bubble === 1 || bubble === 26)) {
    return true;
  }

  return false;
}

function canAddBubble(line, bubble) {
  if ((line >= 1 && line <= 4 || line >= 9 && line <= 10 || line >= 20 && line <= 22 || line >= 26 && line <= 28) && (bubble === 13 || bubble === 14)) {
    return false;
  } else if ((line >= 2 && line <= 4 || line >= 21 && line <= 22) && (bubble >= 2 && bubble <= 5 || bubble >= 7 && bubble <= 11 || bubble >= 16 && bubble <= 20 || bubble >= 22 && bubble <= 25)) {
    return false;
  } else if (line >= 6 && line <= 7 && (bubble >= 2 && bubble <= 5 || bubble >= 7 && bubble <= 8 || bubble >= 10 && bubble <= 17 || bubble >= 19 && bubble <= 20 || bubble >= 22 && bubble <= 25)) {
    return false;
  } else if (line === 8 && (bubble >= 7 && bubble <= 8 || bubble >= 13 && bubble <= 14 || bubble >= 19 && bubble <= 20)) {
    return false;
  } else if (line >= 9 && line <= 19 && (bubble >= 1 && bubble <= 5 || bubble >= 22 && bubble <= 26)) {
    return false;
  } else if ((line === 11 || line === 17) && bubble >= 7 && bubble <= 20) {
    return false;
  } else if ((line === 9 || line === 10) && (bubble === 12 || bubble === 15)) {
    return false;
  } else if ((line >= 12 && line <= 13 || line >= 15 && line <= 16) && (bubble === 9 || bubble === 18)) {
    return false;
  } else if (line === 14 && (bubble >= 7 && bubble <= 9 || bubble >= 18 && bubble <= 20)) {
    return false;
  } else if ((line === 18 || line === 19) && (bubble === 9 || bubble === 18)) {
    return false;
  } else if (line >= 9 && line <= 10 && (bubble >= 7 && bubble <= 11 || bubble >= 16 && bubble <= 20)) {
    return false;
  } else if ((line >= 11 && line <= 13 || line >= 15 && line <= 19) && (bubble >= 7 && bubble <= 8 || bubble >= 19 && bubble <= 20)) {
    return false;
  } else if ((line >= 12 && line <= 16 || line >= 18 && line <= 19) && bubble >= 10 && bubble <= 17) {
    return false;
  } else if (line === 23 && (bubble >= 4 && bubble <= 5 || bubble >= 22 && bubble <= 23)) {
    return false;
  } else if (line >= 24 && line <= 25 && (bubble >= 1 && bubble <= 2 || bubble >= 4 && bubble <= 5 || bubble >= 7 && bubble <= 8 || bubble >= 10 && bubble <= 17 || bubble >= 19 && bubble <= 20 || bubble >= 22 && bubble <= 23 || bubble >= 25 && bubble <= 26)) {
    return false;
  } else if (line === 26 && (bubble >= 7 && bubble <= 8 || bubble >= 19 && bubble <= 20)) {
    return false;
  } else if (line >= 27 && line <= 28 && (bubble >= 2 && bubble <= 11 || bubble >= 16 && bubble <= 25)) {
    return false;
  }

  return true;
}

function correctionX(x, bubble) {
  if (bubble === 3) {
    return x + 1;
  } else if (bubble === 6) {
    return x + 1;
  } else if (bubble === 15) {
    return x + 1;
  } else if (bubble === 18) {
    return x + 1;
  } else if (bubble === 21) {
    return x + 2;
  } else if (bubble === 24) {
    return x + 2;
  } else if (bubble === 26) {
    return x + 1;
  }

  return x;
}

function getYFromLine(line) {
  var y = BUBBLES_Y_START;

  if (line < 8) {
    y = BUBBLES_Y_START + (line - 1) * 18;
  } else if (line > 7 && line < 15) {
    y = 150 + (line - 8) * 18;
  } else if (line > 14 && line < 21) {
    y = 256 + (line - 14) * 18;
  } else if (line > 20 && line < 26) {
    y = 362 + (line - 20) * 18;
  } else if (line > 25 && line < 29) {
    y = 452 + (line - 25) * 18;
  } else if (line === 29) {
    y = BUBBLES_Y_END;
  }

  return y;
}

/***/ }),

/***/ "./public/game-6/js/fruits.js":
/*!************************************!*\
  !*** ./public/game-6/js/fruits.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var FRUITS_CANVAS_CONTEXT = null;
var LEVEL_FRUITS_CANVAS_CONTEXT = null;
var FRUITS_SIZE = 30;
var FRUITS_POSITION_X = 276;
var FRUITS_POSITION_Y = 310;
var FRUIT_MINIMUM_START = 15;
var FRUIT_CANCEL_TIMER = null;
var FRUIT_CANCEL_SPEED = 7500;
var FRUIT = null;

function initFruits() {
  var canvas = document.getElementById('canvas-fruits');
  canvas.setAttribute('width', '550');
  canvas.setAttribute('height', '550');

  if (canvas.getContext) {
    FRUITS_CANVAS_CONTEXT = canvas.getContext('2d');
  }

  var levelCanvas = document.getElementById('canvas-level-fruits');
  levelCanvas.setAttribute('width', '265');
  levelCanvas.setAttribute('height', '30');

  if (levelCanvas.getContext) {
    LEVEL_FRUITS_CANVAS_CONTEXT = levelCanvas.getContext('2d');
  }

  var ctx = getLevelFruitsCanevasContext();
  ctx.clearRect(0, 0, 265, 30);
  var x = 245;
  var y = 14;
  var i = 0;

  if (LEVEL > 7) {
    var l = LEVEL;
    if (l > 13) l = 13;
    i = -(l - 7);
  }

  drawFruit(ctx, "cherry", x - i * 37, y, 27);
  i++;

  if (LEVEL > 1) {
    drawFruit(ctx, "strawberry", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 2) {
    drawFruit(ctx, "orange", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 3) {
    drawFruit(ctx, "orange", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 4) {
    drawFruit(ctx, "apple", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 5) {
    drawFruit(ctx, "apple", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 6) {
    drawFruit(ctx, "melon", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 7) {
    drawFruit(ctx, "melon", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 8) {
    drawFruit(ctx, "galboss", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 9) {
    drawFruit(ctx, "galboss", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 10) {
    drawFruit(ctx, "bell", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 11) {
    drawFruit(ctx, "bell", x - i * 37, y, 27);
    i++;
  }

  if (LEVEL > 12) {
    drawFruit(ctx, "key", x - i * 37, y, 27);
    i++;
  }
}

function getFruitsCanevasContext() {
  return FRUITS_CANVAS_CONTEXT;
}

function getLevelFruitsCanevasContext() {
  return LEVEL_FRUITS_CANVAS_CONTEXT;
}

function eatFruit() {
  playEatFruitSound();
  var s = 0;
  if (FRUIT === "cherry") s = 100;else if (FRUIT === "strawberry") s = 300;else if (FRUIT === "orange") s = 500;else if (FRUIT === "apple") s = 700;else if (FRUIT === "melon") s = 1000;else if (FRUIT === "galboss") s = 2000;else if (FRUIT === "bell") s = 3000;else if (FRUIT === "key") s = 5000;
  score(s, "fruit");
  cancelFruit();
}

function fruit() {
  if (TIME_FRUITS === 2 && $("#board .fruits").length > 0) {
    $("#board .fruits").remove();
  }

  if (TIME_FRUITS > FRUIT_MINIMUM_START) {
    if (anyGoodIdea() > 3) {
      oneFruit();
    }
  }
}

function oneFruit() {
  if (FRUIT_CANCEL_TIMER === null) {
    var ctx = getFruitsCanevasContext();
    if (LEVEL === 1) FRUIT = "cherry";else if (LEVEL === 2) FRUIT = "strawberry";else if (LEVEL === 3 || LEVEL === 4) FRUIT = "orange";else if (LEVEL === 5 || LEVEL === 6) FRUIT = "apple";else if (LEVEL === 7 || LEVEL === 8) FRUIT = "melon";else if (LEVEL === 9 || LEVEL === 10) FRUIT = "galboss";else if (LEVEL === 11 || LEVEL === 12) FRUIT = "bell";else if (LEVEL === 13) FRUIT = "key";
    drawFruit(ctx, FRUIT, FRUITS_POSITION_X, FRUITS_POSITION_Y, FRUITS_SIZE);
    FRUIT_CANCEL_TIMER = new Timer("cancelFruit()", FRUIT_CANCEL_SPEED);
  }
}

function cancelFruit() {
  eraseFruit();
  FRUIT_CANCEL_TIMER.cancel();
  FRUIT_CANCEL_TIMER = null;
  TIME_FRUITS = 0;
}

function eraseFruit() {
  var ctx = getFruitsCanevasContext(); //ctx.translate(FRUITS_POSITION_X - (FRUITS_SIZE / 2), FRUITS_POSITION_Y - (FRUITS_SIZE / 2));
  //ctx.save();
  //ctx.globalCompositeOperation = "destination-out";
  //ctx.beginPath();
  //ctx.translate(FRUITS_POSITION_X - (FRUITS_SIZE / 2), FRUITS_POSITION_Y - (FRUITS_SIZE / 2));

  ctx.clearRect(FRUITS_POSITION_X - FRUITS_SIZE, FRUITS_POSITION_Y - FRUITS_SIZE, FRUITS_SIZE * 2, FRUITS_SIZE * 2); //ctx.fill();
  //ctx.closePath();
  //ctx.restore();
}

function drawFruit(ctx, f, x, y, size) {
  ctx.save();
  if (f === "cherry") drawCherry(ctx, x, y, size);else if (f === "strawberry") drawStrawberry(ctx, x, y, size);else if (f === "orange") drawOrange(ctx, x, y, size);else if (f === "apple") drawApple(ctx, x, y, size);else if (f === "melon") drawMelon(ctx, x, y + 7, size / 1.6);else if (f === "galboss") drawGalboss(ctx, x, y, size);else if (f === "bell") drawBell(ctx, x, y, size);else if (f === "key") drawKey(ctx, x, y, size);
  ctx.restore();
}

function drawKey(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2);
  ctx.fillStyle = "#52c4cc";
  ctx.beginPath();
  ctx.rect(size / 3, 5, size - size / 3, size / 3);
  ctx.rect(size / 6 * 3, 2, size - size / 3 * 2, size / (size / 3));
  ctx.fill();
  ctx.fillStyle = "#000";
  ctx.beginPath();
  ctx.rect(size / 6 * 3, size / 6, size - size / 3 * 2, size / 10);
  ctx.fill();
  ctx.strokeStyle = "#ccc";
  ctx.lineWidth = "3";
  ctx.beginPath();
  ctx.moveTo(size / 2 + 2, size - 4);
  ctx.lineTo(size / 2 + 2, size / 2);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(size / 2 + 7, size - 4);
  ctx.lineTo(size / 2 + 7, size / 2);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(size / 2 + 4, size - 4);
  ctx.lineTo(size / 2 + 4, size - 1);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(size / 2 + 5, size - 4);
  ctx.lineTo(size / 2 + 5, size - 1);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(size / 2 + 9, size / 2 + 2);
  ctx.lineTo(size / 2 + 9, size / 2 + 5);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(size / 2 + 10, size / 2 + 8);
  ctx.lineTo(size / 2 + 10, size / 2 + 11);
  ctx.stroke();
  ctx.closePath();
}

function drawBell(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2);
  ctx.oval(size / 2, size / 2, size / 1, size - 5);
  ctx.fillStyle = "#fff200";
  ctx.fill();
  ctx.beginPath();
  ctx.rect(4, size - size / 2.5 - 3, size - 8, size / 2.5 - 1);
  ctx.fill();
  ctx.fillStyle = "#52c4cc";
  ctx.beginPath();
  ctx.rect(4 + 2, size - 6, size - 12, 5);
  ctx.fill();
  ctx.fillStyle = "#8c8c8c";
  ctx.beginPath();
  ctx.rect(size / 2, size - 6, 5, 5);
  ctx.fill();
  ctx.closePath();
  ctx.strokeStyle = "#bbb";
  ctx.lineWidth = "2";
  ctx.beginPath();
  ctx.moveTo(15, 7);
  ctx.arcTo(8, 7, 8, 30, 9);
  ctx.stroke();
  ctx.closePath();
}

function drawGalboss(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2 + 1);
  ctx.strokeStyle = "#868df5";
  ctx.lineWidth = "5";
  ctx.beginPath();
  ctx.moveTo(size / 2, size / 2 + size / 4);
  ctx.arcTo(size - 1, size / 2 + 4, size - 1, size / 2 + 1, size / 3);
  ctx.lineTo(size - 1, 4);
  ctx.stroke();
  ctx.closePath();
  ctx.beginPath();
  ctx.moveTo(size / 2, size / 2 + size / 4);
  ctx.arcTo(1, size / 2 + 4, 1, size / 2 + 1, size / 3);
  ctx.lineTo(1, 4);
  ctx.stroke();
  ctx.closePath();
  ctx.strokeStyle = "#ffff00";
  ctx.lineWidth = "6";
  ctx.beginPath();
  ctx.moveTo(size / 2, size / 2 - 2);
  ctx.lineTo(size / 2, size);
  ctx.stroke();
  ctx.fillStyle = "#ffff00";
  ctx.beginPath();
  ctx.arc(size / 2, size / 3.5, size / 2.5, 0, Math.PI * 1);
  ctx.fill();
  ctx.closePath();
  ctx.strokeStyle = "#000";
  ctx.lineWidth = "3";
  ctx.beginPath();
  ctx.moveTo(size / 2 - size / 6, size / 2 + 1);
  ctx.lineTo(size / 2 - size / 6, size);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(size / 2 + size / 6, size / 2 + 1);
  ctx.lineTo(size / 2 + size / 6, size);
  ctx.stroke();
  ctx.strokeStyle = "#ff3f3f";
  ctx.lineWidth = "4";
  ctx.beginPath();
  ctx.moveTo(size / 2, size / 2);
  ctx.lineTo(size / 2, 2);
  ctx.stroke();
  ctx.moveTo(size / 2 + 1, 2);
  ctx.lineTo(size / 2 - 8, size / 2 - size / 6);
  ctx.stroke();
  ctx.moveTo(size / 2 - 1, 2);
  ctx.lineTo(size / 2 + 8, size / 2 - size / 6);
  ctx.stroke();
  ctx.closePath();
}

function drawMelon(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2);
  ctx.fillStyle = "#198122";
  ctx.beginPath();
  ctx.moveTo(size / 2, size / 6);
  ctx.arc(size / 2, size / 6, size / 1.15, 1.1, 2.5, true);
  ctx.fill();
  ctx.closePath();
  ctx.beginPath();
  ctx.fillStyle = "#ACFB77";
  ctx.moveTo(size / 2, size / 6);
  ctx.arc(size / 2, size / 6, size / 1.3, 1.1, 2.5, true);
  ctx.fill();
  ctx.closePath();
  ctx.beginPath();
  ctx.fillStyle = "#F92F2F";
  ctx.moveTo(size / 2, size / 6);
  ctx.arc(size / 2, size / 6, size / 1.7, 1.1, 2.5, true);
  ctx.fill();
  ctx.closePath();
  var mod = size / 23;
  ctx.beginPath();
  ctx.fillStyle = "black";
  ctx.moveTo(12 * mod, 9 * mod);
  ctx.arc(12 * mod, 9 * mod, size / 12, 1.1, 2.5, true);
  ctx.moveTo(13 * mod, 12 * mod);
  ctx.arc(13 * mod, 12 * mod, size / 12, 1.1, 2.5, true);
  ctx.moveTo(10.5 * mod, 12 * mod);
  ctx.arc(10.5 * mod, 12 * mod, size / 12, 1.1, 2.5, true);
  ctx.fill();
  ctx.closePath();
}

function drawApple(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2 - 2);
  ctx.fillStyle = "#ff0000";
  ctx.beginPath();
  ctx.arc(size / 2, size / 2 + size / 9, size / 2.1, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  ctx.fillStyle = "#ff0000";
  ctx.beginPath();
  ctx.arc(9, size - 3, size / 4.5, Math.PI * 2, -Math.PI * 2, true);
  ctx.arc(size - 8, size - 3, size / 4.5, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  ctx.fillStyle = "black";
  ctx.beginPath();
  ctx.arc(size / 2, size / 6, size / 7, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  var mod = size / 23;
  ctx.strokeStyle = "#24da1c";
  ctx.lineWidth = 2;
  ctx.beginPath();
  ctx.beginPath();
  ctx.moveTo(13 * mod + 2, size / 9 + 4);
  ctx.lineTo(13 * mod - size / 4, size / 9 + 1);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(13 * mod + 2, size / 9 + 4);
  ctx.lineTo(13 * mod - size / 2.5, size / 9 + 3);
  ctx.stroke();
  ctx.strokeStyle = "#bbb";
  ctx.lineWidth = "2";
  ctx.beginPath();
  ctx.moveTo(12, 11);
  ctx.arcTo(5, 11, 5, 30, 7);
  ctx.stroke();
  ctx.closePath();
}

function drawOrange(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2 - 1);
  ctx.fillStyle = "#fcb424";
  ctx.beginPath();
  ctx.arc(size / 2, size / 2 + size / 9, size / 2.2, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  ctx.fillStyle = "black";
  ctx.beginPath();
  ctx.arc(size / 2, size / 6, size / 7, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  var mod = size / 23;
  ctx.strokeStyle = "#24da1c";
  ctx.lineWidth = 2.5;
  ctx.beginPath();
  ctx.moveTo(size / 2, size / 3);
  ctx.lineTo(size / 2, size / 8);
  ctx.lineTo(9 * mod, size / 9);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(9 * mod, size / 9);
  ctx.lineTo(9 * mod + size / 3, size / 9 - 2);
  ctx.stroke();
  ctx.closePath();
}

function drawStrawberry(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2 + 2);
  ctx.beginPath();
  ctx.fillStyle = "#ff0000";
  ctx.moveTo(size / 2, size - size / 18);
  ctx.bezierCurveTo(0, size / 1.3, 0, -size / 9, size / 2, size / 6);
  ctx.moveTo(size / 2, size - size / 18);
  ctx.bezierCurveTo(size, size / 1.3, size, -size / 9, size / 2, size / 6);
  ctx.fill();
  ctx.closePath();
  ctx.fillStyle = "white";
  ctx.fillRect(size / 4, size / 3, size / 18, size / 16);
  ctx.fillRect(size / 2, size / 4, size / 18, size / 16);
  ctx.fillRect(size - size / 3.5, size / 2.4, size / 18, size / 16);
  ctx.fillRect(size - size / 2.2, size / 2, size / 18, size / 16);
  ctx.fillRect(size / 2.6, size / 1.3, size / 18, size / 16);
  ctx.fillRect(size / 3, size / 1.8, size / 18, size / 16);
  ctx.fillRect(size / 1.6, size / 1.4, size / 18, size / 16);
  ctx.beginPath();
  ctx.fillStyle = "#24DA1D";
  var mod = size / 23;
  ctx.moveTo(6 * mod, 2 * mod);
  ctx.lineTo(1 * mod, 8 * mod);
  ctx.lineTo(6 * mod, 6 * mod);
  ctx.lineTo(11 * mod, 11 * mod);
  ctx.lineTo(16 * mod, 6 * mod);
  ctx.lineTo(21 * mod, 8 * mod);
  ctx.lineTo(17 * mod, 2 * mod);
  ctx.moveTo(size / 2, 2 * mod);
  ctx.lineTo(8 * mod, 0 * mod);
  ctx.lineTo(15 * mod, 0 * mod);
  ctx.lineTo(size / 2, 2 * mod);
  ctx.fill();
  ctx.closePath();
}

function drawCherry(ctx, x, y, size) {
  ctx.translate(x - size / 2, y - size / 2 + 1);
  ctx.beginPath();
  ctx.fillStyle = "#ff0000";
  ctx.arc(size / 8, size - size / 2.8, size / 4, Math.PI * 2, -Math.PI * 2, true);
  ctx.arc(size - size / 3, size - size / 4, size / 4, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  ctx.beginPath();
  ctx.fillStyle = "#670303";
  ctx.arc(size / 7.2, size - size / 2.25, size / 14, Math.PI * 2, -Math.PI * 2, true);
  ctx.arc(size - size / 3, size - size / 3, size / 14, Math.PI * 2, -Math.PI * 2, true);
  ctx.fill();
  ctx.closePath();
  ctx.beginPath();
  ctx.strokeStyle = "#959817";
  ctx.lineWidth = 2;
  ctx.moveTo(size / 8, size - size / 2);
  ctx.bezierCurveTo(size / 6, size / 1.5, size / 7, size / 4, size - size / 4, size / 8);
  ctx.moveTo(size - size / 2.5, size - size / 3);
  ctx.bezierCurveTo(size / 1.3, size / 1.5, size / 3, size / 2.5, size - size / 4, size / 8);
  ctx.stroke();
  ctx.closePath();
  ctx.fillStyle = "#959817";
  ctx.fillRect(size - size / 3, size / 12, size / 9, size / 9);
  ctx.closePath();
}

/***/ }),

/***/ "./public/game-6/js/game.js":
/*!**********************************!*\
  !*** ./public/game-6/js/game.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var KEYDOWN = false;
var PAUSE = false;
var LOCK = false;
var HIGHSCORE = 0;
var SCORE = 0;
var SCORE_BUBBLE = 10;
var SCORE_SUPER_BUBBLE = 50;
var SCORE_GHOST_COMBO = 200;
var LIFES = 2;
var GAMEOVER = false;
var LEVEL = 1;
var LEVEL_NEXT_TIMER = -1;
var LEVEL_NEXT_STATE = 0;
var TIME_GENERAL_TIMER = -1;
var TIME_GAME = 0;
var TIME_LEVEL = 0;
var TIME_LIFE = 0;
var TIME_FRUITS = 0;
var HELP_DELAY = 1500;
var HELP_TIMER = -1; // function blinkHelp() {
// 	if ( $('.help-button').attr("class").indexOf("yo") > -1 ) {
// 		$('.help-button').removeClass("yo");
// 	} else {
// 		$('.help-button').addClass("yo");
// 	}
// }

function initGame(newgame) {
  if (newgame) {
    stopPresentation();
    stopTrailer();
    HOME = false;
    GAMEOVER = false;
    $('#help').fadeOut("slow");
    score(0);
    clearMessage();
    $("#home").hide();
    $("#panel").show();
    var ctx = null;
    var canvas = document.getElementById('canvas-panel-title-pacman');
    canvas.setAttribute('width', '38');
    canvas.setAttribute('height', '32');

    if (canvas.getContext) {
      ctx = canvas.getContext('2d');
    }

    var x = 15;
    var y = 16;
    ctx.fillStyle = "#fff200";
    ctx.beginPath();
    ctx.arc(x, y, 14, (0.35 - 3 * 0.05) * Math.PI, (1.65 + 3 * 0.05) * Math.PI, false);
    ctx.lineTo(x - 5, y);
    ctx.fill();
    ctx.closePath();
    x = 32;
    y = 16;
    ctx.fillStyle = "#dca5be";
    ctx.beginPath();
    ctx.arc(x, y, 4, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.closePath();
  }

  initBoard();
  drawBoard();
  drawBoardDoor();
  initPaths();
  drawPaths();
  initBubbles();
  drawBubbles();
  initFruits();
  initPacman();
  drawPacman();
  initGhosts();
  drawGhosts();
  lifes();
  ready();
}

function win() {
  stopAllSound();
  LOCK = true;
  stopPacman();
  stopGhosts();
  stopBlinkSuperBubbles();
  stopTimes();
  eraseGhosts();
  window.sendreq(9);
}

function prepareNextLevel(i) {
  if (LEVEL_NEXT_TIMER === -1) {
    eraseBoardDoor();
    LEVEL_NEXT_TIMER = setInterval("prepareNextLevel()", 250);
  } else {
    LEVEL_NEXT_STATE++;
    drawBoard(LEVEL_NEXT_STATE % 2 === 0);

    if (LEVEL_NEXT_STATE > 6) {
      LEVEL_NEXT_STATE = 0;
      clearInterval(LEVEL_NEXT_TIMER);
      LEVEL_NEXT_TIMER = -1;
      nextLevel();
    }
  }
}

function nextLevel() {
  LOCK = false;
  LEVEL++;
  erasePacman();
  eraseGhosts();
  resetPacman();
  resetGhosts();
  initGame();
  TIME_LEVEL = 0;
  TIME_LIFE = 0;
  TIME_FRUITS = 0;
}

function retry() {
  stopTimes();
  erasePacman();
  eraseGhosts();
  resetPacman();
  resetGhosts();
  drawPacman();
  drawGhosts();
  TIME_LIFE = 0;
  TIME_FRUITS = 0;
  ready();
}

function ready() {
  LOCK = true;
  message("ready!");
  playReadySound();
  setTimeout("go()", "4100");
}

function go() {
  playSirenSound();
  LOCK = false;
  startTimes();
  clearMessage();
  blinkSuperBubbles();
  movePacman();
  moveGhosts();
}

function startTimes() {
  if (TIME_GENERAL_TIMER === -1) {
    TIME_GENERAL_TIMER = setInterval("times()", 1000);
  }
}

function times() {
  TIME_GAME++;
  TIME_LEVEL++;
  TIME_LIFE++;
  TIME_FRUITS++;
  fruit();
}

function pauseTimes() {
  if (TIME_GENERAL_TIMER != -1) {
    clearInterval(TIME_GENERAL_TIMER);
    TIME_GENERAL_TIMER = -1;
  }

  if (FRUIT_CANCEL_TIMER != null) FRUIT_CANCEL_TIMER.pause();
}

function resumeTimes() {
  startTimes();
  if (FRUIT_CANCEL_TIMER != null) FRUIT_CANCEL_TIMER.resume();
}

function stopTimes() {
  if (TIME_GENERAL_TIMER != -1) {
    clearInterval(TIME_GENERAL_TIMER);
    TIME_GENERAL_TIMER = -1;
  }

  if (FRUIT_CANCEL_TIMER != null) {
    FRUIT_CANCEL_TIMER.cancel();
    FRUIT_CANCEL_TIMER = null;
    eraseFruit();
  }
}

function pauseGame() {
  if (!PAUSE) {
    stopAllSound();
    PAUSE = true;
    message("pause");
    pauseTimes();
    pausePacman();
    pauseGhosts();
    stopBlinkSuperBubbles();
  }
}

function resumeGame() {
  if (PAUSE) {
    testStateGhosts();
    PAUSE = false;
    clearMessage();
    resumeTimes();
    resumePacman();
    resumeGhosts();
    blinkSuperBubbles();
  }
}

function lifes(l) {
  if (l) {
    if (l > 0) {
      playExtraLifeSound();
    }

    LIFES += l;
  }

  var canvas = document.getElementById('canvas-lifes');
  canvas.setAttribute('width', '120');
  canvas.setAttribute('height', '30');

  if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, 120, 30);
    ctx.fillStyle = "#fff200";

    for (var i = 0, imax = LIFES; i < imax && i < 4; i++) {
      ctx.beginPath();
      var lineToX = 13;
      var lineToY = 15;
      ctx.arc(lineToX + i * 30, lineToY, 13, (1.35 - 3 * 0.05) * Math.PI, (0.65 + 3 * 0.05) * Math.PI, false);
      ctx.lineTo(lineToX + i * 30 + 4, lineToY);
      ctx.fill();
      ctx.closePath();
    }
  }
}

function gameover() {
  GAMEOVER = true;
  message("game over");
  stopTimes();
  erasePacman();
  eraseGhosts();
  resetPacman();
  resetGhosts();
  TIME_GAME = 0;
  TIME_LEVEL = 0;
  TIME_LIFE = 0;
  TIME_FRUITS = 0;
  LIFES = 2;
  LEVEL = 1;
  SCORE = 0;
  window.sendreq(1);
}

function message(m) {
  $("#message").html(m);
  if (m === "game over") $("#message").addClass("red");
}

function clearMessage() {
  $("#message").html("");
  $("#message").removeClass("red");
}

function score(s, type) {
  var scoreBefore = SCORE / 10000 | 0;
  SCORE += s;

  if (SCORE === 0) {
    $('#score span').html("00");
  } else {
    $('#score span').html(SCORE);
  }

  var scoreAfter = SCORE / 10000 | 0;

  if (scoreAfter > scoreBefore) {
    lifes(+1);
  }

  if (SCORE > HIGHSCORE) {
    HIGHSCORE = SCORE;

    if (HIGHSCORE === 0) {
      $('#highscore span').html("00");
    } else {
      $('#highscore span').html(HIGHSCORE);
    }
  }

  if (type && (type === "clyde" || type === "pinky" || type === "inky" || type === "blinky")) {
    erasePacman();
    eraseGhost(type);
    $("#board").append('<span class="combo">' + SCORE_GHOST_COMBO + '</span>');
    $("#board span.combo").css('top', eval('GHOST_' + type.toUpperCase() + '_POSITION_Y - 10') + 'px');
    $("#board span.combo").css('left', eval('GHOST_' + type.toUpperCase() + '_POSITION_X - 10') + 'px');
    SCORE_GHOST_COMBO = SCORE_GHOST_COMBO * 2;
  } else if (type && type === "fruit") {
    $("#board").append('<span class="fruits">' + s + '</span>');
    $("#board span.fruits").css('top', FRUITS_POSITION_Y - 14 + 'px');
    $("#board span.fruits").css('left', FRUITS_POSITION_X - 14 + 'px');
  }
}

/***/ }),

/***/ "./public/game-6/js/ghosts.js":
/*!************************************!*\
  !*** ./public/game-6/js/ghosts.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var GHOST_BLINKY_CANVAS_CONTEXT = null;
var GHOST_BLINKY_POSITION_X = 276;
var GHOST_BLINKY_POSITION_Y = 204;
var GHOST_BLINKY_DIRECTION = 1;
var GHOST_BLINKY_COLOR = "#ed1b24";
var GHOST_BLINKY_MOVING_TIMER = -1;
var GHOST_BLINKY_MOVING = false;
var GHOST_BLINKY_BODY_STATE = 0;
var GHOST_BLINKY_STATE = 0;
var GHOST_BLINKY_EAT_TIMER = null;
var GHOST_BLINKY_AFFRAID_TIMER = null;
var GHOST_BLINKY_AFFRAID_STATE = 0;
var GHOST_BLINKY_TUNNEL = false;
var GHOST_PINKY_CANVAS_CONTEXT = null;
var GHOST_PINKY_POSITION_X = 276;
var GHOST_PINKY_POSITION_Y = 258;
var GHOST_PINKY_DIRECTION = 2;
var GHOST_PINKY_COLOR = "#feaec9";
var GHOST_PINKY_MOVING_TIMER = -1;
var GHOST_PINKY_MOVING = false;
var GHOST_PINKY_BODY_STATE = 1;
var GHOST_PINKY_STATE = 0;
var GHOST_PINKY_EAT_TIMER = null;
var GHOST_PINKY_AFFRAID_TIMER = null;
var GHOST_PINKY_AFFRAID_STATE = 0;
var GHOST_PINKY_TUNNEL = false;
var GHOST_INKY_CANVAS_CONTEXT = null;
var GHOST_INKY_POSITION_X = 238;
var GHOST_INKY_POSITION_Y = 258;
var GHOST_INKY_DIRECTION = 3;
var GHOST_INKY_COLOR = "#4adecb";
var GHOST_INKY_MOVING_TIMER = -1;
var GHOST_INKY_MOVING = false;
var GHOST_INKY_BODY_STATE = 2;
var GHOST_INKY_STATE = 0;
var GHOST_INKY_EAT_TIMER = null;
var GHOST_INKY_AFFRAID_TIMER = null;
var GHOST_INKY_AFFRAID_STATE = 0;
var GHOST_INKY_TUNNEL = false;
var GHOST_CLYDE_CANVAS_CONTEXT = null;
var GHOST_CLYDE_POSITION_X = 314;
var GHOST_CLYDE_POSITION_Y = 258;
var GHOST_CLYDE_DIRECTION = 4;
var GHOST_CLYDE_COLOR = "#f99c00";
var GHOST_CLYDE_MOVING_TIMER = -1;
var GHOST_CLYDE_MOVING = false;
var GHOST_CLYDE_BODY_STATE = 3;
var GHOST_CLYDE_STATE = 0;
var GHOST_CLYDE_EAT_TIMER = null;
var GHOST_CLYDE_AFFRAID_TIMER = null;
var GHOST_CLYDE_AFFRAID_STATE = 0;
var GHOST_CLYDE_TUNNEL = false;
var GHOST_AFFRAID_COLOR = "#2d3eff";
var GHOST_AFFRAID_FINISH_COLOR = "#fff";
var GHOST_POSITION_STEP = 2;
var GHOST_MOVING_SPEED = 15;
var GHOST_TUNNEL_MOVING_SPEED = 35;
var GHOST_AFFRAID_MOVING_SPEED = 40;
var GHOST_EAT_MOVING_SPEED = 6;
var GHOST_AFFRAID_TIME = 8500;
var GHOST_EAT_TIME = 5500;
var GHOST_BODY_STATE_MAX = 6;

function initGhosts() {
  initGhost('blinky');
  initGhost('pinky');
  initGhost('inky');
  initGhost('clyde');
}

function initGhost(ghost) {
  var canvas = document.getElementById('canvas-ghost-' + ghost);
  canvas.setAttribute('width', '550');
  canvas.setAttribute('height', '550');

  if (canvas.getContext) {
    eval('GHOST_' + ghost.toUpperCase() + '_CANVAS_CONTEXT = canvas.getContext("2d")');
  }
}

function resetGhosts() {
  stopGhosts();
  GHOST_BLINKY_POSITION_X = 276;
  GHOST_BLINKY_POSITION_Y = 204;
  GHOST_BLINKY_DIRECTION = 1;
  GHOST_BLINKY_MOVING_TIMER = -1;
  GHOST_BLINKY_MOVING = false;
  GHOST_BLINKY_BODY_STATE = 0;
  GHOST_BLINKY_STATE = 0;
  GHOST_BLINKY_EAT_TIMER = null;
  GHOST_BLINKY_AFFRAID_TIMER = null;
  GHOST_BLINKY_AFFRAID_STATE = 0;
  GHOST_PINKY_POSITION_X = 276;
  GHOST_PINKY_POSITION_Y = 258;
  GHOST_PINKY_DIRECTION = 2;
  GHOST_PINKY_MOVING_TIMER = -1;
  GHOST_PINKY_MOVING = false;
  GHOST_PINKY_BODY_STATE = 1;
  GHOST_PINKY_STATE = 0;
  GHOST_PINKY_EAT_TIMER = null;
  GHOST_PINKY_AFFRAID_TIMER = null;
  GHOST_PINKY_AFFRAID_STATE = 0;
  GHOST_INKY_POSITION_X = 238;
  GHOST_INKY_POSITION_Y = 258;
  GHOST_INKY_DIRECTION = 3;
  GHOST_INKY_MOVING_TIMER = -1;
  GHOST_INKY_MOVING = false;
  GHOST_INKY_BODY_STATE = 2;
  GHOST_INKY_STATE = 0;
  GHOST_INKY_EAT_TIMER = null;
  GHOST_INKY_AFFRAID_TIMER = null;
  GHOST_INKY_AFFRAID_STATE = 0;
  GHOST_CLYDE_POSITION_X = 314;
  GHOST_CLYDE_POSITION_Y = 258;
  GHOST_CLYDE_DIRECTION = 4;
  GHOST_CLYDE_MOVING_TIMER = -1;
  GHOST_CLYDE_MOVING = false;
  GHOST_CLYDE_BODY_STATE = 3;
  GHOST_CLYDE_STATE = 0;
  GHOST_CLYDE_EAT_TIMER = null;
  GHOST_CLYDE_AFFRAID_TIMER = null;
  GHOST_CLYDE_AFFRAID_STATE = 0;
}

function getGhostCanevasContext(ghost) {
  return eval('GHOST_' + ghost.toUpperCase() + '_CANVAS_CONTEXT');
}

function drawGhosts() {
  drawGhost("blinky");
  drawGhost('pinky');
  drawGhost('inky');
  drawGhost("clyde");
}

function drawGhost(ghost) {
  var ctx = getGhostCanevasContext(ghost);

  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 0')) {
    eval('ctx.fillStyle = GHOST_' + ghost.toUpperCase() + '_COLOR');
  } else {
    if (eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_STATE === 1')) {
      eval('ctx.fillStyle = GHOST_AFFRAID_FINISH_COLOR');
    } else {
      eval('ctx.fillStyle = GHOST_AFFRAID_COLOR');
    }
  }

  eval('drawHelperGhost(ctx, GHOST_' + ghost.toUpperCase() + '_POSITION_X, GHOST_' + ghost.toUpperCase() + '_POSITION_Y, GHOST_' + ghost.toUpperCase() + '_DIRECTION, GHOST_' + ghost.toUpperCase() + '_BODY_STATE, GHOST_' + ghost.toUpperCase() + '_STATE, GHOST_' + ghost.toUpperCase() + '_AFFRAID_STATE)');
  ctx.closePath();
}

function affraidGhosts() {
  playWazaSound();
  SCORE_GHOST_COMBO = 200;
  affraidGhost("blinky");
  affraidGhost("pinky");
  affraidGhost("inky");
  affraidGhost("clyde");
}

function affraidGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER !== null')) {
    eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.cancel()');
    eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER = null');
  }

  eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_STATE = 0');

  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 0') || eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
    stopGhost(ghost);
    eval('GHOST_' + ghost.toUpperCase() + '_STATE = 1');
    moveGhost(ghost);
    eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER = new Timer("cancelAffraidGhost(\'' + ghost + '\')", GHOST_AFFRAID_TIME)');
  }
}

function cancelAffraidGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
    eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.cancel()');
    eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER = null');
    stopGhost(ghost);
    eval('GHOST_' + ghost.toUpperCase() + '_STATE = 0');
    moveGhost(ghost);
    testStateGhosts();
  }
}

function testStateGhosts() {
  if (GHOST_BLINKY_STATE === 1 || GHOST_PINKY_STATE === 1 || GHOST_INKY_STATE === 1 || GHOST_CLYDE_STATE === 1) {
    playWazaSound();
  } else if (GHOST_BLINKY_STATE === -1 || GHOST_PINKY_STATE === -1 || GHOST_INKY_STATE === -1 || GHOST_CLYDE_STATE === -1) {
    playGhostEatenSound();
  } else {
    playSirenSound();
  }
}

function startEatGhost(ghost) {
  if (!LOCK) {
    playEatGhostSound();
    LOCK = true;

    if (eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER !== null')) {
      eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.cancel()');
      eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER = null');
    }

    score(SCORE_GHOST_COMBO, ghost);
    pauseGhosts();
    pausePacman();
    setTimeout('eatGhost(\'' + ghost + '\')', 600);
  }
}

function eatGhost(ghost) {
  playGhostEatenSound();

  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
    $("#board span.combo").remove();
    eval('GHOST_' + ghost.toUpperCase() + '_STATE = -1');
    eval('GHOST_' + ghost.toUpperCase() + '_EAT_TIMER = new Timer("cancelEatGhost(\'' + ghost + '\')", GHOST_EAT_TIME)');
    eval('GHOST_' + ghost.toUpperCase() + '_EAT_TIMER.pause()');
  }

  resumeGhosts();
  resumePacman();
  LOCK = false;
}

function cancelEatGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === -1')) {
    eval('GHOST_' + ghost.toUpperCase() + '_EAT_TIMER = null');
    stopGhost(ghost);
    eval('GHOST_' + ghost.toUpperCase() + '_STATE = 0');
    moveGhost(ghost);
    testStateGhosts();
  }
}

function moveGhosts() {
  moveGhost("blinky");
  moveGhost('pinky');
  moveGhost('inky');
  moveGhost("clyde");
}

function moveGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_MOVING === false')) {
    eval('GHOST_' + ghost.toUpperCase() + '_MOVING = true;');
    var speed = -1;

    if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
      speed = GHOST_AFFRAID_MOVING_SPEED;
    } else if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 0')) {
      if (eval('GHOST_' + ghost.toUpperCase() + '_TUNNEL === false')) {
        speed = GHOST_MOVING_SPEED;
      } else {
        speed = GHOST_TUNNEL_MOVING_SPEED;
      }
    } else {
      speed = GHOST_EAT_MOVING_SPEED;
    }

    eval('GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER = setInterval("moveGhost(\'' + ghost + '\')", ' + speed + ');');
  } else {
    changeDirection(ghost);

    if (eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER !== null')) {
      var remain = eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.remain();');

      if (remain >= 2500 && remain < 3000 || remain >= 1500 && remain <= 2000 || remain >= 500 && remain <= 1000 || remain < 0) {
        eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_STATE = 1;');
      } else if (remain > 2000 && remain < 2500 || remain > 1000 && remain < 1500 || remain >= 0 && remain < 500) {
        eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_STATE = 0;');
      }
    }

    if (canMoveGhost(ghost)) {
      eraseGhost(ghost);

      if (eval('GHOST_' + ghost.toUpperCase() + '_BODY_STATE < GHOST_BODY_STATE_MAX')) {
        eval('GHOST_' + ghost.toUpperCase() + '_BODY_STATE ++;');
      } else {
        eval('GHOST_' + ghost.toUpperCase() + '_BODY_STATE = 0;');
      }

      if (eval('GHOST_' + ghost.toUpperCase() + '_DIRECTION === 1')) {
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X += GHOST_POSITION_STEP;');
      } else if (eval('GHOST_' + ghost.toUpperCase() + '_DIRECTION === 2')) {
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y += GHOST_POSITION_STEP;');
      } else if (eval('GHOST_' + ghost.toUpperCase() + '_DIRECTION === 3')) {
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X -= GHOST_POSITION_STEP;');
      } else if (eval('GHOST_' + ghost.toUpperCase() + '_DIRECTION === 4')) {
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y -= GHOST_POSITION_STEP;');
      }

      if (eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X === 2') && eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y === 258')) {
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X = 548;');
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y = 258;');
      } else if (eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X === 548') && eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y === 258')) {
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X = 2;');
        eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y = 258;');
      }

      drawGhost(ghost);

      if (eval('GHOST_' + ghost.toUpperCase() + '_BODY_STATE === 3') && eval('GHOST_' + ghost.toUpperCase() + '_STATE != -1')) {
        if (!PACMAN_MOVING) {
          testGhostPacman(ghost);
        }

        testGhostTunnel(ghost);
      }
    } else {
      eval('GHOST_' + ghost.toUpperCase() + '_DIRECTION = oneDirection();');
    }
  }
}

function testGhostTunnel(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 0')) {
    if (isInTunnel(ghost) && eval('GHOST_' + ghost.toUpperCase() + '_TUNNEL === false')) {
      stopGhost(ghost);
      eval('GHOST_' + ghost.toUpperCase() + '_TUNNEL = true');
      moveGhost(ghost);
    } else if (!isInTunnel(ghost) && eval('GHOST_' + ghost.toUpperCase() + '_TUNNEL === true')) {
      stopGhost(ghost);
      eval('GHOST_' + ghost.toUpperCase() + '_TUNNEL = false');
      moveGhost(ghost);
    }
  }
}

function isInTunnel(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X >= 2') && eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X <= 106') && eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y === 258')) {
    return true;
  } else if (eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X >= 462') && eval('GHOST_' + ghost.toUpperCase() + '_POSITION_X <= 548') && eval('GHOST_' + ghost.toUpperCase() + '_POSITION_Y === 258')) {
    return true;
  }
}

function changeDirection(ghost) {
  eval('var direction = GHOST_' + ghost.toUpperCase() + '_DIRECTION');
  eval('var state = GHOST_' + ghost.toUpperCase() + '_STATE');
  eval('var ghostX = GHOST_' + ghost.toUpperCase() + '_POSITION_X');
  eval('var ghostY = GHOST_' + ghost.toUpperCase() + '_POSITION_Y');
  var tryDirection = oneDirection();

  if (state === 0 || state === 1) {
    if (ghostX != 276 && ghostY != 258) {
      var pacmanX = PACMAN_POSITION_X;
      var pacmanY = PACMAN_POSITION_Y;
      var axe = oneAxe();

      if (ghost === "blinky") {
        var nothing = whatsYourProblem();

        if (nothing < 6) {
          tryDirection = getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY);

          if (!(canMoveGhost(ghost, tryDirection) && direction != tryDirection - 2 && direction != tryDirection + 2)) {
            axe++;
            if (axe > 2) axe = 1;
            tryDirection = getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY);
          }
        }
      } else if (ghost === "pinky") {
        var nothing = whatsYourProblem();

        if (nothing < 3) {
          tryDirection = getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY);

          if (!(canMoveGhost(ghost, tryDirection) && direction != tryDirection - 2 && direction != tryDirection + 2)) {
            axe++;
            if (axe > 2) axe = 1;
            tryDirection = getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY);
          }

          tryDirection = reverseDirection(tryDirection);
        }
      } else if (ghost === "inky") {
        var good = anyGoodIdea();

        if (good < 3) {
          tryDirection = getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY);

          if (!(canMoveGhost(ghost, tryDirection) && direction != tryDirection - 2 && direction != tryDirection + 2)) {
            axe++;
            if (axe > 2) axe = 1;
            tryDirection = getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY);
          }
        }
      }
    }

    if (state === 1) {
      tryDirection = reverseDirection(tryDirection);
    }
  } else {
    var axe = oneAxe();
    tryDirection = getRightDirectionForHome(axe, ghostX, ghostY);

    if (canMoveGhost(ghost, tryDirection) && direction != tryDirection - 2 && direction != tryDirection + 2) {} else {
      axe++;
      if (axe > 2) axe = 1;
      tryDirection = getRightDirectionForHome(axe, ghostX, ghostY);
    }
  }

  if (canMoveGhost(ghost, tryDirection) && direction != tryDirection - 2 && direction != tryDirection + 2) {
    eval('GHOST_' + ghost.toUpperCase() + '_DIRECTION = tryDirection');
  }
}

function getRightDirectionForHome(axe, ghostX, ghostY) {
  var homeX = 276;
  var homeY = 204;

  if (ghostY === 204 && ghostX === 276) {
    return 2;
  } else if (ghostX === 276 && ghostY === 258) {
    return oneDirectionX();
  } else {
    if (axe === 1) {
      if (ghostX > homeX) {
        return 3;
      } else {
        return 1;
      }
    } else {
      if (ghostY > homeY) {
        return 4;
      } else {
        return 2;
      }
    }
  }
}

function getRightDirection(axe, ghostX, ghostY, pacmanX, pacmanY) {
  if (axe === 1) {
    if (ghostX > pacmanX) {
      return 3;
    } else {
      return 1;
    }
  } else {
    if (ghostY > pacmanY) {
      return 4;
    } else {
      return 2;
    }
  }
}

function reverseDirection(direction) {
  if (direction > 2) return direction - 2;else return direction + 2;
}

function eraseGhost(ghost) {
  var ctx = getGhostCanevasContext(ghost);
  eval('ctx.clearRect(GHOST_' + ghost.toUpperCase() + '_POSITION_X - 17, GHOST_' + ghost.toUpperCase() + '_POSITION_Y - 17, 34, 34)');
}

function eraseGhosts() {
  eraseGhost('blinky');
  eraseGhost('pinky');
  eraseGhost('inky');
  eraseGhost('clyde');
}

function canMoveGhost(ghost, direction) {
  if (!direction) {
    eval('var direction = GHOST_' + ghost.toUpperCase() + '_DIRECTION');
  }

  eval('var positionX = GHOST_' + ghost.toUpperCase() + '_POSITION_X');
  eval('var positionY = GHOST_' + ghost.toUpperCase() + '_POSITION_Y');
  eval('var state = GHOST_' + ghost.toUpperCase() + '_STATE');
  if (positionX === 276 && positionY === 204 && direction === 2 && state === 0) return false;

  if (direction === 1) {
    positionX += GHOST_POSITION_STEP;
  } else if (direction === 2) {
    positionY += GHOST_POSITION_STEP;
  } else if (direction === 3) {
    positionX -= GHOST_POSITION_STEP;
  } else if (direction === 4) {
    positionY -= GHOST_POSITION_STEP;
  }

  for (var i = 0, imax = PATHS.length; i < imax; i++) {
    var p = PATHS[i];
    var startX = p.split("-")[0].split(",")[0];
    var startY = p.split("-")[0].split(",")[1];
    var endX = p.split("-")[1].split(",")[0];
    var endY = p.split("-")[1].split(",")[1];

    if (positionX >= startX && positionX <= endX && positionY >= startY && positionY <= endY) {
      return true;
    }
  }

  return false;
}

function oneDirection() {
  return Math.floor(Math.random() * (4 - 1 + 1) + 1);
}

function oneDirectionX() {
  var direction = oneDirection();
  if (direction === 4 || direction === 2) direction -= 1;
  return direction;
}

function oneDirectionY() {
  var direction = oneDirection();
  if (direction === 3 || direction === 1) direction -= 1;
  return direction;
}

function stopGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
    eval('if(GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER !== null) GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.cancel()');
    eval('GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER = null');
    eval('GHOST_' + ghost.toUpperCase() + '_STATE = 0');
  } else if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === -1')) {
    eval('if(GHOST_' + ghost.toUpperCase() + '_EAT_TIMER !== null) GHOST_' + ghost.toUpperCase() + '_EAT_TIMER.cancel()');
    eval('GHOST_' + ghost.toUpperCase() + '_EAT_TIMER = null');
    eval('GHOST_' + ghost.toUpperCase() + '_STATE = 0');
  }

  if (eval('GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER != -1')) {
    eval('clearInterval(GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER)');
    eval('GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER = -1');
    eval('GHOST_' + ghost.toUpperCase() + '_MOVING = false');
  }
}

function stopGhosts() {
  stopGhost('blinky');
  stopGhost('pinky');
  stopGhost('inky');
  stopGhost('clyde');
}

function pauseGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
    eval('if(GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER !== null) GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.pause()');
  } else if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === -1')) {
    eval('if(GHOST_' + ghost.toUpperCase() + '_EAT_TIMER !== null) GHOST_' + ghost.toUpperCase() + '_EAT_TIMER.pause()');
  }

  if (eval('GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER != -1')) {
    eval('clearInterval(GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER)');
    eval('GHOST_' + ghost.toUpperCase() + '_MOVING_TIMER = -1');
    eval('GHOST_' + ghost.toUpperCase() + '_MOVING = false');
  }
}

function pauseGhosts() {
  pauseGhost('blinky');
  pauseGhost('pinky');
  pauseGhost('inky');
  pauseGhost('clyde');
}

function resumeGhost(ghost) {
  if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === 1')) {
    eval('if(GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER !== null) GHOST_' + ghost.toUpperCase() + '_AFFRAID_TIMER.resume()');
  } else if (eval('GHOST_' + ghost.toUpperCase() + '_STATE === -1')) {
    eval('if(GHOST_' + ghost.toUpperCase() + '_EAT_TIMER !== null) GHOST_' + ghost.toUpperCase() + '_EAT_TIMER.resume()');
  }

  moveGhost(ghost);
}

function resumeGhosts() {
  resumeGhost('blinky');
  resumeGhost('pinky');
  resumeGhost('inky');
  resumeGhost('clyde');
}

function drawHelperGhost(ctx, x, y, d, b, s, a) {
  if (s != -1) {
    ctx.beginPath();
    ctx.moveTo(x - 15, y + 16);
    ctx.lineTo(x - 15, y + 16 - 18);
    ctx.bezierCurveTo(x - 15, y + 16 - 26, x - 15 + 6, y + 16 - 32, x - 15 + 14, y + 16 - 32);
    ctx.bezierCurveTo(x - 15 + 22, y + 16 - 32, x - 15 + 28, y + 16 - 26, x - 15 + 28, y + 16 - 18);
    ctx.lineTo(x - 15 + 28, y + 16);

    if (b < 4) {
      ctx.lineTo(x - 15 + 23.333, y + 16 - 5.333);
      ctx.lineTo(x - 15 + 18.666, y + 16);
      ctx.lineTo(x - 15 + 14, y + 16 - 5.333);
      ctx.lineTo(x - 15 + 9.333, y + 16);
      ctx.lineTo(x - 15 + 4.666, y + 16 - 5.333);
    } else {
      ctx.lineTo(x - 15 + 24.333, y + 16 - 5.333);
      ctx.lineTo(x - 15 + 20.666, y + 16);
      ctx.lineTo(x - 15 + 17.333, y + 16 - 5.333);
      ctx.lineTo(x - 15 + 12.666, y + 16);
      ctx.lineTo(x - 15 + 9, y + 16 - 5.333);
      ctx.lineTo(x - 15 + 5.333, y + 16);
      ctx.lineTo(x - 15 + 2.666, y + 16 - 5.333);
    }

    ctx.lineTo(x - 15, y + 16);
    ctx.fill();
  }

  var eyesX = 0;
  var eyesY = 0;

  if (d === 4) {
    eyesY = -5;
  } else if (d === 1) {
    eyesX = +2;
  } else if (d === 2) {
    eyesY = 0;
    eyesY = +5;
  } else if (d === 3) {
    eyesX = -3;
  }

  if (s === 0 || s === -1) {
    ctx.fillStyle = "white";
    ctx.beginPath();
    ctx.moveTo(x - 15 + 8 + eyesX, y + 16 - 24 + eyesY);
    ctx.bezierCurveTo(x - 15 + 5 + eyesX, y + 16 - 24 + eyesY, x - 15 + 4 + eyesX, y + 16 - 21 + eyesY, x - 15 + 4 + eyesX, y + 16 - 19 + eyesY);
    ctx.bezierCurveTo(x - 15 + 4 + eyesX, y + 16 - 17 + eyesY, x - 15 + 5 + eyesX, y + 16 - 14 + eyesY, x - 15 + 8 + eyesX, y + 16 - 14 + eyesY);
    ctx.bezierCurveTo(x - 15 + 11 + eyesX, y + 16 - 14 + eyesY, x - 15 + 12 + eyesX, y + 16 - 17 + eyesY, x - 15 + 12 + eyesX, y + 16 - 19 + eyesY);
    ctx.bezierCurveTo(x - 15 + 12 + eyesX, y + 16 - 21 + eyesY, x - 15 + 11 + eyesX, y + 16 - 24 + eyesY, x - 15 + 8 + eyesX, y + 16 - 24 + eyesY);
    ctx.moveTo(x - 15 + 20 + eyesX, y + 16 - 24 + eyesY);
    ctx.bezierCurveTo(x - 15 + 17 + eyesX, y + 16 - 24 + eyesY, x - 15 + 16 + eyesX, y + 16 - 21 + eyesY, x - 15 + 16 + eyesX, y + 16 - 19 + eyesY);
    ctx.bezierCurveTo(x - 15 + 16 + eyesX, y + 16 - 17 + eyesY, x - 15 + 17 + eyesX, y + 16 - 14 + eyesY, x - 15 + 20 + eyesX, y + 16 - 14 + eyesY);
    ctx.bezierCurveTo(x - 15 + 23 + eyesX, y + 16 - 14 + eyesY, x - 15 + 24 + eyesX, y + 16 - 17 + eyesY, x - 15 + 24 + eyesX, y + 16 - 19 + eyesY);
    ctx.bezierCurveTo(x - 15 + 24 + eyesX, y + 16 - 21 + eyesY, x - 15 + 23 + eyesX, y + 16 - 24 + eyesY, x - 15 + 20 + eyesX, y + 16 - 24 + eyesY);
    ctx.fill();

    if (d === 4) {
      eyesY = -9;
      eyesX = 2;
    } else if (d === 1) {
      eyesX = +6;
    } else if (d === 2) {
      eyesY = +8;
      eyesX = 2;
    } else if (d === 3) {}

    ctx.fillStyle = "#0000fa";
    ctx.beginPath();
    ctx.arc(x - 15 + 18 + eyesX, y + 16 - 18 + eyesY, 2, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.beginPath();
    ctx.arc(x - 15 + 6 + eyesX, y + 16 - 18 + eyesY, 2, 0, Math.PI * 2, true);
    ctx.fill();
  } else {
    if (a === 1) {
      ctx.fillStyle = "#ee2933";
    } else {
      ctx.fillStyle = "#e5bed0";
    }

    ctx.beginPath();
    ctx.arc(x - 15 + 18, y + 13 - 17, 2, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.beginPath();
    ctx.arc(x - 15 + 10, y + 13 - 17, 2, 0, Math.PI * 2, true);
    ctx.fill();

    if (a === 1) {
      ctx.strokeStyle = "#ee2933";
    } else {
      ctx.strokeStyle = "#e5bed0";
    }

    ctx.beginPath();
    ctx.lineTo(x - 14.333 + 24, y + 6);
    ctx.lineTo(x - 14.333 + 21, y + 6 - 3);
    ctx.lineTo(x - 14.333 + 17, y + 6);
    ctx.lineTo(x - 14.333 + 14, y + 6 - 3);
    ctx.lineTo(x - 14.333 + 10, y + 6);
    ctx.lineTo(x - 14.333 + 7, y + 6 - 3);
    ctx.lineTo(x - 14.333 + 3, y + 6);
    ctx.stroke();
  }
}

/***/ }),

/***/ "./public/game-6/js/home.js":
/*!**********************************!*\
  !*** ./public/game-6/js/home.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var HOME = false;
var HOME_PRESENTATION_TIMER = -1;
var HOME_PRESENTATION_STATE = 0;
var HOME_TRAILER_TIMER = -1;
var HOME_TRAILER_STATE = 0;
var PACMAN_TRAILER_CANVAS_CONTEXT = null;
var PACMAN_TRAILER_DIRECTION = 3;
var PACMAN_TRAILER_POSITION_X = 600;
var PACMAN_TRAILER_POSITION_Y = 25;
var PACMAN_TRAILER_POSITION_STEP = 3;
var PACMAN_TRAILER_MOUNTH_STATE = 3;
var PACMAN_TRAILER_MOUNTH_STATE_MAX = 6;
var PACMAN_TRAILER_SIZE = 16;
var GHOST_TRAILER_CANVAS_CONTEXT = null;
var GHOST_TRAILER_BODY_STATE_MAX = 6;
var GHOST_TRAILER_POSITION_STEP = 3;
var GHOST_BLINKY_TRAILER_POSITION_X = 1000;
var GHOST_BLINKY_TRAILER_POSITION_Y = 25;
var GHOST_BLINKY_TRAILER_DIRECTION = 3;
var GHOST_BLINKY_TRAILER_COLOR = "#ed1b24";
var GHOST_BLINKY_TRAILER_BODY_STATE = 0;
var GHOST_BLINKY_TRAILER_STATE = 0;
var GHOST_PINKY_TRAILER_POSITION_X = 1035;
var GHOST_PINKY_TRAILER_POSITION_Y = 25;
var GHOST_PINKY_TRAILER_DIRECTION = 3;
var GHOST_PINKY_TRAILER_COLOR = "#feaec9";
var GHOST_PINKY_TRAILER_BODY_STATE = 1;
var GHOST_PINKY_TRAILER_STATE = 0;
var GHOST_INKY_TRAILER_POSITION_X = 1070;
var GHOST_INKY_TRAILER_POSITION_Y = 25;
var GHOST_INKY_TRAILER_DIRECTION = 3;
var GHOST_INKY_TRAILER_COLOR = "#4adecb";
var GHOST_INKY_TRAILER_BODY_STATE = 2;
var GHOST_INKY_TRAILER_STATE = 0;
var GHOST_CLYDE_TRAILER_POSITION_X = 1105;
var GHOST_CLYDE_TRAILER_POSITION_Y = 25;
var GHOST_CLYDE_TRAILER_DIRECTION = 3;
var GHOST_CLYDE_TRAILER_COLOR = "#f99c00";
var GHOST_CLYDE_TRAILER_BODY_STATE = 3;
var GHOST_CLYDE_TRAILER_STATE = 0;

function initHome() {
  HOME = true;
  GAMEOVER = false;
  LOCK = false;
  PACMAN_DEAD = false;
  $("#panel").hide();
  $("#home").show();
  $("#home h3 em").append(" - " + new Date().getFullYear());
  $('#help').fadeOut("slow");
  var ctx = null;
  var canvas = document.getElementById('canvas-home-title-pacman');
  canvas.setAttribute('width', '0');
  canvas.setAttribute('height', '0');

  if (canvas.getContext) {
    ctx = canvas.getContext('2d');
  }

  var x = 50;
  var y = 50;
  ctx.fillStyle = "#fff200";
  ctx.beginPath();
  ctx.arc(x, y, 45, (0.35 - 3 * 0.05) * Math.PI, (1.65 + 3 * 0.05) * Math.PI, false);
  ctx.lineTo(x - 10, y);
  ctx.fill();
  ctx.closePath();
  x = 95;
  y = 50;
  ctx.fillStyle = "#dca5be";
  ctx.beginPath();
  ctx.arc(x, y, 10, 0, 2 * Math.PI, false);
  ctx.fill();
  ctx.closePath();
  canvas = document.getElementById('canvas-presentation-blinky');
  canvas.setAttribute('width', '50');
  canvas.setAttribute('height', '50');

  if (canvas.getContext) {
    ctx = canvas.getContext('2d');
  }

  ctx.fillStyle = GHOST_BLINKY_COLOR;
  drawHelperGhost(ctx, 25, 25, 1, 0, 0, 0);
  canvas = document.getElementById('canvas-presentation-pinky');
  canvas.setAttribute('width', '50');
  canvas.setAttribute('height', '50');

  if (canvas.getContext) {
    ctx = canvas.getContext('2d');
  }

  ctx.fillStyle = GHOST_PINKY_COLOR;
  drawHelperGhost(ctx, 25, 25, 1, 0, 0, 0);
  canvas = document.getElementById('canvas-presentation-inky');
  canvas.setAttribute('width', '50');
  canvas.setAttribute('height', '50');

  if (canvas.getContext) {
    ctx = canvas.getContext('2d');
  }

  ctx.fillStyle = GHOST_INKY_COLOR;
  drawHelperGhost(ctx, 25, 25, 1, 0, 0, 0);
  canvas = document.getElementById('canvas-presentation-clyde');
  canvas.setAttribute('width', '50');
  canvas.setAttribute('height', '50');

  if (canvas.getContext) {
    ctx = canvas.getContext('2d');
  }

  ctx.fillStyle = GHOST_CLYDE_COLOR;
  drawHelperGhost(ctx, 25, 25, 1, 0, 0, 0); //startPresentation();
}

function startPresentation() {
  $("#presentation *").hide();

  if (HOME_PRESENTATION_TIMER === -1) {
    HOME_PRESENTATION_STATE = 0;
    HOME_PRESENTATION_TIMER = setInterval("nextSequencePresentation()", 500);
  }
}

function stopPresentation() {
  if (HOME_PRESENTATION_TIMER != -1) {
    $("#presentation *").hide();
    HOME_PRESENTATION_STATE = 0;
    clearInterval(HOME_PRESENTATION_TIMER);
    HOME_PRESENTATION_TIMER = -1;
  }
}

function nextSequencePresentation() {
  if (HOME_PRESENTATION_STATE === 0) {
    $("#presentation-titles").show();
  } else if (HOME_PRESENTATION_STATE === 2) {
    $("#canvas-presentation-blinky").show();
  } else if (HOME_PRESENTATION_STATE === 4) {
    $("#presentation-character-blinky").show();
  } else if (HOME_PRESENTATION_STATE === 5) {
    $("#presentation-name-blinky").show();
  } else if (HOME_PRESENTATION_STATE === 6) {
    $("#canvas-presentation-pinky").show();
  } else if (HOME_PRESENTATION_STATE === 8) {
    $("#presentation-character-pinky").show();
  } else if (HOME_PRESENTATION_STATE === 9) {
    $("#presentation-name-pinky").show();
  } else if (HOME_PRESENTATION_STATE === 10) {
    $("#canvas-presentation-inky").show();
  } else if (HOME_PRESENTATION_STATE === 12) {
    $("#presentation-character-inky").show();
  } else if (HOME_PRESENTATION_STATE === 13) {
    $("#presentation-name-inky").show();
  } else if (HOME_PRESENTATION_STATE === 14) {
    $("#canvas-presentation-clyde").show();
  } else if (HOME_PRESENTATION_STATE === 16) {
    $("#presentation-character-clyde").show();
  } else if (HOME_PRESENTATION_STATE === 17) {
    $("#presentation-name-clyde").show();
  }

  if (HOME_PRESENTATION_STATE === 17) {
    clearInterval(HOME_PRESENTATION_TIMER);
    HOME_PRESENTATION_TIMER = -1;
    startTrailer();
  } else {
    HOME_PRESENTATION_STATE++;
  }
}

function startTrailer() {
  var canvas = document.getElementById('trailer');
  canvas.setAttribute('width', '500');
  canvas.setAttribute('height', '50');

  if (canvas.getContext) {
    PACMAN_TRAILER_CANVAS_CONTEXT = canvas.getContext('2d');
  }

  if (HOME_TRAILER_TIMER === -1) {
    HOME_TRAILER_STATE = 0;
    HOME_TRAILER_TIMER = setInterval("nextSequenceTrailer()", 20);
  }
}

function stopTrailer() {
  if (HOME_TRAILER_TIMER != -1) {
    $("#presentation *").hide();
    HOME_TRAILER_STATE = 0;
    clearInterval(HOME_TRAILER_TIMER);
    HOME_TRAILER_TIMER = -1;
  }
}

function nextSequenceTrailer() {
  erasePacmanTrailer();
  eraseGhostsTrailer();

  if (PACMAN_TRAILER_MOUNTH_STATE < PACMAN_TRAILER_MOUNTH_STATE_MAX) {
    PACMAN_TRAILER_MOUNTH_STATE++;
  } else {
    PACMAN_TRAILER_MOUNTH_STATE = 0;
  }

  if (PACMAN_TRAILER_DIRECTION === 1) {
    PACMAN_TRAILER_POSITION_X += PACMAN_TRAILER_POSITION_STEP;
  } else if (PACMAN_TRAILER_DIRECTION === 3) {
    PACMAN_TRAILER_POSITION_X -= PACMAN_TRAILER_POSITION_STEP;
  }

  if (PACMAN_TRAILER_POSITION_X < -650) {
    PACMAN_TRAILER_DIRECTION = 1;
    PACMAN_TRAILER_POSITION_STEP++;
  }

  if (GHOST_BLINKY_TRAILER_BODY_STATE < GHOST_TRAILER_BODY_STATE_MAX) {
    GHOST_BLINKY_TRAILER_BODY_STATE++;
  } else {
    GHOST_BLINKY_TRAILER_BODY_STATE = 0;
  }

  if (GHOST_PINKY_TRAILER_BODY_STATE < GHOST_TRAILER_BODY_STATE_MAX) {
    GHOST_PINKY_TRAILER_BODY_STATE++;
  } else {
    GHOST_PINKY_TRAILER_BODY_STATE = 0;
  }

  if (GHOST_INKY_TRAILER_BODY_STATE < GHOST_TRAILER_BODY_STATE_MAX) {
    GHOST_INKY_TRAILER_BODY_STATE++;
  } else {
    GHOST_INKY_TRAILER_BODY_STATE = 0;
  }

  if (GHOST_CLYDE_TRAILER_BODY_STATE < GHOST_TRAILER_BODY_STATE_MAX) {
    GHOST_CLYDE_TRAILER_BODY_STATE++;
  } else {
    GHOST_CLYDE_TRAILER_BODY_STATE = 0;
  }

  if (GHOST_BLINKY_TRAILER_DIRECTION === 1) {
    GHOST_BLINKY_TRAILER_POSITION_X += GHOST_TRAILER_POSITION_STEP;
  } else if (GHOST_BLINKY_TRAILER_DIRECTION === 3) {
    GHOST_BLINKY_TRAILER_POSITION_X -= GHOST_TRAILER_POSITION_STEP;
  }

  if (GHOST_PINKY_TRAILER_DIRECTION === 1) {
    GHOST_PINKY_TRAILER_POSITION_X += GHOST_TRAILER_POSITION_STEP;
  } else if (GHOST_PINKY_TRAILER_DIRECTION === 3) {
    GHOST_PINKY_TRAILER_POSITION_X -= GHOST_TRAILER_POSITION_STEP;
  }

  if (GHOST_INKY_TRAILER_DIRECTION === 1) {
    GHOST_INKY_TRAILER_POSITION_X += GHOST_TRAILER_POSITION_STEP;
  } else if (GHOST_INKY_TRAILER_DIRECTION === 3) {
    GHOST_INKY_TRAILER_POSITION_X -= GHOST_TRAILER_POSITION_STEP;
  }

  if (GHOST_CLYDE_TRAILER_DIRECTION === 1) {
    GHOST_CLYDE_TRAILER_POSITION_X += GHOST_TRAILER_POSITION_STEP;
  } else if (GHOST_CLYDE_TRAILER_DIRECTION === 3) {
    GHOST_CLYDE_TRAILER_POSITION_X -= GHOST_TRAILER_POSITION_STEP;
  }

  if (GHOST_BLINKY_TRAILER_POSITION_X < -255) {
    GHOST_BLINKY_TRAILER_DIRECTION = 1;
    GHOST_BLINKY_TRAILER_STATE = 1;
  }

  if (GHOST_PINKY_TRAILER_POSITION_X < -220) {
    GHOST_PINKY_TRAILER_DIRECTION = 1;
    GHOST_PINKY_TRAILER_STATE = 1;
  }

  if (GHOST_INKY_TRAILER_POSITION_X < -185) {
    GHOST_INKY_TRAILER_DIRECTION = 1;
    GHOST_INKY_TRAILER_STATE = 1;
  }

  if (GHOST_CLYDE_TRAILER_POSITION_X < -150) {
    GHOST_CLYDE_TRAILER_DIRECTION = 1;
    GHOST_CLYDE_TRAILER_STATE = 1;
  }

  drawPacmanTrailer();
  drawGhostsTrailer();

  if (HOME_TRAILER_STATE === 750) {
    clearInterval(HOME_TRAILER_TIMER);
    HOME_TRAILER_TIMER = -1;
  } else {
    HOME_TRAILER_STATE++;
  }
}

function getGhostsTrailerCanevasContext() {
  return PACMAN_TRAILER_CANVAS_CONTEXT;
}

function drawGhostsTrailer() {
  var ctx = getGhostsTrailerCanevasContext();

  if (GHOST_BLINKY_TRAILER_STATE === 1) {
    ctx.fillStyle = GHOST_AFFRAID_COLOR;
  } else {
    ctx.fillStyle = GHOST_BLINKY_COLOR;
  }

  drawHelperGhost(ctx, GHOST_BLINKY_TRAILER_POSITION_X, GHOST_BLINKY_TRAILER_POSITION_Y, GHOST_BLINKY_TRAILER_DIRECTION, GHOST_BLINKY_TRAILER_BODY_STATE, GHOST_BLINKY_TRAILER_STATE, 0);

  if (GHOST_PINKY_TRAILER_STATE === 1) {
    ctx.fillStyle = GHOST_AFFRAID_COLOR;
  } else {
    ctx.fillStyle = GHOST_PINKY_COLOR;
  }

  drawHelperGhost(ctx, GHOST_PINKY_TRAILER_POSITION_X, GHOST_PINKY_TRAILER_POSITION_Y, GHOST_PINKY_TRAILER_DIRECTION, GHOST_PINKY_TRAILER_BODY_STATE, GHOST_PINKY_TRAILER_STATE, 0);

  if (GHOST_INKY_TRAILER_STATE === 1) {
    ctx.fillStyle = GHOST_AFFRAID_COLOR;
  } else {
    ctx.fillStyle = GHOST_INKY_COLOR;
  }

  drawHelperGhost(ctx, GHOST_INKY_TRAILER_POSITION_X, GHOST_INKY_TRAILER_POSITION_Y, GHOST_INKY_TRAILER_DIRECTION, GHOST_INKY_TRAILER_BODY_STATE, GHOST_INKY_TRAILER_STATE, 0);

  if (GHOST_CLYDE_TRAILER_STATE === 1) {
    ctx.fillStyle = GHOST_AFFRAID_COLOR;
  } else {
    ctx.fillStyle = GHOST_CLYDE_COLOR;
  }

  drawHelperGhost(ctx, GHOST_CLYDE_TRAILER_POSITION_X, GHOST_CLYDE_TRAILER_POSITION_Y, GHOST_CLYDE_TRAILER_DIRECTION, GHOST_CLYDE_TRAILER_BODY_STATE, GHOST_CLYDE_TRAILER_STATE, 0);
}

function eraseGhostsTrailer(ghost) {
  var ctx = getGhostsTrailerCanevasContext();
  ctx.clearRect(GHOST_BLINKY_TRAILER_POSITION_X - 17, GHOST_BLINKY_TRAILER_POSITION_Y - 17, 34, 34);
  ctx.clearRect(GHOST_PINKY_TRAILER_POSITION_X - 17, GHOST_BLINKY_TRAILER_POSITION_Y - 17, 34, 34);
  ctx.clearRect(GHOST_INKY_TRAILER_POSITION_X - 17, GHOST_BLINKY_TRAILER_POSITION_Y - 17, 34, 34);
  ctx.clearRect(GHOST_CLYDE_TRAILER_POSITION_X - 17, GHOST_BLINKY_TRAILER_POSITION_Y - 17, 34, 34);
}

function getPacmanTrailerCanevasContext() {
  return PACMAN_TRAILER_CANVAS_CONTEXT;
}

function drawPacmanTrailer() {
  var ctx = getPacmanTrailerCanevasContext();
  ctx.fillStyle = "#fff200";
  ctx.beginPath();
  var startAngle = 0;
  var endAngle = 2 * Math.PI;
  var lineToX = PACMAN_TRAILER_POSITION_X;
  var lineToY = PACMAN_TRAILER_POSITION_Y;

  if (PACMAN_TRAILER_DIRECTION === 1) {
    startAngle = (0.35 - PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (1.65 + PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    lineToX -= 8;
  } else if (PACMAN_TRAILER_DIRECTION === 2) {
    startAngle = (0.85 - PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (0.15 + PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    lineToY -= 8;
  } else if (PACMAN_TRAILER_DIRECTION === 3) {
    startAngle = (1.35 - PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (0.65 + PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    lineToX += 8;
  } else if (PACMAN_TRAILER_DIRECTION === 4) {
    startAngle = (1.85 - PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (1.15 + PACMAN_TRAILER_MOUNTH_STATE * 0.05) * Math.PI;
    lineToY += 8;
  }

  ctx.arc(PACMAN_TRAILER_POSITION_X, PACMAN_TRAILER_POSITION_Y, PACMAN_TRAILER_SIZE, startAngle, endAngle, false);
  ctx.lineTo(lineToX, lineToY);
  ctx.fill();
  ctx.closePath();
}

function erasePacmanTrailer() {
  var ctx = getPacmanTrailerCanevasContext();
  ctx.clearRect(PACMAN_TRAILER_POSITION_X - PACMAN_TRAILER_SIZE, PACMAN_TRAILER_POSITION_Y - PACMAN_TRAILER_SIZE, PACMAN_TRAILER_SIZE * 2, PACMAN_TRAILER_SIZE * 2);
}

/***/ }),

/***/ "./public/game-6/js/jquery-buzz.js":
/*!*****************************************!*\
  !*** ./public/game-6/js/jquery-buzz.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// ----------------------------------------------------------------------------
// Buzz, a Javascript HTML5 Audio library
// v1.2.0 - Built 2016-05-22 15:16
// Licensed under the MIT license.
// http://buzz.jaysalvat.com/
// ----------------------------------------------------------------------------
// Copyright (C) 2010-2016 Jay Salvat
// http://jaysalvat.com/
// ----------------------------------------------------------------------------
(function (context, factory) {
  "use strict";

  if ( true && module.exports) {
    module.exports = factory();
  } else if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(this, function () {
  "use strict";

  var AudioContext = window.AudioContext || window.webkitAudioContext;
  var buzz = {
    defaults: {
      autoplay: false,
      crossOrigin: null,
      duration: 5e3,
      formats: [],
      loop: false,
      placeholder: "--",
      preload: "metadata",
      volume: 80,
      webAudioApi: false,
      document: window.document
    },
    types: {
      mp3: "audio/mpeg",
      ogg: "audio/ogg",
      wav: "audio/wav",
      aac: "audio/aac",
      m4a: "audio/x-m4a"
    },
    sounds: [],
    el: document.createElement("audio"),
    getAudioContext: function getAudioContext() {
      if (this.audioCtx === undefined) {
        try {
          this.audioCtx = AudioContext ? new AudioContext() : null;
        } catch (e) {
          this.audioCtx = null;
        }
      }

      return this.audioCtx;
    },
    sound: function sound(src, options) {
      options = options || {};
      var doc = options.document || buzz.defaults.document;
      var pid = 0,
          events = [],
          eventsOnce = {},
          supported = buzz.isSupported();

      this.load = function () {
        if (!supported) {
          return this;
        }

        this.sound.load();
        return this;
      };

      this.play = function () {
        if (!supported) {
          return this;
        }

        this.sound.play();
        return this;
      };

      this.togglePlay = function () {
        if (!supported) {
          return this;
        }

        if (this.sound.paused) {
          this.sound.play();
        } else {
          this.sound.pause();
        }

        return this;
      };

      this.pause = function () {
        if (!supported) {
          return this;
        }

        this.sound.pause();
        return this;
      };

      this.isPaused = function () {
        if (!supported) {
          return null;
        }

        return this.sound.paused;
      };

      this.stop = function () {
        if (!supported) {
          return this;
        }

        this.setTime(0);
        this.sound.pause();
        return this;
      };

      this.isEnded = function () {
        if (!supported) {
          return null;
        }

        return this.sound.ended;
      };

      this.loop = function () {
        if (!supported) {
          return this;
        }

        this.sound.loop = "loop";
        this.bind("ended.buzzloop", function () {
          this.currentTime = 0;
          this.play();
        });
        return this;
      };

      this.unloop = function () {
        if (!supported) {
          return this;
        }

        this.sound.removeAttribute("loop");
        this.unbind("ended.buzzloop");
        return this;
      };

      this.mute = function () {
        if (!supported) {
          return this;
        }

        this.sound.muted = true;
        return this;
      };

      this.unmute = function () {
        if (!supported) {
          return this;
        }

        this.sound.muted = false;
        return this;
      };

      this.toggleMute = function () {
        if (!supported) {
          return this;
        }

        this.sound.muted = !this.sound.muted;
        return this;
      };

      this.isMuted = function () {
        if (!supported) {
          return null;
        }

        return this.sound.muted;
      };

      this.setVolume = function (volume) {
        if (!supported) {
          return this;
        }

        if (volume < 0) {
          volume = 0;
        }

        if (volume > 100) {
          volume = 100;
        }

        this.volume = volume;
        this.sound.volume = volume / 100;
        return this;
      };

      this.getVolume = function () {
        if (!supported) {
          return this;
        }

        return this.volume;
      };

      this.increaseVolume = function (value) {
        return this.setVolume(this.volume + (value || 1));
      };

      this.decreaseVolume = function (value) {
        return this.setVolume(this.volume - (value || 1));
      };

      this.setTime = function (time) {
        if (!supported) {
          return this;
        }

        var set = true;
        this.whenReady(function () {
          if (set === true) {
            set = false;
            this.sound.currentTime = time;
          }
        });
        return this;
      };

      this.getTime = function () {
        if (!supported) {
          return null;
        }

        var time = Math.round(this.sound.currentTime * 100) / 100;
        return isNaN(time) ? buzz.defaults.placeholder : time;
      };

      this.setPercent = function (percent) {
        if (!supported) {
          return this;
        }

        return this.setTime(buzz.fromPercent(percent, this.sound.duration));
      };

      this.getPercent = function () {
        if (!supported) {
          return null;
        }

        var percent = Math.round(buzz.toPercent(this.sound.currentTime, this.sound.duration));
        return isNaN(percent) ? buzz.defaults.placeholder : percent;
      };

      this.setSpeed = function (duration) {
        if (!supported) {
          return this;
        }

        this.sound.playbackRate = duration;
        return this;
      };

      this.getSpeed = function () {
        if (!supported) {
          return null;
        }

        return this.sound.playbackRate;
      };

      this.getDuration = function () {
        if (!supported) {
          return null;
        }

        var duration = Math.round(this.sound.duration * 100) / 100;
        return isNaN(duration) ? buzz.defaults.placeholder : duration;
      };

      this.getPlayed = function () {
        if (!supported) {
          return null;
        }

        return timerangeToArray(this.sound.played);
      };

      this.getBuffered = function () {
        if (!supported) {
          return null;
        }

        return timerangeToArray(this.sound.buffered);
      };

      this.getSeekable = function () {
        if (!supported) {
          return null;
        }

        return timerangeToArray(this.sound.seekable);
      };

      this.getErrorCode = function () {
        if (supported && this.sound.error) {
          return this.sound.error.code;
        }

        return 0;
      };

      this.getErrorMessage = function () {
        if (!supported) {
          return null;
        }

        switch (this.getErrorCode()) {
          case 1:
            return "MEDIA_ERR_ABORTED";

          case 2:
            return "MEDIA_ERR_NETWORK";

          case 3:
            return "MEDIA_ERR_DECODE";

          case 4:
            return "MEDIA_ERR_SRC_NOT_SUPPORTED";

          default:
            return null;
        }
      };

      this.getStateCode = function () {
        if (!supported) {
          return null;
        }

        return this.sound.readyState;
      };

      this.getStateMessage = function () {
        if (!supported) {
          return null;
        }

        switch (this.getStateCode()) {
          case 0:
            return "HAVE_NOTHING";

          case 1:
            return "HAVE_METADATA";

          case 2:
            return "HAVE_CURRENT_DATA";

          case 3:
            return "HAVE_FUTURE_DATA";

          case 4:
            return "HAVE_ENOUGH_DATA";

          default:
            return null;
        }
      };

      this.getNetworkStateCode = function () {
        if (!supported) {
          return null;
        }

        return this.sound.networkState;
      };

      this.getNetworkStateMessage = function () {
        if (!supported) {
          return null;
        }

        switch (this.getNetworkStateCode()) {
          case 0:
            return "NETWORK_EMPTY";

          case 1:
            return "NETWORK_IDLE";

          case 2:
            return "NETWORK_LOADING";

          case 3:
            return "NETWORK_NO_SOURCE";

          default:
            return null;
        }
      };

      this.set = function (key, value) {
        if (!supported) {
          return this;
        }

        this.sound[key] = value;
        return this;
      };

      this.get = function (key) {
        if (!supported) {
          return null;
        }

        return key ? this.sound[key] : this.sound;
      };

      this.bind = function (types, func) {
        if (!supported) {
          return this;
        }

        types = types.split(" ");

        var self = this,
            efunc = function efunc(e) {
          func.call(self, e);
        };

        for (var t = 0; t < types.length; t++) {
          var type = types[t],
              idx = type;
          type = idx.split(".")[0];
          events.push({
            idx: idx,
            func: efunc
          });
          this.sound.addEventListener(type, efunc, true);
        }

        return this;
      };

      this.unbind = function (types) {
        if (!supported) {
          return this;
        }

        types = types.split(" ");

        for (var t = 0; t < types.length; t++) {
          var idx = types[t],
              type = idx.split(".")[0];

          for (var i = 0; i < events.length; i++) {
            var namespace = events[i].idx.split(".");

            if (events[i].idx === idx || namespace[1] && namespace[1] === idx.replace(".", "")) {
              this.sound.removeEventListener(type, events[i].func, true);
              events.splice(i, 1);
            }
          }
        }

        return this;
      };

      this.bindOnce = function (type, func) {
        if (!supported) {
          return this;
        }

        var self = this;
        eventsOnce[pid++] = false;
        this.bind(type + "." + pid, function () {
          if (!eventsOnce[pid]) {
            eventsOnce[pid] = true;
            func.call(self);
          }

          self.unbind(type + "." + pid);
        });
        return this;
      };

      this.trigger = function (types, detail) {
        if (!supported) {
          return this;
        }

        types = types.split(" ");

        for (var t = 0; t < types.length; t++) {
          var idx = types[t];

          for (var i = 0; i < events.length; i++) {
            var eventType = events[i].idx.split(".");

            if (events[i].idx === idx || eventType[0] && eventType[0] === idx.replace(".", "")) {
              var evt = doc.createEvent("HTMLEvents");
              evt.initEvent(eventType[0], false, true);
              evt.originalEvent = detail;
              this.sound.dispatchEvent(evt);
            }
          }
        }

        return this;
      };

      this.fadeTo = function (to, duration, callback) {
        if (!supported) {
          return this;
        }

        if (duration instanceof Function) {
          callback = duration;
          duration = buzz.defaults.duration;
        } else {
          duration = duration || buzz.defaults.duration;
        }

        var from = this.volume,
            delay = duration / Math.abs(from - to),
            self = this,
            fadeToTimeout;
        this.play();

        function doFade() {
          clearTimeout(fadeToTimeout);
          fadeToTimeout = setTimeout(function () {
            if (from < to && self.volume < to) {
              self.setVolume(self.volume += 1);
              doFade();
            } else if (from > to && self.volume > to) {
              self.setVolume(self.volume -= 1);
              doFade();
            } else if (callback instanceof Function) {
              callback.apply(self);
            }
          }, delay);
        }

        this.whenReady(function () {
          doFade();
        });
        return this;
      };

      this.fadeIn = function (duration, callback) {
        if (!supported) {
          return this;
        }

        return this.setVolume(0).fadeTo(100, duration, callback);
      };

      this.fadeOut = function (duration, callback) {
        if (!supported) {
          return this;
        }

        return this.fadeTo(0, duration, callback);
      };

      this.fadeWith = function (sound, duration) {
        if (!supported) {
          return this;
        }

        this.fadeOut(duration, function () {
          this.stop();
        });
        sound.play().fadeIn(duration);
        return this;
      };

      this.whenReady = function (func) {
        if (!supported) {
          return null;
        }

        var self = this;

        if (this.sound.readyState === 0) {
          this.bind("canplay.buzzwhenready", function () {
            func.call(self);
          });
        } else {
          func.call(self);
        }
      };

      this.addSource = function (src) {
        var self = this,
            source = doc.createElement("source");
        source.src = src;

        if (buzz.types[getExt(src)]) {
          source.type = buzz.types[getExt(src)];
        }

        this.sound.appendChild(source);
        source.addEventListener("error", function (e) {
          self.trigger("sourceerror", e);
        });
        return source;
      };

      function timerangeToArray(timeRange) {
        var array = [],
            length = timeRange.length - 1;

        for (var i = 0; i <= length; i++) {
          array.push({
            start: timeRange.start(i),
            end: timeRange.end(i)
          });
        }

        return array;
      }

      function getExt(filename) {
        return filename.split(".").pop();
      }

      if (supported && src) {
        for (var i in buzz.defaults) {
          if (buzz.defaults.hasOwnProperty(i)) {
            if (options[i] === undefined) {
              options[i] = buzz.defaults[i];
            }
          }
        }

        this.sound = doc.createElement("audio");

        if (options.crossOrigin !== null) {
          this.sound.crossOrigin = options.crossOrigin;
        }

        if (options.webAudioApi) {
          var audioCtx = buzz.getAudioContext();

          if (audioCtx) {
            this.source = audioCtx.createMediaElementSource(this.sound);
            this.source.connect(audioCtx.destination);
          }
        }

        if (src instanceof Array) {
          for (var j in src) {
            if (src.hasOwnProperty(j)) {
              this.addSource(src[j]);
            }
          }
        } else if (options.formats.length) {
          for (var k in options.formats) {
            if (options.formats.hasOwnProperty(k)) {
              this.addSource(src + "." + options.formats[k]);
            }
          }
        } else {
          this.addSource(src);
        }

        if (options.loop) {
          this.loop();
        }

        if (options.autoplay) {
          this.sound.autoplay = "autoplay";
        }

        if (options.preload === true) {
          this.sound.preload = "auto";
        } else if (options.preload === false) {
          this.sound.preload = "none";
        } else {
          this.sound.preload = options.preload;
        }

        this.setVolume(options.volume);
        buzz.sounds.push(this);
      }
    },
    group: function group(sounds) {
      sounds = argsToArray(sounds, arguments);

      this.getSounds = function () {
        return sounds;
      };

      this.add = function (soundArray) {
        soundArray = argsToArray(soundArray, arguments);

        for (var a = 0; a < soundArray.length; a++) {
          sounds.push(soundArray[a]);
        }
      };

      this.remove = function (soundArray) {
        soundArray = argsToArray(soundArray, arguments);

        for (var a = 0; a < soundArray.length; a++) {
          for (var i = 0; i < sounds.length; i++) {
            if (sounds[i] === soundArray[a]) {
              sounds.splice(i, 1);
              break;
            }
          }
        }
      };

      this.load = function () {
        fn("load");
        return this;
      };

      this.play = function () {
        fn("play");
        return this;
      };

      this.togglePlay = function () {
        fn("togglePlay");
        return this;
      };

      this.pause = function (time) {
        fn("pause", time);
        return this;
      };

      this.stop = function () {
        fn("stop");
        return this;
      };

      this.mute = function () {
        fn("mute");
        return this;
      };

      this.unmute = function () {
        fn("unmute");
        return this;
      };

      this.toggleMute = function () {
        fn("toggleMute");
        return this;
      };

      this.setVolume = function (volume) {
        fn("setVolume", volume);
        return this;
      };

      this.increaseVolume = function (value) {
        fn("increaseVolume", value);
        return this;
      };

      this.decreaseVolume = function (value) {
        fn("decreaseVolume", value);
        return this;
      };

      this.loop = function () {
        fn("loop");
        return this;
      };

      this.unloop = function () {
        fn("unloop");
        return this;
      };

      this.setSpeed = function (speed) {
        fn("setSpeed", speed);
        return this;
      };

      this.setTime = function (time) {
        fn("setTime", time);
        return this;
      };

      this.set = function (key, value) {
        fn("set", key, value);
        return this;
      };

      this.bind = function (type, func) {
        fn("bind", type, func);
        return this;
      };

      this.unbind = function (type) {
        fn("unbind", type);
        return this;
      };

      this.bindOnce = function (type, func) {
        fn("bindOnce", type, func);
        return this;
      };

      this.trigger = function (type) {
        fn("trigger", type);
        return this;
      };

      this.fade = function (from, to, duration, callback) {
        fn("fade", from, to, duration, callback);
        return this;
      };

      this.fadeIn = function (duration, callback) {
        fn("fadeIn", duration, callback);
        return this;
      };

      this.fadeOut = function (duration, callback) {
        fn("fadeOut", duration, callback);
        return this;
      };

      function fn() {
        var args = argsToArray(null, arguments),
            func = args.shift();

        for (var i = 0; i < sounds.length; i++) {
          sounds[i][func].apply(sounds[i], args);
        }
      }

      function argsToArray(array, args) {
        return array instanceof Array ? array : Array.prototype.slice.call(args);
      }
    },
    all: function all() {
      return new buzz.group(buzz.sounds);
    },
    isSupported: function isSupported() {
      return !!buzz.el.canPlayType;
    },
    isOGGSupported: function isOGGSupported() {
      return !!buzz.el.canPlayType && buzz.el.canPlayType('audio/ogg; codecs="vorbis"');
    },
    isWAVSupported: function isWAVSupported() {
      return !!buzz.el.canPlayType && buzz.el.canPlayType('audio/wav; codecs="1"');
    },
    isMP3Supported: function isMP3Supported() {
      return !!buzz.el.canPlayType && buzz.el.canPlayType("audio/mpeg;");
    },
    isAACSupported: function isAACSupported() {
      return !!buzz.el.canPlayType && (buzz.el.canPlayType("audio/x-m4a;") || buzz.el.canPlayType("audio/aac;"));
    },
    toTimer: function toTimer(time, withHours) {
      var h, m, s;
      h = Math.floor(time / 3600);
      h = isNaN(h) ? "--" : h >= 10 ? h : "0" + h;
      m = withHours ? Math.floor(time / 60 % 60) : Math.floor(time / 60);
      m = isNaN(m) ? "--" : m >= 10 ? m : "0" + m;
      s = Math.floor(time % 60);
      s = isNaN(s) ? "--" : s >= 10 ? s : "0" + s;
      return withHours ? h + ":" + m + ":" + s : m + ":" + s;
    },
    fromTimer: function fromTimer(time) {
      var splits = time.toString().split(":");

      if (splits && splits.length === 3) {
        time = parseInt(splits[0], 10) * 3600 + parseInt(splits[1], 10) * 60 + parseInt(splits[2], 10);
      }

      if (splits && splits.length === 2) {
        time = parseInt(splits[0], 10) * 60 + parseInt(splits[1], 10);
      }

      return time;
    },
    toPercent: function toPercent(value, total, decimal) {
      var r = Math.pow(10, decimal || 0);
      return Math.round(value * 100 / total * r) / r;
    },
    fromPercent: function fromPercent(percent, total, decimal) {
      var r = Math.pow(10, decimal || 0);
      return Math.round(total / 100 * percent * r) / r;
    }
  };
  return buzz;
});

/***/ }),

/***/ "./public/game-6/js/jquery.js":
/*!************************************!*\
  !*** ./public/game-6/js/jquery.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! jQuery v1.8.3 jquery.com | jquery.org/license */
(function (e, t) {
  function _(e) {
    var t = M[e] = {};
    return v.each(e.split(y), function (e, n) {
      t[n] = !0;
    }), t;
  }

  function H(e, n, r) {
    if (r === t && e.nodeType === 1) {
      var i = "data-" + n.replace(P, "-$1").toLowerCase();
      r = e.getAttribute(i);

      if (typeof r == "string") {
        try {
          r = r === "true" ? !0 : r === "false" ? !1 : r === "null" ? null : +r + "" === r ? +r : D.test(r) ? v.parseJSON(r) : r;
        } catch (s) {}

        v.data(e, n, r);
      } else r = t;
    }

    return r;
  }

  function B(e) {
    var t;

    for (t in e) {
      if (t === "data" && v.isEmptyObject(e[t])) continue;
      if (t !== "toJSON") return !1;
    }

    return !0;
  }

  function et() {
    return !1;
  }

  function tt() {
    return !0;
  }

  function ut(e) {
    return !e || !e.parentNode || e.parentNode.nodeType === 11;
  }

  function at(e, t) {
    do {
      e = e[t];
    } while (e && e.nodeType !== 1);

    return e;
  }

  function ft(e, t, n) {
    t = t || 0;
    if (v.isFunction(t)) return v.grep(e, function (e, r) {
      var i = !!t.call(e, r, e);
      return i === n;
    });
    if (t.nodeType) return v.grep(e, function (e, r) {
      return e === t === n;
    });

    if (typeof t == "string") {
      var r = v.grep(e, function (e) {
        return e.nodeType === 1;
      });
      if (it.test(t)) return v.filter(t, r, !n);
      t = v.filter(t, r);
    }

    return v.grep(e, function (e, r) {
      return v.inArray(e, t) >= 0 === n;
    });
  }

  function lt(e) {
    var t = ct.split("|"),
        n = e.createDocumentFragment();
    if (n.createElement) while (t.length) {
      n.createElement(t.pop());
    }
    return n;
  }

  function Lt(e, t) {
    return e.getElementsByTagName(t)[0] || e.appendChild(e.ownerDocument.createElement(t));
  }

  function At(e, t) {
    if (t.nodeType !== 1 || !v.hasData(e)) return;

    var n,
        r,
        i,
        s = v._data(e),
        o = v._data(t, s),
        u = s.events;

    if (u) {
      delete o.handle, o.events = {};

      for (n in u) {
        for (r = 0, i = u[n].length; r < i; r++) {
          v.event.add(t, n, u[n][r]);
        }
      }
    }

    o.data && (o.data = v.extend({}, o.data));
  }

  function Ot(e, t) {
    var n;
    if (t.nodeType !== 1) return;
    t.clearAttributes && t.clearAttributes(), t.mergeAttributes && t.mergeAttributes(e), n = t.nodeName.toLowerCase(), n === "object" ? (t.parentNode && (t.outerHTML = e.outerHTML), v.support.html5Clone && e.innerHTML && !v.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : n === "input" && Et.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : n === "option" ? t.selected = e.defaultSelected : n === "input" || n === "textarea" ? t.defaultValue = e.defaultValue : n === "script" && t.text !== e.text && (t.text = e.text), t.removeAttribute(v.expando);
  }

  function Mt(e) {
    return typeof e.getElementsByTagName != "undefined" ? e.getElementsByTagName("*") : typeof e.querySelectorAll != "undefined" ? e.querySelectorAll("*") : [];
  }

  function _t(e) {
    Et.test(e.type) && (e.defaultChecked = e.checked);
  }

  function Qt(e, t) {
    if (t in e) return t;
    var n = t.charAt(0).toUpperCase() + t.slice(1),
        r = t,
        i = Jt.length;

    while (i--) {
      t = Jt[i] + n;
      if (t in e) return t;
    }

    return r;
  }

  function Gt(e, t) {
    return e = t || e, v.css(e, "display") === "none" || !v.contains(e.ownerDocument, e);
  }

  function Yt(e, t) {
    var n,
        r,
        i = [],
        s = 0,
        o = e.length;

    for (; s < o; s++) {
      n = e[s];
      if (!n.style) continue;
      i[s] = v._data(n, "olddisplay"), t ? (!i[s] && n.style.display === "none" && (n.style.display = ""), n.style.display === "" && Gt(n) && (i[s] = v._data(n, "olddisplay", nn(n.nodeName)))) : (r = Dt(n, "display"), !i[s] && r !== "none" && v._data(n, "olddisplay", r));
    }

    for (s = 0; s < o; s++) {
      n = e[s];
      if (!n.style) continue;
      if (!t || n.style.display === "none" || n.style.display === "") n.style.display = t ? i[s] || "" : "none";
    }

    return e;
  }

  function Zt(e, t, n) {
    var r = Rt.exec(t);
    return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t;
  }

  function en(e, t, n, r) {
    var i = n === (r ? "border" : "content") ? 4 : t === "width" ? 1 : 0,
        s = 0;

    for (; i < 4; i += 2) {
      n === "margin" && (s += v.css(e, n + $t[i], !0)), r ? (n === "content" && (s -= parseFloat(Dt(e, "padding" + $t[i])) || 0), n !== "margin" && (s -= parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0)) : (s += parseFloat(Dt(e, "padding" + $t[i])) || 0, n !== "padding" && (s += parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0));
    }

    return s;
  }

  function tn(e, t, n) {
    var r = t === "width" ? e.offsetWidth : e.offsetHeight,
        i = !0,
        s = v.support.boxSizing && v.css(e, "boxSizing") === "border-box";

    if (r <= 0 || r == null) {
      r = Dt(e, t);
      if (r < 0 || r == null) r = e.style[t];
      if (Ut.test(r)) return r;
      i = s && (v.support.boxSizingReliable || r === e.style[t]), r = parseFloat(r) || 0;
    }

    return r + en(e, t, n || (s ? "border" : "content"), i) + "px";
  }

  function nn(e) {
    if (Wt[e]) return Wt[e];
    var t = v("<" + e + ">").appendTo(i.body),
        n = t.css("display");
    t.remove();

    if (n === "none" || n === "") {
      Pt = i.body.appendChild(Pt || v.extend(i.createElement("iframe"), {
        frameBorder: 0,
        width: 0,
        height: 0
      }));
      if (!Ht || !Pt.createElement) Ht = (Pt.contentWindow || Pt.contentDocument).document, Ht.write("<!doctype html><html><body>"), Ht.close();
      t = Ht.body.appendChild(Ht.createElement(e)), n = Dt(t, "display"), i.body.removeChild(Pt);
    }

    return Wt[e] = n, n;
  }

  function fn(e, t, n, r) {
    var i;
    if (v.isArray(t)) v.each(t, function (t, i) {
      n || sn.test(e) ? r(e, i) : fn(e + "[" + (_typeof(i) == "object" ? t : "") + "]", i, n, r);
    });else if (!n && v.type(t) === "object") for (i in t) {
      fn(e + "[" + i + "]", t[i], n, r);
    } else r(e, t);
  }

  function Cn(e) {
    return function (t, n) {
      typeof t != "string" && (n = t, t = "*");
      var r,
          i,
          s,
          o = t.toLowerCase().split(y),
          u = 0,
          a = o.length;
      if (v.isFunction(n)) for (; u < a; u++) {
        r = o[u], s = /^\+/.test(r), s && (r = r.substr(1) || "*"), i = e[r] = e[r] || [], i[s ? "unshift" : "push"](n);
      }
    };
  }

  function kn(e, n, r, i, s, o) {
    s = s || n.dataTypes[0], o = o || {}, o[s] = !0;
    var u,
        a = e[s],
        f = 0,
        l = a ? a.length : 0,
        c = e === Sn;

    for (; f < l && (c || !u); f++) {
      u = a[f](n, r, i), typeof u == "string" && (!c || o[u] ? u = t : (n.dataTypes.unshift(u), u = kn(e, n, r, i, u, o)));
    }

    return (c || !u) && !o["*"] && (u = kn(e, n, r, i, "*", o)), u;
  }

  function Ln(e, n) {
    var r,
        i,
        s = v.ajaxSettings.flatOptions || {};

    for (r in n) {
      n[r] !== t && ((s[r] ? e : i || (i = {}))[r] = n[r]);
    }

    i && v.extend(!0, e, i);
  }

  function An(e, n, r) {
    var i,
        s,
        o,
        u,
        a = e.contents,
        f = e.dataTypes,
        l = e.responseFields;

    for (s in l) {
      s in r && (n[l[s]] = r[s]);
    }

    while (f[0] === "*") {
      f.shift(), i === t && (i = e.mimeType || n.getResponseHeader("content-type"));
    }

    if (i) for (s in a) {
      if (a[s] && a[s].test(i)) {
        f.unshift(s);
        break;
      }
    }
    if (f[0] in r) o = f[0];else {
      for (s in r) {
        if (!f[0] || e.converters[s + " " + f[0]]) {
          o = s;
          break;
        }

        u || (u = s);
      }

      o = o || u;
    }
    if (o) return o !== f[0] && f.unshift(o), r[o];
  }

  function On(e, t) {
    var n,
        r,
        i,
        s,
        o = e.dataTypes.slice(),
        u = o[0],
        a = {},
        f = 0;
    e.dataFilter && (t = e.dataFilter(t, e.dataType));
    if (o[1]) for (n in e.converters) {
      a[n.toLowerCase()] = e.converters[n];
    }

    for (; i = o[++f];) {
      if (i !== "*") {
        if (u !== "*" && u !== i) {
          n = a[u + " " + i] || a["* " + i];
          if (!n) for (r in a) {
            s = r.split(" ");

            if (s[1] === i) {
              n = a[u + " " + s[0]] || a["* " + s[0]];

              if (n) {
                n === !0 ? n = a[r] : a[r] !== !0 && (i = s[0], o.splice(f--, 0, i));
                break;
              }
            }
          }
          if (n !== !0) if (n && e["throws"]) t = n(t);else try {
            t = n(t);
          } catch (l) {
            return {
              state: "parsererror",
              error: n ? l : "No conversion from " + u + " to " + i
            };
          }
        }

        u = i;
      }
    }

    return {
      state: "success",
      data: t
    };
  }

  function Fn() {
    try {
      return new e.XMLHttpRequest();
    } catch (t) {}
  }

  function In() {
    try {
      return new e.ActiveXObject("Microsoft.XMLHTTP");
    } catch (t) {}
  }

  function $n() {
    return setTimeout(function () {
      qn = t;
    }, 0), qn = v.now();
  }

  function Jn(e, t) {
    v.each(t, function (t, n) {
      var r = (Vn[t] || []).concat(Vn["*"]),
          i = 0,
          s = r.length;

      for (; i < s; i++) {
        if (r[i].call(e, t, n)) return;
      }
    });
  }

  function Kn(e, t, n) {
    var r,
        i = 0,
        s = 0,
        o = Xn.length,
        u = v.Deferred().always(function () {
      delete a.elem;
    }),
        a = function a() {
      var t = qn || $n(),
          n = Math.max(0, f.startTime + f.duration - t),
          r = n / f.duration || 0,
          i = 1 - r,
          s = 0,
          o = f.tweens.length;

      for (; s < o; s++) {
        f.tweens[s].run(i);
      }

      return u.notifyWith(e, [f, i, n]), i < 1 && o ? n : (u.resolveWith(e, [f]), !1);
    },
        f = u.promise({
      elem: e,
      props: v.extend({}, t),
      opts: v.extend(!0, {
        specialEasing: {}
      }, n),
      originalProperties: t,
      originalOptions: n,
      startTime: qn || $n(),
      duration: n.duration,
      tweens: [],
      createTween: function createTween(t, n, r) {
        var i = v.Tween(e, f.opts, t, n, f.opts.specialEasing[t] || f.opts.easing);
        return f.tweens.push(i), i;
      },
      stop: function stop(t) {
        var n = 0,
            r = t ? f.tweens.length : 0;

        for (; n < r; n++) {
          f.tweens[n].run(1);
        }

        return t ? u.resolveWith(e, [f, t]) : u.rejectWith(e, [f, t]), this;
      }
    }),
        l = f.props;

    Qn(l, f.opts.specialEasing);

    for (; i < o; i++) {
      r = Xn[i].call(f, e, l, f.opts);
      if (r) return r;
    }

    return Jn(f, l), v.isFunction(f.opts.start) && f.opts.start.call(e, f), v.fx.timer(v.extend(a, {
      anim: f,
      queue: f.opts.queue,
      elem: e
    })), f.progress(f.opts.progress).done(f.opts.done, f.opts.complete).fail(f.opts.fail).always(f.opts.always);
  }

  function Qn(e, t) {
    var n, r, i, s, o;

    for (n in e) {
      r = v.camelCase(n), i = t[r], s = e[n], v.isArray(s) && (i = s[1], s = e[n] = s[0]), n !== r && (e[r] = s, delete e[n]), o = v.cssHooks[r];

      if (o && "expand" in o) {
        s = o.expand(s), delete e[r];

        for (n in s) {
          n in e || (e[n] = s[n], t[n] = i);
        }
      } else t[r] = i;
    }
  }

  function Gn(e, t, n) {
    var r,
        i,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h = this,
        p = e.style,
        d = {},
        m = [],
        g = e.nodeType && Gt(e);
    n.queue || (l = v._queueHooks(e, "fx"), l.unqueued == null && (l.unqueued = 0, c = l.empty.fire, l.empty.fire = function () {
      l.unqueued || c();
    }), l.unqueued++, h.always(function () {
      h.always(function () {
        l.unqueued--, v.queue(e, "fx").length || l.empty.fire();
      });
    })), e.nodeType === 1 && ("height" in t || "width" in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], v.css(e, "display") === "inline" && v.css(e, "float") === "none" && (!v.support.inlineBlockNeedsLayout || nn(e.nodeName) === "inline" ? p.display = "inline-block" : p.zoom = 1)), n.overflow && (p.overflow = "hidden", v.support.shrinkWrapBlocks || h.done(function () {
      p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2];
    }));

    for (r in t) {
      s = t[r];

      if (Un.exec(s)) {
        delete t[r], a = a || s === "toggle";
        if (s === (g ? "hide" : "show")) continue;
        m.push(r);
      }
    }

    o = m.length;

    if (o) {
      u = v._data(e, "fxshow") || v._data(e, "fxshow", {}), "hidden" in u && (g = u.hidden), a && (u.hidden = !g), g ? v(e).show() : h.done(function () {
        v(e).hide();
      }), h.done(function () {
        var t;
        v.removeData(e, "fxshow", !0);

        for (t in d) {
          v.style(e, t, d[t]);
        }
      });

      for (r = 0; r < o; r++) {
        i = m[r], f = h.createTween(i, g ? u[i] : 0), d[i] = u[i] || v.style(e, i), i in u || (u[i] = f.start, g && (f.end = f.start, f.start = i === "width" || i === "height" ? 1 : 0));
      }
    }
  }

  function Yn(e, t, n, r, i) {
    return new Yn.prototype.init(e, t, n, r, i);
  }

  function Zn(e, t) {
    var n,
        r = {
      height: e
    },
        i = 0;
    t = t ? 1 : 0;

    for (; i < 4; i += 2 - t) {
      n = $t[i], r["margin" + n] = r["padding" + n] = e;
    }

    return t && (r.opacity = r.width = e), r;
  }

  function tr(e) {
    return v.isWindow(e) ? e : e.nodeType === 9 ? e.defaultView || e.parentWindow : !1;
  }

  var n,
      r,
      i = e.document,
      s = e.location,
      o = e.navigator,
      u = e.jQuery,
      a = e.$,
      f = Array.prototype.push,
      l = Array.prototype.slice,
      c = Array.prototype.indexOf,
      h = Object.prototype.toString,
      p = Object.prototype.hasOwnProperty,
      d = String.prototype.trim,
      v = function v(e, t) {
    return new v.fn.init(e, t, n);
  },
      m = /[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,
      g = /\S/,
      y = /\s+/,
      b = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      w = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
      E = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
      S = /^[\],:{}\s]*$/,
      x = /(?:^|:|,)(?:\s*\[)+/g,
      T = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
      N = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,
      C = /^-ms-/,
      k = /-([\da-z])/gi,
      L = function L(e, t) {
    return (t + "").toUpperCase();
  },
      A = function A() {
    i.addEventListener ? (i.removeEventListener("DOMContentLoaded", A, !1), v.ready()) : i.readyState === "complete" && (i.detachEvent("onreadystatechange", A), v.ready());
  },
      O = {};

  v.fn = v.prototype = {
    constructor: v,
    init: function init(e, n, r) {
      var s, o, u, a;
      if (!e) return this;
      if (e.nodeType) return this.context = this[0] = e, this.length = 1, this;

      if (typeof e == "string") {
        e.charAt(0) === "<" && e.charAt(e.length - 1) === ">" && e.length >= 3 ? s = [null, e, null] : s = w.exec(e);

        if (s && (s[1] || !n)) {
          if (s[1]) return n = n instanceof v ? n[0] : n, a = n && n.nodeType ? n.ownerDocument || n : i, e = v.parseHTML(s[1], a, !0), E.test(s[1]) && v.isPlainObject(n) && this.attr.call(e, n, !0), v.merge(this, e);
          o = i.getElementById(s[2]);

          if (o && o.parentNode) {
            if (o.id !== s[2]) return r.find(e);
            this.length = 1, this[0] = o;
          }

          return this.context = i, this.selector = e, this;
        }

        return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e);
      }

      return v.isFunction(e) ? r.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), v.makeArray(e, this));
    },
    selector: "",
    jquery: "1.8.3",
    length: 0,
    size: function size() {
      return this.length;
    },
    toArray: function toArray() {
      return l.call(this);
    },
    get: function get(e) {
      return e == null ? this.toArray() : e < 0 ? this[this.length + e] : this[e];
    },
    pushStack: function pushStack(e, t, n) {
      var r = v.merge(this.constructor(), e);
      return r.prevObject = this, r.context = this.context, t === "find" ? r.selector = this.selector + (this.selector ? " " : "") + n : t && (r.selector = this.selector + "." + t + "(" + n + ")"), r;
    },
    each: function each(e, t) {
      return v.each(this, e, t);
    },
    ready: function ready(e) {
      return v.ready.promise().done(e), this;
    },
    eq: function eq(e) {
      return e = +e, e === -1 ? this.slice(e) : this.slice(e, e + 1);
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    slice: function slice() {
      return this.pushStack(l.apply(this, arguments), "slice", l.call(arguments).join(","));
    },
    map: function map(e) {
      return this.pushStack(v.map(this, function (t, n) {
        return e.call(t, n, t);
      }));
    },
    end: function end() {
      return this.prevObject || this.constructor(null);
    },
    push: f,
    sort: [].sort,
    splice: [].splice
  }, v.fn.init.prototype = v.fn, v.extend = v.fn.extend = function () {
    var e,
        n,
        r,
        i,
        s,
        o,
        u = arguments[0] || {},
        a = 1,
        f = arguments.length,
        l = !1;
    typeof u == "boolean" && (l = u, u = arguments[1] || {}, a = 2), _typeof(u) != "object" && !v.isFunction(u) && (u = {}), f === a && (u = this, --a);

    for (; a < f; a++) {
      if ((e = arguments[a]) != null) for (n in e) {
        r = u[n], i = e[n];
        if (u === i) continue;
        l && i && (v.isPlainObject(i) || (s = v.isArray(i))) ? (s ? (s = !1, o = r && v.isArray(r) ? r : []) : o = r && v.isPlainObject(r) ? r : {}, u[n] = v.extend(l, o, i)) : i !== t && (u[n] = i);
      }
    }

    return u;
  }, v.extend({
    noConflict: function noConflict(t) {
      return e.$ === v && (e.$ = a), t && e.jQuery === v && (e.jQuery = u), v;
    },
    isReady: !1,
    readyWait: 1,
    holdReady: function holdReady(e) {
      e ? v.readyWait++ : v.ready(!0);
    },
    ready: function ready(e) {
      if (e === !0 ? --v.readyWait : v.isReady) return;
      if (!i.body) return setTimeout(v.ready, 1);
      v.isReady = !0;
      if (e !== !0 && --v.readyWait > 0) return;
      r.resolveWith(i, [v]), v.fn.trigger && v(i).trigger("ready").off("ready");
    },
    isFunction: function isFunction(e) {
      return v.type(e) === "function";
    },
    isArray: Array.isArray || function (e) {
      return v.type(e) === "array";
    },
    isWindow: function isWindow(e) {
      return e != null && e == e.window;
    },
    isNumeric: function isNumeric(e) {
      return !isNaN(parseFloat(e)) && isFinite(e);
    },
    type: function type(e) {
      return e == null ? String(e) : O[h.call(e)] || "object";
    },
    isPlainObject: function isPlainObject(e) {
      if (!e || v.type(e) !== "object" || e.nodeType || v.isWindow(e)) return !1;

      try {
        if (e.constructor && !p.call(e, "constructor") && !p.call(e.constructor.prototype, "isPrototypeOf")) return !1;
      } catch (n) {
        return !1;
      }

      var r;

      for (r in e) {
        ;
      }

      return r === t || p.call(e, r);
    },
    isEmptyObject: function isEmptyObject(e) {
      var t;

      for (t in e) {
        return !1;
      }

      return !0;
    },
    error: function error(e) {
      throw new Error(e);
    },
    parseHTML: function parseHTML(e, t, n) {
      var r;
      return !e || typeof e != "string" ? null : (typeof t == "boolean" && (n = t, t = 0), t = t || i, (r = E.exec(e)) ? [t.createElement(r[1])] : (r = v.buildFragment([e], t, n ? null : []), v.merge([], (r.cacheable ? v.clone(r.fragment) : r.fragment).childNodes)));
    },
    parseJSON: function parseJSON(t) {
      if (!t || typeof t != "string") return null;
      t = v.trim(t);
      if (e.JSON && e.JSON.parse) return e.JSON.parse(t);
      if (S.test(t.replace(T, "@").replace(N, "]").replace(x, ""))) return new Function("return " + t)();
      v.error("Invalid JSON: " + t);
    },
    parseXML: function parseXML(n) {
      var r, i;
      if (!n || typeof n != "string") return null;

      try {
        e.DOMParser ? (i = new DOMParser(), r = i.parseFromString(n, "text/xml")) : (r = new ActiveXObject("Microsoft.XMLDOM"), r.async = "false", r.loadXML(n));
      } catch (s) {
        r = t;
      }

      return (!r || !r.documentElement || r.getElementsByTagName("parsererror").length) && v.error("Invalid XML: " + n), r;
    },
    noop: function noop() {},
    globalEval: function globalEval(t) {
      t && g.test(t) && (e.execScript || function (t) {
        e.eval.call(e, t);
      })(t);
    },
    camelCase: function camelCase(e) {
      return e.replace(C, "ms-").replace(k, L);
    },
    nodeName: function nodeName(e, t) {
      return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
    },
    each: function each(e, n, r) {
      var i,
          s = 0,
          o = e.length,
          u = o === t || v.isFunction(e);

      if (r) {
        if (u) {
          for (i in e) {
            if (n.apply(e[i], r) === !1) break;
          }
        } else for (; s < o;) {
          if (n.apply(e[s++], r) === !1) break;
        }
      } else if (u) {
        for (i in e) {
          if (n.call(e[i], i, e[i]) === !1) break;
        }
      } else for (; s < o;) {
        if (n.call(e[s], s, e[s++]) === !1) break;
      }

      return e;
    },
    trim: d && !d.call("\uFEFF\xA0") ? function (e) {
      return e == null ? "" : d.call(e);
    } : function (e) {
      return e == null ? "" : (e + "").replace(b, "");
    },
    makeArray: function makeArray(e, t) {
      var n,
          r = t || [];
      return e != null && (n = v.type(e), e.length == null || n === "string" || n === "function" || n === "regexp" || v.isWindow(e) ? f.call(r, e) : v.merge(r, e)), r;
    },
    inArray: function inArray(e, t, n) {
      var r;

      if (t) {
        if (c) return c.call(t, e, n);
        r = t.length, n = n ? n < 0 ? Math.max(0, r + n) : n : 0;

        for (; n < r; n++) {
          if (n in t && t[n] === e) return n;
        }
      }

      return -1;
    },
    merge: function merge(e, n) {
      var r = n.length,
          i = e.length,
          s = 0;
      if (typeof r == "number") for (; s < r; s++) {
        e[i++] = n[s];
      } else while (n[s] !== t) {
        e[i++] = n[s++];
      }
      return e.length = i, e;
    },
    grep: function grep(e, t, n) {
      var r,
          i = [],
          s = 0,
          o = e.length;
      n = !!n;

      for (; s < o; s++) {
        r = !!t(e[s], s), n !== r && i.push(e[s]);
      }

      return i;
    },
    map: function map(e, n, r) {
      var i,
          s,
          o = [],
          u = 0,
          a = e.length,
          f = e instanceof v || a !== t && typeof a == "number" && (a > 0 && e[0] && e[a - 1] || a === 0 || v.isArray(e));
      if (f) for (; u < a; u++) {
        i = n(e[u], u, r), i != null && (o[o.length] = i);
      } else for (s in e) {
        i = n(e[s], s, r), i != null && (o[o.length] = i);
      }
      return o.concat.apply([], o);
    },
    guid: 1,
    proxy: function proxy(e, n) {
      var r, i, s;
      return typeof n == "string" && (r = e[n], n = e, e = r), v.isFunction(e) ? (i = l.call(arguments, 2), s = function s() {
        return e.apply(n, i.concat(l.call(arguments)));
      }, s.guid = e.guid = e.guid || v.guid++, s) : t;
    },
    access: function access(e, n, r, i, s, o, u) {
      var a,
          f = r == null,
          l = 0,
          c = e.length;

      if (r && _typeof(r) == "object") {
        for (l in r) {
          v.access(e, n, l, r[l], 1, o, i);
        }

        s = 1;
      } else if (i !== t) {
        a = u === t && v.isFunction(i), f && (a ? (a = n, n = function n(e, t, _n2) {
          return a.call(v(e), _n2);
        }) : (n.call(e, i), n = null));
        if (n) for (; l < c; l++) {
          n(e[l], r, a ? i.call(e[l], l, n(e[l], r)) : i, u);
        }
        s = 1;
      }

      return s ? e : f ? n.call(e) : c ? n(e[0], r) : o;
    },
    now: function now() {
      return new Date().getTime();
    }
  }), v.ready.promise = function (t) {
    if (!r) {
      r = v.Deferred();
      if (i.readyState === "complete") setTimeout(v.ready, 1);else if (i.addEventListener) i.addEventListener("DOMContentLoaded", A, !1), e.addEventListener("load", v.ready, !1);else {
        i.attachEvent("onreadystatechange", A), e.attachEvent("onload", v.ready);
        var n = !1;

        try {
          n = e.frameElement == null && i.documentElement;
        } catch (s) {}

        n && n.doScroll && function o() {
          if (!v.isReady) {
            try {
              n.doScroll("left");
            } catch (e) {
              return setTimeout(o, 50);
            }

            v.ready();
          }
        }();
      }
    }

    return r.promise(t);
  }, v.each("Boolean Number String Function Array Date RegExp Object".split(" "), function (e, t) {
    O["[object " + t + "]"] = t.toLowerCase();
  }), n = v(i);
  var M = {};
  v.Callbacks = function (e) {
    e = typeof e == "string" ? M[e] || _(e) : v.extend({}, e);

    var n,
        r,
        i,
        s,
        o,
        u,
        a = [],
        f = !e.once && [],
        l = function l(t) {
      n = e.memory && t, r = !0, u = s || 0, s = 0, o = a.length, i = !0;

      for (; a && u < o; u++) {
        if (a[u].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
          n = !1;
          break;
        }
      }

      i = !1, a && (f ? f.length && l(f.shift()) : n ? a = [] : c.disable());
    },
        c = {
      add: function add() {
        if (a) {
          var t = a.length;
          (function r(t) {
            v.each(t, function (t, n) {
              var i = v.type(n);
              i === "function" ? (!e.unique || !c.has(n)) && a.push(n) : n && n.length && i !== "string" && r(n);
            });
          })(arguments), i ? o = a.length : n && (s = t, l(n));
        }

        return this;
      },
      remove: function remove() {
        return a && v.each(arguments, function (e, t) {
          var n;

          while ((n = v.inArray(t, a, n)) > -1) {
            a.splice(n, 1), i && (n <= o && o--, n <= u && u--);
          }
        }), this;
      },
      has: function has(e) {
        return v.inArray(e, a) > -1;
      },
      empty: function empty() {
        return a = [], this;
      },
      disable: function disable() {
        return a = f = n = t, this;
      },
      disabled: function disabled() {
        return !a;
      },
      lock: function lock() {
        return f = t, n || c.disable(), this;
      },
      locked: function locked() {
        return !f;
      },
      fireWith: function fireWith(e, t) {
        return t = t || [], t = [e, t.slice ? t.slice() : t], a && (!r || f) && (i ? f.push(t) : l(t)), this;
      },
      fire: function fire() {
        return c.fireWith(this, arguments), this;
      },
      fired: function fired() {
        return !!r;
      }
    };

    return c;
  }, v.extend({
    Deferred: function Deferred(e) {
      var t = [["resolve", "done", v.Callbacks("once memory"), "resolved"], ["reject", "fail", v.Callbacks("once memory"), "rejected"], ["notify", "progress", v.Callbacks("memory")]],
          n = "pending",
          r = {
        state: function state() {
          return n;
        },
        always: function always() {
          return i.done(arguments).fail(arguments), this;
        },
        then: function then() {
          var e = arguments;
          return v.Deferred(function (n) {
            v.each(t, function (t, r) {
              var s = r[0],
                  o = e[t];
              i[r[1]](v.isFunction(o) ? function () {
                var e = o.apply(this, arguments);
                e && v.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[s + "With"](this === i ? n : this, [e]);
              } : n[s]);
            }), e = null;
          }).promise();
        },
        promise: function promise(e) {
          return e != null ? v.extend(e, r) : r;
        }
      },
          i = {};
      return r.pipe = r.then, v.each(t, function (e, s) {
        var o = s[2],
            u = s[3];
        r[s[1]] = o.add, u && o.add(function () {
          n = u;
        }, t[e ^ 1][2].disable, t[2][2].lock), i[s[0]] = o.fire, i[s[0] + "With"] = o.fireWith;
      }), r.promise(i), e && e.call(i, i), i;
    },
    when: function when(e) {
      var t = 0,
          n = l.call(arguments),
          r = n.length,
          i = r !== 1 || e && v.isFunction(e.promise) ? r : 0,
          s = i === 1 ? e : v.Deferred(),
          o = function o(e, t, n) {
        return function (r) {
          t[e] = this, n[e] = arguments.length > 1 ? l.call(arguments) : r, n === u ? s.notifyWith(t, n) : --i || s.resolveWith(t, n);
        };
      },
          u,
          a,
          f;

      if (r > 1) {
        u = new Array(r), a = new Array(r), f = new Array(r);

        for (; t < r; t++) {
          n[t] && v.isFunction(n[t].promise) ? n[t].promise().done(o(t, f, n)).fail(s.reject).progress(o(t, a, u)) : --i;
        }
      }

      return i || s.resolveWith(f, n), s.promise();
    }
  }), v.support = function () {
    var t,
        n,
        r,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h,
        p = i.createElement("div");
    p.setAttribute("className", "t"), p.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = p.getElementsByTagName("*"), r = p.getElementsByTagName("a")[0];
    if (!n || !r || !n.length) return {};
    s = i.createElement("select"), o = s.appendChild(i.createElement("option")), u = p.getElementsByTagName("input")[0], r.style.cssText = "top:1px;float:left;opacity:.5", t = {
      leadingWhitespace: p.firstChild.nodeType === 3,
      tbody: !p.getElementsByTagName("tbody").length,
      htmlSerialize: !!p.getElementsByTagName("link").length,
      style: /top/.test(r.getAttribute("style")),
      hrefNormalized: r.getAttribute("href") === "/a",
      opacity: /^0.5/.test(r.style.opacity),
      cssFloat: !!r.style.cssFloat,
      checkOn: u.value === "on",
      optSelected: o.selected,
      getSetAttribute: p.className !== "t",
      enctype: !!i.createElement("form").enctype,
      html5Clone: i.createElement("nav").cloneNode(!0).outerHTML !== "<:nav></:nav>",
      boxModel: i.compatMode === "CSS1Compat",
      submitBubbles: !0,
      changeBubbles: !0,
      focusinBubbles: !1,
      deleteExpando: !0,
      noCloneEvent: !0,
      inlineBlockNeedsLayout: !1,
      shrinkWrapBlocks: !1,
      reliableMarginRight: !0,
      boxSizingReliable: !0,
      pixelPosition: !1
    }, u.checked = !0, t.noCloneChecked = u.cloneNode(!0).checked, s.disabled = !0, t.optDisabled = !o.disabled;

    try {
      delete p.test;
    } catch (d) {
      t.deleteExpando = !1;
    }

    !p.addEventListener && p.attachEvent && p.fireEvent && (p.attachEvent("onclick", h = function h() {
      t.noCloneEvent = !1;
    }), p.cloneNode(!0).fireEvent("onclick"), p.detachEvent("onclick", h)), u = i.createElement("input"), u.value = "t", u.setAttribute("type", "radio"), t.radioValue = u.value === "t", u.setAttribute("checked", "checked"), u.setAttribute("name", "t"), p.appendChild(u), a = i.createDocumentFragment(), a.appendChild(p.lastChild), t.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked, t.appendChecked = u.checked, a.removeChild(u), a.appendChild(p);
    if (p.attachEvent) for (l in {
      submit: !0,
      change: !0,
      focusin: !0
    }) {
      f = "on" + l, c = f in p, c || (p.setAttribute(f, "return;"), c = typeof p[f] == "function"), t[l + "Bubbles"] = c;
    }
    return v(function () {
      var n,
          r,
          s,
          o,
          u = "padding:0;margin:0;border:0;display:block;overflow:hidden;",
          a = i.getElementsByTagName("body")[0];
      if (!a) return;
      n = i.createElement("div"), n.style.cssText = "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px", a.insertBefore(n, a.firstChild), r = i.createElement("div"), n.appendChild(r), r.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", s = r.getElementsByTagName("td"), s[0].style.cssText = "padding:0;margin:0;border:0;display:none", c = s[0].offsetHeight === 0, s[0].style.display = "", s[1].style.display = "none", t.reliableHiddenOffsets = c && s[0].offsetHeight === 0, r.innerHTML = "", r.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", t.boxSizing = r.offsetWidth === 4, t.doesNotIncludeMarginInBodyOffset = a.offsetTop !== 1, e.getComputedStyle && (t.pixelPosition = (e.getComputedStyle(r, null) || {}).top !== "1%", t.boxSizingReliable = (e.getComputedStyle(r, null) || {
        width: "4px"
      }).width === "4px", o = i.createElement("div"), o.style.cssText = r.style.cssText = u, o.style.marginRight = o.style.width = "0", r.style.width = "1px", r.appendChild(o), t.reliableMarginRight = !parseFloat((e.getComputedStyle(o, null) || {}).marginRight)), typeof r.style.zoom != "undefined" && (r.innerHTML = "", r.style.cssText = u + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = r.offsetWidth === 3, r.style.display = "block", r.style.overflow = "visible", r.innerHTML = "<div></div>", r.firstChild.style.width = "5px", t.shrinkWrapBlocks = r.offsetWidth !== 3, n.style.zoom = 1), a.removeChild(n), n = r = s = o = null;
    }), a.removeChild(p), n = r = s = o = u = a = p = null, t;
  }();
  var D = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
      P = /([A-Z])/g;
  v.extend({
    cache: {},
    deletedIds: [],
    uuid: 0,
    expando: "jQuery" + (v.fn.jquery + Math.random()).replace(/\D/g, ""),
    noData: {
      embed: !0,
      object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
      applet: !0
    },
    hasData: function hasData(e) {
      return e = e.nodeType ? v.cache[e[v.expando]] : e[v.expando], !!e && !B(e);
    },
    data: function data(e, n, r, i) {
      if (!v.acceptData(e)) return;
      var s,
          o,
          u = v.expando,
          a = typeof n == "string",
          f = e.nodeType,
          l = f ? v.cache : e,
          c = f ? e[u] : e[u] && u;
      if ((!c || !l[c] || !i && !l[c].data) && a && r === t) return;
      c || (f ? e[u] = c = v.deletedIds.pop() || v.guid++ : c = u), l[c] || (l[c] = {}, f || (l[c].toJSON = v.noop));
      if (_typeof(n) == "object" || typeof n == "function") i ? l[c] = v.extend(l[c], n) : l[c].data = v.extend(l[c].data, n);
      return s = l[c], i || (s.data || (s.data = {}), s = s.data), r !== t && (s[v.camelCase(n)] = r), a ? (o = s[n], o == null && (o = s[v.camelCase(n)])) : o = s, o;
    },
    removeData: function removeData(e, t, n) {
      if (!v.acceptData(e)) return;
      var r,
          i,
          s,
          o = e.nodeType,
          u = o ? v.cache : e,
          a = o ? e[v.expando] : v.expando;
      if (!u[a]) return;

      if (t) {
        r = n ? u[a] : u[a].data;

        if (r) {
          v.isArray(t) || (t in r ? t = [t] : (t = v.camelCase(t), t in r ? t = [t] : t = t.split(" ")));

          for (i = 0, s = t.length; i < s; i++) {
            delete r[t[i]];
          }

          if (!(n ? B : v.isEmptyObject)(r)) return;
        }
      }

      if (!n) {
        delete u[a].data;
        if (!B(u[a])) return;
      }

      o ? v.cleanData([e], !0) : v.support.deleteExpando || u != u.window ? delete u[a] : u[a] = null;
    },
    _data: function _data(e, t, n) {
      return v.data(e, t, n, !0);
    },
    acceptData: function acceptData(e) {
      var t = e.nodeName && v.noData[e.nodeName.toLowerCase()];
      return !t || t !== !0 && e.getAttribute("classid") === t;
    }
  }), v.fn.extend({
    data: function data(e, n) {
      var r,
          i,
          s,
          o,
          u,
          a = this[0],
          f = 0,
          l = null;

      if (e === t) {
        if (this.length) {
          l = v.data(a);

          if (a.nodeType === 1 && !v._data(a, "parsedAttrs")) {
            s = a.attributes;

            for (u = s.length; f < u; f++) {
              o = s[f].name, o.indexOf("data-") || (o = v.camelCase(o.substring(5)), H(a, o, l[o]));
            }

            v._data(a, "parsedAttrs", !0);
          }
        }

        return l;
      }

      return _typeof(e) == "object" ? this.each(function () {
        v.data(this, e);
      }) : (r = e.split(".", 2), r[1] = r[1] ? "." + r[1] : "", i = r[1] + "!", v.access(this, function (n) {
        if (n === t) return l = this.triggerHandler("getData" + i, [r[0]]), l === t && a && (l = v.data(a, e), l = H(a, e, l)), l === t && r[1] ? this.data(r[0]) : l;
        r[1] = n, this.each(function () {
          var t = v(this);
          t.triggerHandler("setData" + i, r), v.data(this, e, n), t.triggerHandler("changeData" + i, r);
        });
      }, null, n, arguments.length > 1, null, !1));
    },
    removeData: function removeData(e) {
      return this.each(function () {
        v.removeData(this, e);
      });
    }
  }), v.extend({
    queue: function queue(e, t, n) {
      var r;
      if (e) return t = (t || "fx") + "queue", r = v._data(e, t), n && (!r || v.isArray(n) ? r = v._data(e, t, v.makeArray(n)) : r.push(n)), r || [];
    },
    dequeue: function dequeue(e, t) {
      t = t || "fx";

      var n = v.queue(e, t),
          r = n.length,
          i = n.shift(),
          s = v._queueHooks(e, t),
          o = function o() {
        v.dequeue(e, t);
      };

      i === "inprogress" && (i = n.shift(), r--), i && (t === "fx" && n.unshift("inprogress"), delete s.stop, i.call(e, o, s)), !r && s && s.empty.fire();
    },
    _queueHooks: function _queueHooks(e, t) {
      var n = t + "queueHooks";
      return v._data(e, n) || v._data(e, n, {
        empty: v.Callbacks("once memory").add(function () {
          v.removeData(e, t + "queue", !0), v.removeData(e, n, !0);
        })
      });
    }
  }), v.fn.extend({
    queue: function queue(e, n) {
      var r = 2;
      return typeof e != "string" && (n = e, e = "fx", r--), arguments.length < r ? v.queue(this[0], e) : n === t ? this : this.each(function () {
        var t = v.queue(this, e, n);
        v._queueHooks(this, e), e === "fx" && t[0] !== "inprogress" && v.dequeue(this, e);
      });
    },
    dequeue: function dequeue(e) {
      return this.each(function () {
        v.dequeue(this, e);
      });
    },
    delay: function delay(e, t) {
      return e = v.fx ? v.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function (t, n) {
        var r = setTimeout(t, e);

        n.stop = function () {
          clearTimeout(r);
        };
      });
    },
    clearQueue: function clearQueue(e) {
      return this.queue(e || "fx", []);
    },
    promise: function promise(e, n) {
      var r,
          i = 1,
          s = v.Deferred(),
          o = this,
          u = this.length,
          a = function a() {
        --i || s.resolveWith(o, [o]);
      };

      typeof e != "string" && (n = e, e = t), e = e || "fx";

      while (u--) {
        r = v._data(o[u], e + "queueHooks"), r && r.empty && (i++, r.empty.add(a));
      }

      return a(), s.promise(n);
    }
  });
  var j,
      F,
      I,
      q = /[\t\r\n]/g,
      R = /\r/g,
      U = /^(?:button|input)$/i,
      z = /^(?:button|input|object|select|textarea)$/i,
      W = /^a(?:rea|)$/i,
      X = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
      V = v.support.getSetAttribute;
  v.fn.extend({
    attr: function attr(e, t) {
      return v.access(this, v.attr, e, t, arguments.length > 1);
    },
    removeAttr: function removeAttr(e) {
      return this.each(function () {
        v.removeAttr(this, e);
      });
    },
    prop: function prop(e, t) {
      return v.access(this, v.prop, e, t, arguments.length > 1);
    },
    removeProp: function removeProp(e) {
      return e = v.propFix[e] || e, this.each(function () {
        try {
          this[e] = t, delete this[e];
        } catch (n) {}
      });
    },
    addClass: function addClass(e) {
      var t, n, r, i, s, o, u;
      if (v.isFunction(e)) return this.each(function (t) {
        v(this).addClass(e.call(this, t, this.className));
      });

      if (e && typeof e == "string") {
        t = e.split(y);

        for (n = 0, r = this.length; n < r; n++) {
          i = this[n];
          if (i.nodeType === 1) if (!i.className && t.length === 1) i.className = e;else {
            s = " " + i.className + " ";

            for (o = 0, u = t.length; o < u; o++) {
              s.indexOf(" " + t[o] + " ") < 0 && (s += t[o] + " ");
            }

            i.className = v.trim(s);
          }
        }
      }

      return this;
    },
    removeClass: function removeClass(e) {
      var n, r, i, s, o, u, a;
      if (v.isFunction(e)) return this.each(function (t) {
        v(this).removeClass(e.call(this, t, this.className));
      });

      if (e && typeof e == "string" || e === t) {
        n = (e || "").split(y);

        for (u = 0, a = this.length; u < a; u++) {
          i = this[u];

          if (i.nodeType === 1 && i.className) {
            r = (" " + i.className + " ").replace(q, " ");

            for (s = 0, o = n.length; s < o; s++) {
              while (r.indexOf(" " + n[s] + " ") >= 0) {
                r = r.replace(" " + n[s] + " ", " ");
              }
            }

            i.className = e ? v.trim(r) : "";
          }
        }
      }

      return this;
    },
    toggleClass: function toggleClass(e, t) {
      var n = _typeof(e),
          r = typeof t == "boolean";

      return v.isFunction(e) ? this.each(function (n) {
        v(this).toggleClass(e.call(this, n, this.className, t), t);
      }) : this.each(function () {
        if (n === "string") {
          var i,
              s = 0,
              o = v(this),
              u = t,
              a = e.split(y);

          while (i = a[s++]) {
            u = r ? u : !o.hasClass(i), o[u ? "addClass" : "removeClass"](i);
          }
        } else if (n === "undefined" || n === "boolean") this.className && v._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : v._data(this, "__className__") || "";
      });
    },
    hasClass: function hasClass(e) {
      var t = " " + e + " ",
          n = 0,
          r = this.length;

      for (; n < r; n++) {
        if (this[n].nodeType === 1 && (" " + this[n].className + " ").replace(q, " ").indexOf(t) >= 0) return !0;
      }

      return !1;
    },
    val: function val(e) {
      var n,
          r,
          i,
          s = this[0];

      if (!arguments.length) {
        if (s) return n = v.valHooks[s.type] || v.valHooks[s.nodeName.toLowerCase()], n && "get" in n && (r = n.get(s, "value")) !== t ? r : (r = s.value, typeof r == "string" ? r.replace(R, "") : r == null ? "" : r);
        return;
      }

      return i = v.isFunction(e), this.each(function (r) {
        var s,
            o = v(this);
        if (this.nodeType !== 1) return;
        i ? s = e.call(this, r, o.val()) : s = e, s == null ? s = "" : typeof s == "number" ? s += "" : v.isArray(s) && (s = v.map(s, function (e) {
          return e == null ? "" : e + "";
        })), n = v.valHooks[this.type] || v.valHooks[this.nodeName.toLowerCase()];
        if (!n || !("set" in n) || n.set(this, s, "value") === t) this.value = s;
      });
    }
  }), v.extend({
    valHooks: {
      option: {
        get: function get(e) {
          var t = e.attributes.value;
          return !t || t.specified ? e.value : e.text;
        }
      },
      select: {
        get: function get(e) {
          var t,
              n,
              r = e.options,
              i = e.selectedIndex,
              s = e.type === "select-one" || i < 0,
              o = s ? null : [],
              u = s ? i + 1 : r.length,
              a = i < 0 ? u : s ? i : 0;

          for (; a < u; a++) {
            n = r[a];

            if ((n.selected || a === i) && (v.support.optDisabled ? !n.disabled : n.getAttribute("disabled") === null) && (!n.parentNode.disabled || !v.nodeName(n.parentNode, "optgroup"))) {
              t = v(n).val();
              if (s) return t;
              o.push(t);
            }
          }

          return o;
        },
        set: function set(e, t) {
          var n = v.makeArray(t);
          return v(e).find("option").each(function () {
            this.selected = v.inArray(v(this).val(), n) >= 0;
          }), n.length || (e.selectedIndex = -1), n;
        }
      }
    },
    attrFn: {},
    attr: function attr(e, n, r, i) {
      var s,
          o,
          u,
          a = e.nodeType;
      if (!e || a === 3 || a === 8 || a === 2) return;
      if (i && v.isFunction(v.fn[n])) return v(e)[n](r);
      if (typeof e.getAttribute == "undefined") return v.prop(e, n, r);
      u = a !== 1 || !v.isXMLDoc(e), u && (n = n.toLowerCase(), o = v.attrHooks[n] || (X.test(n) ? F : j));

      if (r !== t) {
        if (r === null) {
          v.removeAttr(e, n);
          return;
        }

        return o && "set" in o && u && (s = o.set(e, r, n)) !== t ? s : (e.setAttribute(n, r + ""), r);
      }

      return o && "get" in o && u && (s = o.get(e, n)) !== null ? s : (s = e.getAttribute(n), s === null ? t : s);
    },
    removeAttr: function removeAttr(e, t) {
      var n,
          r,
          i,
          s,
          o = 0;

      if (t && e.nodeType === 1) {
        r = t.split(y);

        for (; o < r.length; o++) {
          i = r[o], i && (n = v.propFix[i] || i, s = X.test(i), s || v.attr(e, i, ""), e.removeAttribute(V ? i : n), s && n in e && (e[n] = !1));
        }
      }
    },
    attrHooks: {
      type: {
        set: function set(e, t) {
          if (U.test(e.nodeName) && e.parentNode) v.error("type property can't be changed");else if (!v.support.radioValue && t === "radio" && v.nodeName(e, "input")) {
            var n = e.value;
            return e.setAttribute("type", t), n && (e.value = n), t;
          }
        }
      },
      value: {
        get: function get(e, t) {
          return j && v.nodeName(e, "button") ? j.get(e, t) : t in e ? e.value : null;
        },
        set: function set(e, t, n) {
          if (j && v.nodeName(e, "button")) return j.set(e, t, n);
          e.value = t;
        }
      }
    },
    propFix: {
      tabindex: "tabIndex",
      readonly: "readOnly",
      "for": "htmlFor",
      "class": "className",
      maxlength: "maxLength",
      cellspacing: "cellSpacing",
      cellpadding: "cellPadding",
      rowspan: "rowSpan",
      colspan: "colSpan",
      usemap: "useMap",
      frameborder: "frameBorder",
      contenteditable: "contentEditable"
    },
    prop: function prop(e, n, r) {
      var i,
          s,
          o,
          u = e.nodeType;
      if (!e || u === 3 || u === 8 || u === 2) return;
      return o = u !== 1 || !v.isXMLDoc(e), o && (n = v.propFix[n] || n, s = v.propHooks[n]), r !== t ? s && "set" in s && (i = s.set(e, r, n)) !== t ? i : e[n] = r : s && "get" in s && (i = s.get(e, n)) !== null ? i : e[n];
    },
    propHooks: {
      tabIndex: {
        get: function get(e) {
          var n = e.getAttributeNode("tabindex");
          return n && n.specified ? parseInt(n.value, 10) : z.test(e.nodeName) || W.test(e.nodeName) && e.href ? 0 : t;
        }
      }
    }
  }), F = {
    get: function get(e, n) {
      var r,
          i = v.prop(e, n);
      return i === !0 || typeof i != "boolean" && (r = e.getAttributeNode(n)) && r.nodeValue !== !1 ? n.toLowerCase() : t;
    },
    set: function set(e, t, n) {
      var r;
      return t === !1 ? v.removeAttr(e, n) : (r = v.propFix[n] || n, r in e && (e[r] = !0), e.setAttribute(n, n.toLowerCase())), n;
    }
  }, V || (I = {
    name: !0,
    id: !0,
    coords: !0
  }, j = v.valHooks.button = {
    get: function get(e, n) {
      var r;
      return r = e.getAttributeNode(n), r && (I[n] ? r.value !== "" : r.specified) ? r.value : t;
    },
    set: function set(e, t, n) {
      var r = e.getAttributeNode(n);
      return r || (r = i.createAttribute(n), e.setAttributeNode(r)), r.value = t + "";
    }
  }, v.each(["width", "height"], function (e, t) {
    v.attrHooks[t] = v.extend(v.attrHooks[t], {
      set: function set(e, n) {
        if (n === "") return e.setAttribute(t, "auto"), n;
      }
    });
  }), v.attrHooks.contenteditable = {
    get: j.get,
    set: function set(e, t, n) {
      t === "" && (t = "false"), j.set(e, t, n);
    }
  }), v.support.hrefNormalized || v.each(["href", "src", "width", "height"], function (e, n) {
    v.attrHooks[n] = v.extend(v.attrHooks[n], {
      get: function get(e) {
        var r = e.getAttribute(n, 2);
        return r === null ? t : r;
      }
    });
  }), v.support.style || (v.attrHooks.style = {
    get: function get(e) {
      return e.style.cssText.toLowerCase() || t;
    },
    set: function set(e, t) {
      return e.style.cssText = t + "";
    }
  }), v.support.optSelected || (v.propHooks.selected = v.extend(v.propHooks.selected, {
    get: function get(e) {
      var t = e.parentNode;
      return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null;
    }
  })), v.support.enctype || (v.propFix.enctype = "encoding"), v.support.checkOn || v.each(["radio", "checkbox"], function () {
    v.valHooks[this] = {
      get: function get(e) {
        return e.getAttribute("value") === null ? "on" : e.value;
      }
    };
  }), v.each(["radio", "checkbox"], function () {
    v.valHooks[this] = v.extend(v.valHooks[this], {
      set: function set(e, t) {
        if (v.isArray(t)) return e.checked = v.inArray(v(e).val(), t) >= 0;
      }
    });
  });

  var $ = /^(?:textarea|input|select)$/i,
      J = /^([^\.]*|)(?:\.(.+)|)$/,
      K = /(?:^|\s)hover(\.\S+|)\b/,
      Q = /^key/,
      G = /^(?:mouse|contextmenu)|click/,
      Y = /^(?:focusinfocus|focusoutblur)$/,
      Z = function Z(e) {
    return v.event.special.hover ? e : e.replace(K, "mouseenter$1 mouseleave$1");
  };

  v.event = {
    add: function add(e, n, r, i, s) {
      var o, _u, a, f, l, c, h, p, d, m, g;

      if (e.nodeType === 3 || e.nodeType === 8 || !n || !r || !(o = v._data(e))) return;
      r.handler && (d = r, r = d.handler, s = d.selector), r.guid || (r.guid = v.guid++), a = o.events, a || (o.events = a = {}), _u = o.handle, _u || (o.handle = _u = function u(e) {
        return typeof v == "undefined" || !!e && v.event.triggered === e.type ? t : v.event.dispatch.apply(_u.elem, arguments);
      }, _u.elem = e), n = v.trim(Z(n)).split(" ");

      for (f = 0; f < n.length; f++) {
        l = J.exec(n[f]) || [], c = l[1], h = (l[2] || "").split(".").sort(), g = v.event.special[c] || {}, c = (s ? g.delegateType : g.bindType) || c, g = v.event.special[c] || {}, p = v.extend({
          type: c,
          origType: l[1],
          data: i,
          handler: r,
          guid: r.guid,
          selector: s,
          needsContext: s && v.expr.match.needsContext.test(s),
          namespace: h.join(".")
        }, d), m = a[c];

        if (!m) {
          m = a[c] = [], m.delegateCount = 0;
          if (!g.setup || g.setup.call(e, i, h, _u) === !1) e.addEventListener ? e.addEventListener(c, _u, !1) : e.attachEvent && e.attachEvent("on" + c, _u);
        }

        g.add && (g.add.call(e, p), p.handler.guid || (p.handler.guid = r.guid)), s ? m.splice(m.delegateCount++, 0, p) : m.push(p), v.event.global[c] = !0;
      }

      e = null;
    },
    global: {},
    remove: function remove(e, t, n, r, i) {
      var s,
          o,
          u,
          a,
          f,
          l,
          c,
          h,
          p,
          d,
          m,
          g = v.hasData(e) && v._data(e);

      if (!g || !(h = g.events)) return;
      t = v.trim(Z(t || "")).split(" ");

      for (s = 0; s < t.length; s++) {
        o = J.exec(t[s]) || [], u = a = o[1], f = o[2];

        if (!u) {
          for (u in h) {
            v.event.remove(e, u + t[s], n, r, !0);
          }

          continue;
        }

        p = v.event.special[u] || {}, u = (r ? p.delegateType : p.bindType) || u, d = h[u] || [], l = d.length, f = f ? new RegExp("(^|\\.)" + f.split(".").sort().join("\\.(?:.*\\.|)") + "(\\.|$)") : null;

        for (c = 0; c < d.length; c++) {
          m = d[c], (i || a === m.origType) && (!n || n.guid === m.guid) && (!f || f.test(m.namespace)) && (!r || r === m.selector || r === "**" && m.selector) && (d.splice(c--, 1), m.selector && d.delegateCount--, p.remove && p.remove.call(e, m));
        }

        d.length === 0 && l !== d.length && ((!p.teardown || p.teardown.call(e, f, g.handle) === !1) && v.removeEvent(e, u, g.handle), delete h[u]);
      }

      v.isEmptyObject(h) && (delete g.handle, v.removeData(e, "events", !0));
    },
    customEvent: {
      getData: !0,
      setData: !0,
      changeData: !0
    },
    trigger: function trigger(n, r, s, o) {
      if (!s || s.nodeType !== 3 && s.nodeType !== 8) {
        var u,
            a,
            f,
            l,
            c,
            h,
            p,
            d,
            m,
            g,
            y = n.type || n,
            b = [];
        if (Y.test(y + v.event.triggered)) return;
        y.indexOf("!") >= 0 && (y = y.slice(0, -1), a = !0), y.indexOf(".") >= 0 && (b = y.split("."), y = b.shift(), b.sort());
        if ((!s || v.event.customEvent[y]) && !v.event.global[y]) return;
        n = _typeof(n) == "object" ? n[v.expando] ? n : new v.Event(y, n) : new v.Event(y), n.type = y, n.isTrigger = !0, n.exclusive = a, n.namespace = b.join("."), n.namespace_re = n.namespace ? new RegExp("(^|\\.)" + b.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, h = y.indexOf(":") < 0 ? "on" + y : "";

        if (!s) {
          u = v.cache;

          for (f in u) {
            u[f].events && u[f].events[y] && v.event.trigger(n, r, u[f].handle.elem, !0);
          }

          return;
        }

        n.result = t, n.target || (n.target = s), r = r != null ? v.makeArray(r) : [], r.unshift(n), p = v.event.special[y] || {};
        if (p.trigger && p.trigger.apply(s, r) === !1) return;
        m = [[s, p.bindType || y]];

        if (!o && !p.noBubble && !v.isWindow(s)) {
          g = p.delegateType || y, l = Y.test(g + y) ? s : s.parentNode;

          for (c = s; l; l = l.parentNode) {
            m.push([l, g]), c = l;
          }

          c === (s.ownerDocument || i) && m.push([c.defaultView || c.parentWindow || e, g]);
        }

        for (f = 0; f < m.length && !n.isPropagationStopped(); f++) {
          l = m[f][0], n.type = m[f][1], d = (v._data(l, "events") || {})[n.type] && v._data(l, "handle"), d && d.apply(l, r), d = h && l[h], d && v.acceptData(l) && d.apply && d.apply(l, r) === !1 && n.preventDefault();
        }

        return n.type = y, !o && !n.isDefaultPrevented() && (!p._default || p._default.apply(s.ownerDocument, r) === !1) && (y !== "click" || !v.nodeName(s, "a")) && v.acceptData(s) && h && s[y] && (y !== "focus" && y !== "blur" || n.target.offsetWidth !== 0) && !v.isWindow(s) && (c = s[h], c && (s[h] = null), v.event.triggered = y, s[y](), v.event.triggered = t, c && (s[h] = c)), n.result;
      }

      return;
    },
    dispatch: function dispatch(n) {
      n = v.event.fix(n || e.event);
      var r,
          i,
          s,
          o,
          u,
          a,
          f,
          c,
          h,
          p,
          d = (v._data(this, "events") || {})[n.type] || [],
          m = d.delegateCount,
          g = l.call(arguments),
          y = !n.exclusive && !n.namespace,
          b = v.event.special[n.type] || {},
          w = [];
      g[0] = n, n.delegateTarget = this;
      if (b.preDispatch && b.preDispatch.call(this, n) === !1) return;
      if (m && (!n.button || n.type !== "click")) for (s = n.target; s != this; s = s.parentNode || this) {
        if (s.disabled !== !0 || n.type !== "click") {
          u = {}, f = [];

          for (r = 0; r < m; r++) {
            c = d[r], h = c.selector, u[h] === t && (u[h] = c.needsContext ? v(h, this).index(s) >= 0 : v.find(h, this, null, [s]).length), u[h] && f.push(c);
          }

          f.length && w.push({
            elem: s,
            matches: f
          });
        }
      }
      d.length > m && w.push({
        elem: this,
        matches: d.slice(m)
      });

      for (r = 0; r < w.length && !n.isPropagationStopped(); r++) {
        a = w[r], n.currentTarget = a.elem;

        for (i = 0; i < a.matches.length && !n.isImmediatePropagationStopped(); i++) {
          c = a.matches[i];
          if (y || !n.namespace && !c.namespace || n.namespace_re && n.namespace_re.test(c.namespace)) n.data = c.data, n.handleObj = c, o = ((v.event.special[c.origType] || {}).handle || c.handler).apply(a.elem, g), o !== t && (n.result = o, o === !1 && (n.preventDefault(), n.stopPropagation()));
        }
      }

      return b.postDispatch && b.postDispatch.call(this, n), n.result;
    },
    props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function filter(e, t) {
        return e.which == null && (e.which = t.charCode != null ? t.charCode : t.keyCode), e;
      }
    },
    mouseHooks: {
      props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
      filter: function filter(e, n) {
        var r,
            s,
            o,
            u = n.button,
            a = n.fromElement;
        return e.pageX == null && n.clientX != null && (r = e.target.ownerDocument || i, s = r.documentElement, o = r.body, e.pageX = n.clientX + (s && s.scrollLeft || o && o.scrollLeft || 0) - (s && s.clientLeft || o && o.clientLeft || 0), e.pageY = n.clientY + (s && s.scrollTop || o && o.scrollTop || 0) - (s && s.clientTop || o && o.clientTop || 0)), !e.relatedTarget && a && (e.relatedTarget = a === e.target ? n.toElement : a), !e.which && u !== t && (e.which = u & 1 ? 1 : u & 2 ? 3 : u & 4 ? 2 : 0), e;
      }
    },
    fix: function fix(e) {
      if (e[v.expando]) return e;
      var t,
          n,
          r = e,
          s = v.event.fixHooks[e.type] || {},
          o = s.props ? this.props.concat(s.props) : this.props;
      e = v.Event(r);

      for (t = o.length; t;) {
        n = o[--t], e[n] = r[n];
      }

      return e.target || (e.target = r.srcElement || i), e.target.nodeType === 3 && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, s.filter ? s.filter(e, r) : e;
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        delegateType: "focusin"
      },
      blur: {
        delegateType: "focusout"
      },
      beforeunload: {
        setup: function setup(e, t, n) {
          v.isWindow(this) && (this.onbeforeunload = n);
        },
        teardown: function teardown(e, t) {
          this.onbeforeunload === t && (this.onbeforeunload = null);
        }
      }
    },
    simulate: function simulate(e, t, n, r) {
      var i = v.extend(new v.Event(), n, {
        type: e,
        isSimulated: !0,
        originalEvent: {}
      });
      r ? v.event.trigger(i, null, t) : v.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault();
    }
  }, v.event.handle = v.event.dispatch, v.removeEvent = i.removeEventListener ? function (e, t, n) {
    e.removeEventListener && e.removeEventListener(t, n, !1);
  } : function (e, t, n) {
    var r = "on" + t;
    e.detachEvent && (typeof e[r] == "undefined" && (e[r] = null), e.detachEvent(r, n));
  }, v.Event = function (e, t) {
    if (!(this instanceof v.Event)) return new v.Event(e, t);
    e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? tt : et) : this.type = e, t && v.extend(this, t), this.timeStamp = e && e.timeStamp || v.now(), this[v.expando] = !0;
  }, v.Event.prototype = {
    preventDefault: function preventDefault() {
      this.isDefaultPrevented = tt;
      var e = this.originalEvent;
      if (!e) return;
      e.preventDefault ? e.preventDefault() : e.returnValue = !1;
    },
    stopPropagation: function stopPropagation() {
      this.isPropagationStopped = tt;
      var e = this.originalEvent;
      if (!e) return;
      e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0;
    },
    stopImmediatePropagation: function stopImmediatePropagation() {
      this.isImmediatePropagationStopped = tt, this.stopPropagation();
    },
    isDefaultPrevented: et,
    isPropagationStopped: et,
    isImmediatePropagationStopped: et
  }, v.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout"
  }, function (e, t) {
    v.event.special[e] = {
      delegateType: t,
      bindType: t,
      handle: function handle(e) {
        var n,
            r = this,
            i = e.relatedTarget,
            s = e.handleObj,
            o = s.selector;
        if (!i || i !== r && !v.contains(r, i)) e.type = s.origType, n = s.handler.apply(this, arguments), e.type = t;
        return n;
      }
    };
  }), v.support.submitBubbles || (v.event.special.submit = {
    setup: function setup() {
      if (v.nodeName(this, "form")) return !1;
      v.event.add(this, "click._submit keypress._submit", function (e) {
        var n = e.target,
            r = v.nodeName(n, "input") || v.nodeName(n, "button") ? n.form : t;
        r && !v._data(r, "_submit_attached") && (v.event.add(r, "submit._submit", function (e) {
          e._submit_bubble = !0;
        }), v._data(r, "_submit_attached", !0));
      });
    },
    postDispatch: function postDispatch(e) {
      e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && v.event.simulate("submit", this.parentNode, e, !0));
    },
    teardown: function teardown() {
      if (v.nodeName(this, "form")) return !1;
      v.event.remove(this, "._submit");
    }
  }), v.support.changeBubbles || (v.event.special.change = {
    setup: function setup() {
      if ($.test(this.nodeName)) {
        if (this.type === "checkbox" || this.type === "radio") v.event.add(this, "propertychange._change", function (e) {
          e.originalEvent.propertyName === "checked" && (this._just_changed = !0);
        }), v.event.add(this, "click._change", function (e) {
          this._just_changed && !e.isTrigger && (this._just_changed = !1), v.event.simulate("change", this, e, !0);
        });
        return !1;
      }

      v.event.add(this, "beforeactivate._change", function (e) {
        var t = e.target;
        $.test(t.nodeName) && !v._data(t, "_change_attached") && (v.event.add(t, "change._change", function (e) {
          this.parentNode && !e.isSimulated && !e.isTrigger && v.event.simulate("change", this.parentNode, e, !0);
        }), v._data(t, "_change_attached", !0));
      });
    },
    handle: function handle(e) {
      var t = e.target;
      if (this !== t || e.isSimulated || e.isTrigger || t.type !== "radio" && t.type !== "checkbox") return e.handleObj.handler.apply(this, arguments);
    },
    teardown: function teardown() {
      return v.event.remove(this, "._change"), !$.test(this.nodeName);
    }
  }), v.support.focusinBubbles || v.each({
    focus: "focusin",
    blur: "focusout"
  }, function (e, t) {
    var n = 0,
        r = function r(e) {
      v.event.simulate(t, e.target, v.event.fix(e), !0);
    };

    v.event.special[t] = {
      setup: function setup() {
        n++ === 0 && i.addEventListener(e, r, !0);
      },
      teardown: function teardown() {
        --n === 0 && i.removeEventListener(e, r, !0);
      }
    };
  }), v.fn.extend({
    on: function on(e, n, r, i, s) {
      var o, u;

      if (_typeof(e) == "object") {
        typeof n != "string" && (r = r || n, n = t);

        for (u in e) {
          this.on(u, n, r, e[u], s);
        }

        return this;
      }

      r == null && i == null ? (i = n, r = n = t) : i == null && (typeof n == "string" ? (i = r, r = t) : (i = r, r = n, n = t));
      if (i === !1) i = et;else if (!i) return this;
      return s === 1 && (o = i, i = function i(e) {
        return v().off(e), o.apply(this, arguments);
      }, i.guid = o.guid || (o.guid = v.guid++)), this.each(function () {
        v.event.add(this, e, i, r, n);
      });
    },
    one: function one(e, t, n, r) {
      return this.on(e, t, n, r, 1);
    },
    off: function off(e, n, r) {
      var i, s;
      if (e && e.preventDefault && e.handleObj) return i = e.handleObj, v(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;

      if (_typeof(e) == "object") {
        for (s in e) {
          this.off(s, n, e[s]);
        }

        return this;
      }

      if (n === !1 || typeof n == "function") r = n, n = t;
      return r === !1 && (r = et), this.each(function () {
        v.event.remove(this, e, r, n);
      });
    },
    bind: function bind(e, t, n) {
      return this.on(e, null, t, n);
    },
    unbind: function unbind(e, t) {
      return this.off(e, null, t);
    },
    live: function live(e, t, n) {
      return v(this.context).on(e, this.selector, t, n), this;
    },
    die: function die(e, t) {
      return v(this.context).off(e, this.selector || "**", t), this;
    },
    delegate: function delegate(e, t, n, r) {
      return this.on(t, e, n, r);
    },
    undelegate: function undelegate(e, t, n) {
      return arguments.length === 1 ? this.off(e, "**") : this.off(t, e || "**", n);
    },
    trigger: function trigger(e, t) {
      return this.each(function () {
        v.event.trigger(e, t, this);
      });
    },
    triggerHandler: function triggerHandler(e, t) {
      if (this[0]) return v.event.trigger(e, t, this[0], !0);
    },
    toggle: function toggle(e) {
      var t = arguments,
          n = e.guid || v.guid++,
          r = 0,
          i = function i(n) {
        var i = (v._data(this, "lastToggle" + e.guid) || 0) % r;
        return v._data(this, "lastToggle" + e.guid, i + 1), n.preventDefault(), t[i].apply(this, arguments) || !1;
      };

      i.guid = n;

      while (r < t.length) {
        t[r++].guid = n;
      }

      return this.click(i);
    },
    hover: function hover(e, t) {
      return this.mouseenter(e).mouseleave(t || e);
    }
  }), v.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (e, t) {
    v.fn[t] = function (e, n) {
      return n == null && (n = e, e = null), arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t);
    }, Q.test(t) && (v.event.fixHooks[t] = v.event.keyHooks), G.test(t) && (v.event.fixHooks[t] = v.event.mouseHooks);
  }), function (e, t) {
    function nt(e, t, n, r) {
      n = n || [], t = t || g;
      var i,
          s,
          a,
          f,
          l = t.nodeType;
      if (!e || typeof e != "string") return n;
      if (l !== 1 && l !== 9) return [];
      a = o(t);
      if (!a && !r) if (i = R.exec(e)) if (f = i[1]) {
        if (l === 9) {
          s = t.getElementById(f);
          if (!s || !s.parentNode) return n;
          if (s.id === f) return n.push(s), n;
        } else if (t.ownerDocument && (s = t.ownerDocument.getElementById(f)) && u(t, s) && s.id === f) return n.push(s), n;
      } else {
        if (i[2]) return S.apply(n, x.call(t.getElementsByTagName(e), 0)), n;
        if ((f = i[3]) && Z && t.getElementsByClassName) return S.apply(n, x.call(t.getElementsByClassName(f), 0)), n;
      }
      return vt(e.replace(j, "$1"), t, n, r, a);
    }

    function rt(e) {
      return function (t) {
        var n = t.nodeName.toLowerCase();
        return n === "input" && t.type === e;
      };
    }

    function it(e) {
      return function (t) {
        var n = t.nodeName.toLowerCase();
        return (n === "input" || n === "button") && t.type === e;
      };
    }

    function st(e) {
      return N(function (t) {
        return t = +t, N(function (n, r) {
          var i,
              s = e([], n.length, t),
              o = s.length;

          while (o--) {
            n[i = s[o]] && (n[i] = !(r[i] = n[i]));
          }
        });
      });
    }

    function ot(e, t, n) {
      if (e === t) return n;
      var r = e.nextSibling;

      while (r) {
        if (r === t) return -1;
        r = r.nextSibling;
      }

      return 1;
    }

    function ut(e, t) {
      var n,
          r,
          s,
          o,
          u,
          a,
          f,
          l = L[d][e + " "];
      if (l) return t ? 0 : l.slice(0);
      u = e, a = [], f = i.preFilter;

      while (u) {
        if (!n || (r = F.exec(u))) r && (u = u.slice(r[0].length) || u), a.push(s = []);
        n = !1;
        if (r = I.exec(u)) s.push(n = new m(r.shift())), u = u.slice(n.length), n.type = r[0].replace(j, " ");

        for (o in i.filter) {
          (r = J[o].exec(u)) && (!f[o] || (r = f[o](r))) && (s.push(n = new m(r.shift())), u = u.slice(n.length), n.type = o, n.matches = r);
        }

        if (!n) break;
      }

      return t ? u.length : u ? nt.error(e) : L(e, a).slice(0);
    }

    function at(e, t, r) {
      var i = t.dir,
          s = r && t.dir === "parentNode",
          o = w++;
      return t.first ? function (t, n, r) {
        while (t = t[i]) {
          if (s || t.nodeType === 1) return e(t, n, r);
        }
      } : function (t, r, u) {
        if (!u) {
          var a,
              f = b + " " + o + " ",
              l = f + n;

          while (t = t[i]) {
            if (s || t.nodeType === 1) {
              if ((a = t[d]) === l) return t.sizset;

              if (typeof a == "string" && a.indexOf(f) === 0) {
                if (t.sizset) return t;
              } else {
                t[d] = l;
                if (e(t, r, u)) return t.sizset = !0, t;
                t.sizset = !1;
              }
            }
          }
        } else while (t = t[i]) {
          if (s || t.nodeType === 1) if (e(t, r, u)) return t;
        }
      };
    }

    function ft(e) {
      return e.length > 1 ? function (t, n, r) {
        var i = e.length;

        while (i--) {
          if (!e[i](t, n, r)) return !1;
        }

        return !0;
      } : e[0];
    }

    function lt(e, t, n, r, i) {
      var s,
          o = [],
          u = 0,
          a = e.length,
          f = t != null;

      for (; u < a; u++) {
        if (s = e[u]) if (!n || n(s, r, i)) o.push(s), f && t.push(u);
      }

      return o;
    }

    function ct(e, t, n, r, i, s) {
      return r && !r[d] && (r = ct(r)), i && !i[d] && (i = ct(i, s)), N(function (s, o, u, a) {
        var f,
            l,
            c,
            h = [],
            p = [],
            d = o.length,
            v = s || dt(t || "*", u.nodeType ? [u] : u, []),
            m = e && (s || !t) ? lt(v, h, e, u, a) : v,
            g = n ? i || (s ? e : d || r) ? [] : o : m;
        n && n(m, g, u, a);

        if (r) {
          f = lt(g, p), r(f, [], u, a), l = f.length;

          while (l--) {
            if (c = f[l]) g[p[l]] = !(m[p[l]] = c);
          }
        }

        if (s) {
          if (i || e) {
            if (i) {
              f = [], l = g.length;

              while (l--) {
                (c = g[l]) && f.push(m[l] = c);
              }

              i(null, g = [], f, a);
            }

            l = g.length;

            while (l--) {
              (c = g[l]) && (f = i ? T.call(s, c) : h[l]) > -1 && (s[f] = !(o[f] = c));
            }
          }
        } else g = lt(g === o ? g.splice(d, g.length) : g), i ? i(null, o, g, a) : S.apply(o, g);
      });
    }

    function ht(e) {
      var t,
          n,
          r,
          s = e.length,
          o = i.relative[e[0].type],
          u = o || i.relative[" "],
          a = o ? 1 : 0,
          f = at(function (e) {
        return e === t;
      }, u, !0),
          l = at(function (e) {
        return T.call(t, e) > -1;
      }, u, !0),
          h = [function (e, n, r) {
        return !o && (r || n !== c) || ((t = n).nodeType ? f(e, n, r) : l(e, n, r));
      }];

      for (; a < s; a++) {
        if (n = i.relative[e[a].type]) h = [at(ft(h), n)];else {
          n = i.filter[e[a].type].apply(null, e[a].matches);

          if (n[d]) {
            r = ++a;

            for (; r < s; r++) {
              if (i.relative[e[r].type]) break;
            }

            return ct(a > 1 && ft(h), a > 1 && e.slice(0, a - 1).join("").replace(j, "$1"), n, a < r && ht(e.slice(a, r)), r < s && ht(e = e.slice(r)), r < s && e.join(""));
          }

          h.push(n);
        }
      }

      return ft(h);
    }

    function pt(e, t) {
      var r = t.length > 0,
          s = e.length > 0,
          o = function o(u, a, f, l, h) {
        var p,
            d,
            v,
            m = [],
            y = 0,
            w = "0",
            x = u && [],
            T = h != null,
            N = c,
            C = u || s && i.find.TAG("*", h && a.parentNode || a),
            k = b += N == null ? 1 : Math.E;
        T && (c = a !== g && a, n = o.el);

        for (; (p = C[w]) != null; w++) {
          if (s && p) {
            for (d = 0; v = e[d]; d++) {
              if (v(p, a, f)) {
                l.push(p);
                break;
              }
            }

            T && (b = k, n = ++o.el);
          }

          r && ((p = !v && p) && y--, u && x.push(p));
        }

        y += w;

        if (r && w !== y) {
          for (d = 0; v = t[d]; d++) {
            v(x, m, a, f);
          }

          if (u) {
            if (y > 0) while (w--) {
              !x[w] && !m[w] && (m[w] = E.call(l));
            }
            m = lt(m);
          }

          S.apply(l, m), T && !u && m.length > 0 && y + t.length > 1 && nt.uniqueSort(l);
        }

        return T && (b = k, c = N), x;
      };

      return o.el = 0, r ? N(o) : o;
    }

    function dt(e, t, n) {
      var r = 0,
          i = t.length;

      for (; r < i; r++) {
        nt(e, t[r], n);
      }

      return n;
    }

    function vt(e, t, n, r, s) {
      var o,
          u,
          f,
          l,
          c,
          h = ut(e),
          p = h.length;

      if (!r && h.length === 1) {
        u = h[0] = h[0].slice(0);

        if (u.length > 2 && (f = u[0]).type === "ID" && t.nodeType === 9 && !s && i.relative[u[1].type]) {
          t = i.find.ID(f.matches[0].replace($, ""), t, s)[0];
          if (!t) return n;
          e = e.slice(u.shift().length);
        }

        for (o = J.POS.test(e) ? -1 : u.length - 1; o >= 0; o--) {
          f = u[o];
          if (i.relative[l = f.type]) break;
          if (c = i.find[l]) if (r = c(f.matches[0].replace($, ""), z.test(u[0].type) && t.parentNode || t, s)) {
            u.splice(o, 1), e = r.length && u.join("");
            if (!e) return S.apply(n, x.call(r, 0)), n;
            break;
          }
        }
      }

      return a(e, h)(r, t, s, n, z.test(e)), n;
    }

    function mt() {}

    var n,
        r,
        i,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h = !0,
        p = "undefined",
        d = ("sizcache" + Math.random()).replace(".", ""),
        m = String,
        g = e.document,
        y = g.documentElement,
        b = 0,
        w = 0,
        E = [].pop,
        S = [].push,
        x = [].slice,
        T = [].indexOf || function (e) {
      var t = 0,
          n = this.length;

      for (; t < n; t++) {
        if (this[t] === e) return t;
      }

      return -1;
    },
        N = function N(e, t) {
      return e[d] = t == null || t, e;
    },
        C = function C() {
      var e = {},
          t = [];
      return N(function (n, r) {
        return t.push(n) > i.cacheLength && delete e[t.shift()], e[n + " "] = r;
      }, e);
    },
        k = C(),
        L = C(),
        A = C(),
        O = "[\\x20\\t\\r\\n\\f]",
        M = "(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",
        _ = M.replace("w", "w#"),
        D = "([*^$|!~]?=)",
        P = "\\[" + O + "*(" + M + ")" + O + "*(?:" + D + O + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + _ + ")|)|)" + O + "*\\]",
        H = ":(" + M + ")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:" + P + ")|[^:]|\\\\.)*|.*))\\)|)",
        B = ":(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + O + "*((?:-\\d)?\\d*)" + O + "*\\)|)(?=[^-]|$)",
        j = new RegExp("^" + O + "+|((?:^|[^\\\\])(?:\\\\.)*)" + O + "+$", "g"),
        F = new RegExp("^" + O + "*," + O + "*"),
        I = new RegExp("^" + O + "*([\\x20\\t\\r\\n\\f>+~])" + O + "*"),
        q = new RegExp(H),
        R = /^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,
        U = /^:not/,
        z = /[\x20\t\r\n\f]*[+~]/,
        W = /:not\($/,
        X = /h\d/i,
        V = /input|select|textarea|button/i,
        $ = /\\(?!\\)/g,
        J = {
      ID: new RegExp("^#(" + M + ")"),
      CLASS: new RegExp("^\\.(" + M + ")"),
      NAME: new RegExp("^\\[name=['\"]?(" + M + ")['\"]?\\]"),
      TAG: new RegExp("^(" + M.replace("w", "w*") + ")"),
      ATTR: new RegExp("^" + P),
      PSEUDO: new RegExp("^" + H),
      POS: new RegExp(B, "i"),
      CHILD: new RegExp("^:(only|nth|first|last)-child(?:\\(" + O + "*(even|odd|(([+-]|)(\\d*)n|)" + O + "*(?:([+-]|)" + O + "*(\\d+)|))" + O + "*\\)|)", "i"),
      needsContext: new RegExp("^" + O + "*[>+~]|" + B, "i")
    },
        K = function K(e) {
      var t = g.createElement("div");

      try {
        return e(t);
      } catch (n) {
        return !1;
      } finally {
        t = null;
      }
    },
        Q = K(function (e) {
      return e.appendChild(g.createComment("")), !e.getElementsByTagName("*").length;
    }),
        G = K(function (e) {
      return e.innerHTML = "<a href='#'></a>", e.firstChild && _typeof(e.firstChild.getAttribute) !== p && e.firstChild.getAttribute("href") === "#";
    }),
        Y = K(function (e) {
      e.innerHTML = "<select></select>";

      var t = _typeof(e.lastChild.getAttribute("multiple"));

      return t !== "boolean" && t !== "string";
    }),
        Z = K(function (e) {
      return e.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>", !e.getElementsByClassName || !e.getElementsByClassName("e").length ? !1 : (e.lastChild.className = "e", e.getElementsByClassName("e").length === 2);
    }),
        et = K(function (e) {
      e.id = d + 0, e.innerHTML = "<a name='" + d + "'></a><div name='" + d + "'></div>", y.insertBefore(e, y.firstChild);
      var t = g.getElementsByName && g.getElementsByName(d).length === 2 + g.getElementsByName(d + 0).length;
      return r = !g.getElementById(d), y.removeChild(e), t;
    });

    try {
      x.call(y.childNodes, 0)[0].nodeType;
    } catch (tt) {
      x = function x(e) {
        var t,
            n = [];

        for (; t = this[e]; e++) {
          n.push(t);
        }

        return n;
      };
    }

    nt.matches = function (e, t) {
      return nt(e, null, null, t);
    }, nt.matchesSelector = function (e, t) {
      return nt(t, null, null, [e]).length > 0;
    }, s = nt.getText = function (e) {
      var t,
          n = "",
          r = 0,
          i = e.nodeType;

      if (i) {
        if (i === 1 || i === 9 || i === 11) {
          if (typeof e.textContent == "string") return e.textContent;

          for (e = e.firstChild; e; e = e.nextSibling) {
            n += s(e);
          }
        } else if (i === 3 || i === 4) return e.nodeValue;
      } else for (; t = e[r]; r++) {
        n += s(t);
      }

      return n;
    }, o = nt.isXML = function (e) {
      var t = e && (e.ownerDocument || e).documentElement;
      return t ? t.nodeName !== "HTML" : !1;
    }, u = nt.contains = y.contains ? function (e, t) {
      var n = e.nodeType === 9 ? e.documentElement : e,
          r = t && t.parentNode;
      return e === r || !!(r && r.nodeType === 1 && n.contains && n.contains(r));
    } : y.compareDocumentPosition ? function (e, t) {
      return t && !!(e.compareDocumentPosition(t) & 16);
    } : function (e, t) {
      while (t = t.parentNode) {
        if (t === e) return !0;
      }

      return !1;
    }, nt.attr = function (e, t) {
      var n,
          r = o(e);
      return r || (t = t.toLowerCase()), (n = i.attrHandle[t]) ? n(e) : r || Y ? e.getAttribute(t) : (n = e.getAttributeNode(t), n ? typeof e[t] == "boolean" ? e[t] ? t : null : n.specified ? n.value : null : null);
    }, i = nt.selectors = {
      cacheLength: 50,
      createPseudo: N,
      match: J,
      attrHandle: G ? {} : {
        href: function href(e) {
          return e.getAttribute("href", 2);
        },
        type: function type(e) {
          return e.getAttribute("type");
        }
      },
      find: {
        ID: r ? function (e, t, n) {
          if (_typeof(t.getElementById) !== p && !n) {
            var r = t.getElementById(e);
            return r && r.parentNode ? [r] : [];
          }
        } : function (e, n, r) {
          if (_typeof(n.getElementById) !== p && !r) {
            var i = n.getElementById(e);
            return i ? i.id === e || _typeof(i.getAttributeNode) !== p && i.getAttributeNode("id").value === e ? [i] : t : [];
          }
        },
        TAG: Q ? function (e, t) {
          if (_typeof(t.getElementsByTagName) !== p) return t.getElementsByTagName(e);
        } : function (e, t) {
          var n = t.getElementsByTagName(e);

          if (e === "*") {
            var r,
                i = [],
                s = 0;

            for (; r = n[s]; s++) {
              r.nodeType === 1 && i.push(r);
            }

            return i;
          }

          return n;
        },
        NAME: et && function (e, t) {
          if (_typeof(t.getElementsByName) !== p) return t.getElementsByName(name);
        },
        CLASS: Z && function (e, t, n) {
          if (_typeof(t.getElementsByClassName) !== p && !n) return t.getElementsByClassName(e);
        }
      },
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function ATTR(e) {
          return e[1] = e[1].replace($, ""), e[3] = (e[4] || e[5] || "").replace($, ""), e[2] === "~=" && (e[3] = " " + e[3] + " "), e.slice(0, 4);
        },
        CHILD: function CHILD(e) {
          return e[1] = e[1].toLowerCase(), e[1] === "nth" ? (e[2] || nt.error(e[0]), e[3] = +(e[3] ? e[4] + (e[5] || 1) : 2 * (e[2] === "even" || e[2] === "odd")), e[4] = +(e[6] + e[7] || e[2] === "odd")) : e[2] && nt.error(e[0]), e;
        },
        PSEUDO: function PSEUDO(e) {
          var t, n;
          if (J.CHILD.test(e[0])) return null;
          if (e[3]) e[2] = e[3];else if (t = e[4]) q.test(t) && (n = ut(t, !0)) && (n = t.indexOf(")", t.length - n) - t.length) && (t = t.slice(0, n), e[0] = e[0].slice(0, n)), e[2] = t;
          return e.slice(0, 3);
        }
      },
      filter: {
        ID: r ? function (e) {
          return e = e.replace($, ""), function (t) {
            return t.getAttribute("id") === e;
          };
        } : function (e) {
          return e = e.replace($, ""), function (t) {
            var n = _typeof(t.getAttributeNode) !== p && t.getAttributeNode("id");
            return n && n.value === e;
          };
        },
        TAG: function TAG(e) {
          return e === "*" ? function () {
            return !0;
          } : (e = e.replace($, "").toLowerCase(), function (t) {
            return t.nodeName && t.nodeName.toLowerCase() === e;
          });
        },
        CLASS: function CLASS(e) {
          var t = k[d][e + " "];
          return t || (t = new RegExp("(^|" + O + ")" + e + "(" + O + "|$)")) && k(e, function (e) {
            return t.test(e.className || _typeof(e.getAttribute) !== p && e.getAttribute("class") || "");
          });
        },
        ATTR: function ATTR(e, t, n) {
          return function (r, i) {
            var s = nt.attr(r, e);
            return s == null ? t === "!=" : t ? (s += "", t === "=" ? s === n : t === "!=" ? s !== n : t === "^=" ? n && s.indexOf(n) === 0 : t === "*=" ? n && s.indexOf(n) > -1 : t === "$=" ? n && s.substr(s.length - n.length) === n : t === "~=" ? (" " + s + " ").indexOf(n) > -1 : t === "|=" ? s === n || s.substr(0, n.length + 1) === n + "-" : !1) : !0;
          };
        },
        CHILD: function CHILD(e, t, n, r) {
          return e === "nth" ? function (e) {
            var t,
                i,
                s = e.parentNode;
            if (n === 1 && r === 0) return !0;

            if (s) {
              i = 0;

              for (t = s.firstChild; t; t = t.nextSibling) {
                if (t.nodeType === 1) {
                  i++;
                  if (e === t) break;
                }
              }
            }

            return i -= r, i === n || i % n === 0 && i / n >= 0;
          } : function (t) {
            var n = t;

            switch (e) {
              case "only":
              case "first":
                while (n = n.previousSibling) {
                  if (n.nodeType === 1) return !1;
                }

                if (e === "first") return !0;
                n = t;

              case "last":
                while (n = n.nextSibling) {
                  if (n.nodeType === 1) return !1;
                }

                return !0;
            }
          };
        },
        PSEUDO: function PSEUDO(e, t) {
          var n,
              r = i.pseudos[e] || i.setFilters[e.toLowerCase()] || nt.error("unsupported pseudo: " + e);
          return r[d] ? r(t) : r.length > 1 ? (n = [e, e, "", t], i.setFilters.hasOwnProperty(e.toLowerCase()) ? N(function (e, n) {
            var i,
                s = r(e, t),
                o = s.length;

            while (o--) {
              i = T.call(e, s[o]), e[i] = !(n[i] = s[o]);
            }
          }) : function (e) {
            return r(e, 0, n);
          }) : r;
        }
      },
      pseudos: {
        not: N(function (e) {
          var t = [],
              n = [],
              r = a(e.replace(j, "$1"));
          return r[d] ? N(function (e, t, n, i) {
            var s,
                o = r(e, null, i, []),
                u = e.length;

            while (u--) {
              if (s = o[u]) e[u] = !(t[u] = s);
            }
          }) : function (e, i, s) {
            return t[0] = e, r(t, null, s, n), !n.pop();
          };
        }),
        has: N(function (e) {
          return function (t) {
            return nt(e, t).length > 0;
          };
        }),
        contains: N(function (e) {
          return function (t) {
            return (t.textContent || t.innerText || s(t)).indexOf(e) > -1;
          };
        }),
        enabled: function enabled(e) {
          return e.disabled === !1;
        },
        disabled: function disabled(e) {
          return e.disabled === !0;
        },
        checked: function checked(e) {
          var t = e.nodeName.toLowerCase();
          return t === "input" && !!e.checked || t === "option" && !!e.selected;
        },
        selected: function selected(e) {
          return e.parentNode && e.parentNode.selectedIndex, e.selected === !0;
        },
        parent: function parent(e) {
          return !i.pseudos.empty(e);
        },
        empty: function empty(e) {
          var t;
          e = e.firstChild;

          while (e) {
            if (e.nodeName > "@" || (t = e.nodeType) === 3 || t === 4) return !1;
            e = e.nextSibling;
          }

          return !0;
        },
        header: function header(e) {
          return X.test(e.nodeName);
        },
        text: function text(e) {
          var t, n;
          return e.nodeName.toLowerCase() === "input" && (t = e.type) === "text" && ((n = e.getAttribute("type")) == null || n.toLowerCase() === t);
        },
        radio: rt("radio"),
        checkbox: rt("checkbox"),
        file: rt("file"),
        password: rt("password"),
        image: rt("image"),
        submit: it("submit"),
        reset: it("reset"),
        button: function button(e) {
          var t = e.nodeName.toLowerCase();
          return t === "input" && e.type === "button" || t === "button";
        },
        input: function input(e) {
          return V.test(e.nodeName);
        },
        focus: function focus(e) {
          var t = e.ownerDocument;
          return e === t.activeElement && (!t.hasFocus || t.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
        },
        active: function active(e) {
          return e === e.ownerDocument.activeElement;
        },
        first: st(function () {
          return [0];
        }),
        last: st(function (e, t) {
          return [t - 1];
        }),
        eq: st(function (e, t, n) {
          return [n < 0 ? n + t : n];
        }),
        even: st(function (e, t) {
          for (var n = 0; n < t; n += 2) {
            e.push(n);
          }

          return e;
        }),
        odd: st(function (e, t) {
          for (var n = 1; n < t; n += 2) {
            e.push(n);
          }

          return e;
        }),
        lt: st(function (e, t, n) {
          for (var r = n < 0 ? n + t : n; --r >= 0;) {
            e.push(r);
          }

          return e;
        }),
        gt: st(function (e, t, n) {
          for (var r = n < 0 ? n + t : n; ++r < t;) {
            e.push(r);
          }

          return e;
        })
      }
    }, f = y.compareDocumentPosition ? function (e, t) {
      return e === t ? (l = !0, 0) : (!e.compareDocumentPosition || !t.compareDocumentPosition ? e.compareDocumentPosition : e.compareDocumentPosition(t) & 4) ? -1 : 1;
    } : function (e, t) {
      if (e === t) return l = !0, 0;
      if (e.sourceIndex && t.sourceIndex) return e.sourceIndex - t.sourceIndex;
      var n,
          r,
          i = [],
          s = [],
          o = e.parentNode,
          u = t.parentNode,
          a = o;
      if (o === u) return ot(e, t);
      if (!o) return -1;
      if (!u) return 1;

      while (a) {
        i.unshift(a), a = a.parentNode;
      }

      a = u;

      while (a) {
        s.unshift(a), a = a.parentNode;
      }

      n = i.length, r = s.length;

      for (var f = 0; f < n && f < r; f++) {
        if (i[f] !== s[f]) return ot(i[f], s[f]);
      }

      return f === n ? ot(e, s[f], -1) : ot(i[f], t, 1);
    }, [0, 0].sort(f), h = !l, nt.uniqueSort = function (e) {
      var t,
          n = [],
          r = 1,
          i = 0;
      l = h, e.sort(f);

      if (l) {
        for (; t = e[r]; r++) {
          t === e[r - 1] && (i = n.push(r));
        }

        while (i--) {
          e.splice(n[i], 1);
        }
      }

      return e;
    }, nt.error = function (e) {
      throw new Error("Syntax error, unrecognized expression: " + e);
    }, a = nt.compile = function (e, t) {
      var n,
          r = [],
          i = [],
          s = A[d][e + " "];

      if (!s) {
        t || (t = ut(e)), n = t.length;

        while (n--) {
          s = ht(t[n]), s[d] ? r.push(s) : i.push(s);
        }

        s = A(e, pt(i, r));
      }

      return s;
    }, g.querySelectorAll && function () {
      var e,
          t = vt,
          n = /'|\\/g,
          r = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
          i = [":focus"],
          s = [":active"],
          u = y.matchesSelector || y.mozMatchesSelector || y.webkitMatchesSelector || y.oMatchesSelector || y.msMatchesSelector;
      K(function (e) {
        e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || i.push("\\[" + O + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"), e.querySelectorAll(":checked").length || i.push(":checked");
      }), K(function (e) {
        e.innerHTML = "<p test=''></p>", e.querySelectorAll("[test^='']").length && i.push("[*^$]=" + O + "*(?:\"\"|'')"), e.innerHTML = "<input type='hidden'/>", e.querySelectorAll(":enabled").length || i.push(":enabled", ":disabled");
      }), i = new RegExp(i.join("|")), vt = function vt(e, r, s, o, u) {
        if (!o && !u && !i.test(e)) {
          var a,
              f,
              l = !0,
              c = d,
              h = r,
              p = r.nodeType === 9 && e;

          if (r.nodeType === 1 && r.nodeName.toLowerCase() !== "object") {
            a = ut(e), (l = r.getAttribute("id")) ? c = l.replace(n, "\\$&") : r.setAttribute("id", c), c = "[id='" + c + "'] ", f = a.length;

            while (f--) {
              a[f] = c + a[f].join("");
            }

            h = z.test(e) && r.parentNode || r, p = a.join(",");
          }

          if (p) try {
            return S.apply(s, x.call(h.querySelectorAll(p), 0)), s;
          } catch (v) {} finally {
            l || r.removeAttribute("id");
          }
        }

        return t(e, r, s, o, u);
      }, u && (K(function (t) {
        e = u.call(t, "div");

        try {
          u.call(t, "[test!='']:sizzle"), s.push("!=", H);
        } catch (n) {}
      }), s = new RegExp(s.join("|")), nt.matchesSelector = function (t, n) {
        n = n.replace(r, "='$1']");
        if (!o(t) && !s.test(n) && !i.test(n)) try {
          var a = u.call(t, n);
          if (a || e || t.document && t.document.nodeType !== 11) return a;
        } catch (f) {}
        return nt(n, null, null, [t]).length > 0;
      });
    }(), i.pseudos.nth = i.pseudos.eq, i.filters = mt.prototype = i.pseudos, i.setFilters = new mt(), nt.attr = v.attr, v.find = nt, v.expr = nt.selectors, v.expr[":"] = v.expr.pseudos, v.unique = nt.uniqueSort, v.text = nt.getText, v.isXMLDoc = nt.isXML, v.contains = nt.contains;
  }(e);
  var nt = /Until$/,
      rt = /^(?:parents|prev(?:Until|All))/,
      it = /^.[^:#\[\.,]*$/,
      st = v.expr.match.needsContext,
      ot = {
    children: !0,
    contents: !0,
    next: !0,
    prev: !0
  };
  v.fn.extend({
    find: function find(e) {
      var t,
          n,
          r,
          i,
          s,
          o,
          u = this;
      if (typeof e != "string") return v(e).filter(function () {
        for (t = 0, n = u.length; t < n; t++) {
          if (v.contains(u[t], this)) return !0;
        }
      });
      o = this.pushStack("", "find", e);

      for (t = 0, n = this.length; t < n; t++) {
        r = o.length, v.find(e, this[t], o);
        if (t > 0) for (i = r; i < o.length; i++) {
          for (s = 0; s < r; s++) {
            if (o[s] === o[i]) {
              o.splice(i--, 1);
              break;
            }
          }
        }
      }

      return o;
    },
    has: function has(e) {
      var t,
          n = v(e, this),
          r = n.length;
      return this.filter(function () {
        for (t = 0; t < r; t++) {
          if (v.contains(this, n[t])) return !0;
        }
      });
    },
    not: function not(e) {
      return this.pushStack(ft(this, e, !1), "not", e);
    },
    filter: function filter(e) {
      return this.pushStack(ft(this, e, !0), "filter", e);
    },
    is: function is(e) {
      return !!e && (typeof e == "string" ? st.test(e) ? v(e, this.context).index(this[0]) >= 0 : v.filter(e, this).length > 0 : this.filter(e).length > 0);
    },
    closest: function closest(e, t) {
      var n,
          r = 0,
          i = this.length,
          s = [],
          o = st.test(e) || typeof e != "string" ? v(e, t || this.context) : 0;

      for (; r < i; r++) {
        n = this[r];

        while (n && n.ownerDocument && n !== t && n.nodeType !== 11) {
          if (o ? o.index(n) > -1 : v.find.matchesSelector(n, e)) {
            s.push(n);
            break;
          }

          n = n.parentNode;
        }
      }

      return s = s.length > 1 ? v.unique(s) : s, this.pushStack(s, "closest", e);
    },
    index: function index(e) {
      return e ? typeof e == "string" ? v.inArray(this[0], v(e)) : v.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.prevAll().length : -1;
    },
    add: function add(e, t) {
      var n = typeof e == "string" ? v(e, t) : v.makeArray(e && e.nodeType ? [e] : e),
          r = v.merge(this.get(), n);
      return this.pushStack(ut(n[0]) || ut(r[0]) ? r : v.unique(r));
    },
    addBack: function addBack(e) {
      return this.add(e == null ? this.prevObject : this.prevObject.filter(e));
    }
  }), v.fn.andSelf = v.fn.addBack, v.each({
    parent: function parent(e) {
      var t = e.parentNode;
      return t && t.nodeType !== 11 ? t : null;
    },
    parents: function parents(e) {
      return v.dir(e, "parentNode");
    },
    parentsUntil: function parentsUntil(e, t, n) {
      return v.dir(e, "parentNode", n);
    },
    next: function next(e) {
      return at(e, "nextSibling");
    },
    prev: function prev(e) {
      return at(e, "previousSibling");
    },
    nextAll: function nextAll(e) {
      return v.dir(e, "nextSibling");
    },
    prevAll: function prevAll(e) {
      return v.dir(e, "previousSibling");
    },
    nextUntil: function nextUntil(e, t, n) {
      return v.dir(e, "nextSibling", n);
    },
    prevUntil: function prevUntil(e, t, n) {
      return v.dir(e, "previousSibling", n);
    },
    siblings: function siblings(e) {
      return v.sibling((e.parentNode || {}).firstChild, e);
    },
    children: function children(e) {
      return v.sibling(e.firstChild);
    },
    contents: function contents(e) {
      return v.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : v.merge([], e.childNodes);
    }
  }, function (e, t) {
    v.fn[e] = function (n, r) {
      var i = v.map(this, t, n);
      return nt.test(e) || (r = n), r && typeof r == "string" && (i = v.filter(r, i)), i = this.length > 1 && !ot[e] ? v.unique(i) : i, this.length > 1 && rt.test(e) && (i = i.reverse()), this.pushStack(i, e, l.call(arguments).join(","));
    };
  }), v.extend({
    filter: function filter(e, t, n) {
      return n && (e = ":not(" + e + ")"), t.length === 1 ? v.find.matchesSelector(t[0], e) ? [t[0]] : [] : v.find.matches(e, t);
    },
    dir: function dir(e, n, r) {
      var i = [],
          s = e[n];

      while (s && s.nodeType !== 9 && (r === t || s.nodeType !== 1 || !v(s).is(r))) {
        s.nodeType === 1 && i.push(s), s = s[n];
      }

      return i;
    },
    sibling: function sibling(e, t) {
      var n = [];

      for (; e; e = e.nextSibling) {
        e.nodeType === 1 && e !== t && n.push(e);
      }

      return n;
    }
  });
  var ct = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
      ht = / jQuery\d+="(?:null|\d+)"/g,
      pt = /^\s+/,
      dt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
      vt = /<([\w:]+)/,
      mt = /<tbody/i,
      gt = /<|&#?\w+;/,
      yt = /<(?:script|style|link)/i,
      bt = /<(?:script|object|embed|option|style)/i,
      wt = new RegExp("<(?:" + ct + ")[\\s/>]", "i"),
      Et = /^(?:checkbox|radio)$/,
      St = /checked\s*(?:[^=]|=\s*.checked.)/i,
      xt = /\/(java|ecma)script/i,
      Tt = /^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,
      Nt = {
    option: [1, "<select multiple='multiple'>", "</select>"],
    legend: [1, "<fieldset>", "</fieldset>"],
    thead: [1, "<table>", "</table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
    area: [1, "<map>", "</map>"],
    _default: [0, "", ""]
  },
      Ct = lt(i),
      kt = Ct.appendChild(i.createElement("div"));
  Nt.optgroup = Nt.option, Nt.tbody = Nt.tfoot = Nt.colgroup = Nt.caption = Nt.thead, Nt.th = Nt.td, v.support.htmlSerialize || (Nt._default = [1, "X<div>", "</div>"]), v.fn.extend({
    text: function text(e) {
      return v.access(this, function (e) {
        return e === t ? v.text(this) : this.empty().append((this[0] && this[0].ownerDocument || i).createTextNode(e));
      }, null, e, arguments.length);
    },
    wrapAll: function wrapAll(e) {
      if (v.isFunction(e)) return this.each(function (t) {
        v(this).wrapAll(e.call(this, t));
      });

      if (this[0]) {
        var t = v(e, this[0].ownerDocument).eq(0).clone(!0);
        this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
          var e = this;

          while (e.firstChild && e.firstChild.nodeType === 1) {
            e = e.firstChild;
          }

          return e;
        }).append(this);
      }

      return this;
    },
    wrapInner: function wrapInner(e) {
      return v.isFunction(e) ? this.each(function (t) {
        v(this).wrapInner(e.call(this, t));
      }) : this.each(function () {
        var t = v(this),
            n = t.contents();
        n.length ? n.wrapAll(e) : t.append(e);
      });
    },
    wrap: function wrap(e) {
      var t = v.isFunction(e);
      return this.each(function (n) {
        v(this).wrapAll(t ? e.call(this, n) : e);
      });
    },
    unwrap: function unwrap() {
      return this.parent().each(function () {
        v.nodeName(this, "body") || v(this).replaceWith(this.childNodes);
      }).end();
    },
    append: function append() {
      return this.domManip(arguments, !0, function (e) {
        (this.nodeType === 1 || this.nodeType === 11) && this.appendChild(e);
      });
    },
    prepend: function prepend() {
      return this.domManip(arguments, !0, function (e) {
        (this.nodeType === 1 || this.nodeType === 11) && this.insertBefore(e, this.firstChild);
      });
    },
    before: function before() {
      if (!ut(this[0])) return this.domManip(arguments, !1, function (e) {
        this.parentNode.insertBefore(e, this);
      });

      if (arguments.length) {
        var e = v.clean(arguments);
        return this.pushStack(v.merge(e, this), "before", this.selector);
      }
    },
    after: function after() {
      if (!ut(this[0])) return this.domManip(arguments, !1, function (e) {
        this.parentNode.insertBefore(e, this.nextSibling);
      });

      if (arguments.length) {
        var e = v.clean(arguments);
        return this.pushStack(v.merge(this, e), "after", this.selector);
      }
    },
    remove: function remove(e, t) {
      var n,
          r = 0;

      for (; (n = this[r]) != null; r++) {
        if (!e || v.filter(e, [n]).length) !t && n.nodeType === 1 && (v.cleanData(n.getElementsByTagName("*")), v.cleanData([n])), n.parentNode && n.parentNode.removeChild(n);
      }

      return this;
    },
    empty: function empty() {
      var e,
          t = 0;

      for (; (e = this[t]) != null; t++) {
        e.nodeType === 1 && v.cleanData(e.getElementsByTagName("*"));

        while (e.firstChild) {
          e.removeChild(e.firstChild);
        }
      }

      return this;
    },
    clone: function clone(e, t) {
      return e = e == null ? !1 : e, t = t == null ? e : t, this.map(function () {
        return v.clone(this, e, t);
      });
    },
    html: function html(e) {
      return v.access(this, function (e) {
        var n = this[0] || {},
            r = 0,
            i = this.length;
        if (e === t) return n.nodeType === 1 ? n.innerHTML.replace(ht, "") : t;

        if (typeof e == "string" && !yt.test(e) && (v.support.htmlSerialize || !wt.test(e)) && (v.support.leadingWhitespace || !pt.test(e)) && !Nt[(vt.exec(e) || ["", ""])[1].toLowerCase()]) {
          e = e.replace(dt, "<$1></$2>");

          try {
            for (; r < i; r++) {
              n = this[r] || {}, n.nodeType === 1 && (v.cleanData(n.getElementsByTagName("*")), n.innerHTML = e);
            }

            n = 0;
          } catch (s) {}
        }

        n && this.empty().append(e);
      }, null, e, arguments.length);
    },
    replaceWith: function replaceWith(e) {
      return ut(this[0]) ? this.length ? this.pushStack(v(v.isFunction(e) ? e() : e), "replaceWith", e) : this : v.isFunction(e) ? this.each(function (t) {
        var n = v(this),
            r = n.html();
        n.replaceWith(e.call(this, t, r));
      }) : (typeof e != "string" && (e = v(e).detach()), this.each(function () {
        var t = this.nextSibling,
            n = this.parentNode;
        v(this).remove(), t ? v(t).before(e) : v(n).append(e);
      }));
    },
    detach: function detach(e) {
      return this.remove(e, !0);
    },
    domManip: function domManip(e, n, r) {
      e = [].concat.apply([], e);
      var i,
          s,
          o,
          u,
          a = 0,
          f = e[0],
          l = [],
          c = this.length;
      if (!v.support.checkClone && c > 1 && typeof f == "string" && St.test(f)) return this.each(function () {
        v(this).domManip(e, n, r);
      });
      if (v.isFunction(f)) return this.each(function (i) {
        var s = v(this);
        e[0] = f.call(this, i, n ? s.html() : t), s.domManip(e, n, r);
      });

      if (this[0]) {
        i = v.buildFragment(e, this, l), o = i.fragment, s = o.firstChild, o.childNodes.length === 1 && (o = s);

        if (s) {
          n = n && v.nodeName(s, "tr");

          for (u = i.cacheable || c - 1; a < c; a++) {
            r.call(n && v.nodeName(this[a], "table") ? Lt(this[a], "tbody") : this[a], a === u ? o : v.clone(o, !0, !0));
          }
        }

        o = s = null, l.length && v.each(l, function (e, t) {
          t.src ? v.ajax ? v.ajax({
            url: t.src,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
          }) : v.error("no ajax") : v.globalEval((t.text || t.textContent || t.innerHTML || "").replace(Tt, "")), t.parentNode && t.parentNode.removeChild(t);
        });
      }

      return this;
    }
  }), v.buildFragment = function (e, n, r) {
    var s,
        o,
        u,
        a = e[0];
    return n = n || i, n = !n.nodeType && n[0] || n, n = n.ownerDocument || n, e.length === 1 && typeof a == "string" && a.length < 512 && n === i && a.charAt(0) === "<" && !bt.test(a) && (v.support.checkClone || !St.test(a)) && (v.support.html5Clone || !wt.test(a)) && (o = !0, s = v.fragments[a], u = s !== t), s || (s = n.createDocumentFragment(), v.clean(e, n, s, r), o && (v.fragments[a] = u && s)), {
      fragment: s,
      cacheable: o
    };
  }, v.fragments = {}, v.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (e, t) {
    v.fn[e] = function (n) {
      var r,
          i = 0,
          s = [],
          o = v(n),
          u = o.length,
          a = this.length === 1 && this[0].parentNode;
      if ((a == null || a && a.nodeType === 11 && a.childNodes.length === 1) && u === 1) return o[t](this[0]), this;

      for (; i < u; i++) {
        r = (i > 0 ? this.clone(!0) : this).get(), v(o[i])[t](r), s = s.concat(r);
      }

      return this.pushStack(s, e, o.selector);
    };
  }), v.extend({
    clone: function clone(e, t, n) {
      var r, i, s, o;
      v.support.html5Clone || v.isXMLDoc(e) || !wt.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (kt.innerHTML = e.outerHTML, kt.removeChild(o = kt.firstChild));

      if ((!v.support.noCloneEvent || !v.support.noCloneChecked) && (e.nodeType === 1 || e.nodeType === 11) && !v.isXMLDoc(e)) {
        Ot(e, o), r = Mt(e), i = Mt(o);

        for (s = 0; r[s]; ++s) {
          i[s] && Ot(r[s], i[s]);
        }
      }

      if (t) {
        At(e, o);

        if (n) {
          r = Mt(e), i = Mt(o);

          for (s = 0; r[s]; ++s) {
            At(r[s], i[s]);
          }
        }
      }

      return r = i = null, o;
    },
    clean: function clean(e, t, n, r) {
      var s,
          o,
          u,
          a,
          f,
          l,
          c,
          h,
          p,
          d,
          m,
          g,
          y = t === i && Ct,
          b = [];
      if (!t || typeof t.createDocumentFragment == "undefined") t = i;

      for (s = 0; (u = e[s]) != null; s++) {
        typeof u == "number" && (u += "");
        if (!u) continue;
        if (typeof u == "string") if (!gt.test(u)) u = t.createTextNode(u);else {
          y = y || lt(t), c = t.createElement("div"), y.appendChild(c), u = u.replace(dt, "<$1></$2>"), a = (vt.exec(u) || ["", ""])[1].toLowerCase(), f = Nt[a] || Nt._default, l = f[0], c.innerHTML = f[1] + u + f[2];

          while (l--) {
            c = c.lastChild;
          }

          if (!v.support.tbody) {
            h = mt.test(u), p = a === "table" && !h ? c.firstChild && c.firstChild.childNodes : f[1] === "<table>" && !h ? c.childNodes : [];

            for (o = p.length - 1; o >= 0; --o) {
              v.nodeName(p[o], "tbody") && !p[o].childNodes.length && p[o].parentNode.removeChild(p[o]);
            }
          }

          !v.support.leadingWhitespace && pt.test(u) && c.insertBefore(t.createTextNode(pt.exec(u)[0]), c.firstChild), u = c.childNodes, c.parentNode.removeChild(c);
        }
        u.nodeType ? b.push(u) : v.merge(b, u);
      }

      c && (u = c = y = null);
      if (!v.support.appendChecked) for (s = 0; (u = b[s]) != null; s++) {
        v.nodeName(u, "input") ? _t(u) : typeof u.getElementsByTagName != "undefined" && v.grep(u.getElementsByTagName("input"), _t);
      }

      if (n) {
        m = function m(e) {
          if (!e.type || xt.test(e.type)) return r ? r.push(e.parentNode ? e.parentNode.removeChild(e) : e) : n.appendChild(e);
        };

        for (s = 0; (u = b[s]) != null; s++) {
          if (!v.nodeName(u, "script") || !m(u)) n.appendChild(u), typeof u.getElementsByTagName != "undefined" && (g = v.grep(v.merge([], u.getElementsByTagName("script")), m), b.splice.apply(b, [s + 1, 0].concat(g)), s += g.length);
        }
      }

      return b;
    },
    cleanData: function cleanData(e, t) {
      var n,
          r,
          i,
          s,
          o = 0,
          u = v.expando,
          a = v.cache,
          f = v.support.deleteExpando,
          l = v.event.special;

      for (; (i = e[o]) != null; o++) {
        if (t || v.acceptData(i)) {
          r = i[u], n = r && a[r];

          if (n) {
            if (n.events) for (s in n.events) {
              l[s] ? v.event.remove(i, s) : v.removeEvent(i, s, n.handle);
            }
            a[r] && (delete a[r], f ? delete i[u] : i.removeAttribute ? i.removeAttribute(u) : i[u] = null, v.deletedIds.push(r));
          }
        }
      }
    }
  }), function () {
    var e, t;
    v.uaMatch = function (e) {
      e = e.toLowerCase();
      var t = /(chrome)[ \/]([\w.]+)/.exec(e) || /(webkit)[ \/]([\w.]+)/.exec(e) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) || /(msie) ([\w.]+)/.exec(e) || e.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e) || [];
      return {
        browser: t[1] || "",
        version: t[2] || "0"
      };
    }, e = v.uaMatch(o.userAgent), t = {}, e.browser && (t[e.browser] = !0, t.version = e.version), t.chrome ? t.webkit = !0 : t.webkit && (t.safari = !0), v.browser = t, v.sub = function () {
      function e(t, n) {
        return new e.fn.init(t, n);
      }

      v.extend(!0, e, this), e.superclass = this, e.fn = e.prototype = this(), e.fn.constructor = e, e.sub = this.sub, e.fn.init = function (r, i) {
        return i && i instanceof v && !(i instanceof e) && (i = e(i)), v.fn.init.call(this, r, i, t);
      }, e.fn.init.prototype = e.fn;
      var t = e(i);
      return e;
    };
  }();
  var Dt,
      Pt,
      Ht,
      Bt = /alpha\([^)]*\)/i,
      jt = /opacity=([^)]*)/,
      Ft = /^(top|right|bottom|left)$/,
      It = /^(none|table(?!-c[ea]).+)/,
      qt = /^margin/,
      Rt = new RegExp("^(" + m + ")(.*)$", "i"),
      Ut = new RegExp("^(" + m + ")(?!px)[a-z%]+$", "i"),
      zt = new RegExp("^([-+])=(" + m + ")", "i"),
      Wt = {
    BODY: "block"
  },
      Xt = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  },
      Vt = {
    letterSpacing: 0,
    fontWeight: 400
  },
      $t = ["Top", "Right", "Bottom", "Left"],
      Jt = ["Webkit", "O", "Moz", "ms"],
      Kt = v.fn.toggle;
  v.fn.extend({
    css: function css(e, n) {
      return v.access(this, function (e, n, r) {
        return r !== t ? v.style(e, n, r) : v.css(e, n);
      }, e, n, arguments.length > 1);
    },
    show: function show() {
      return Yt(this, !0);
    },
    hide: function hide() {
      return Yt(this);
    },
    toggle: function toggle(e, t) {
      var n = typeof e == "boolean";
      return v.isFunction(e) && v.isFunction(t) ? Kt.apply(this, arguments) : this.each(function () {
        (n ? e : Gt(this)) ? v(this).show() : v(this).hide();
      });
    }
  }), v.extend({
    cssHooks: {
      opacity: {
        get: function get(e, t) {
          if (t) {
            var n = Dt(e, "opacity");
            return n === "" ? "1" : n;
          }
        }
      }
    },
    cssNumber: {
      fillOpacity: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {
      "float": v.support.cssFloat ? "cssFloat" : "styleFloat"
    },
    style: function style(e, n, r, i) {
      if (!e || e.nodeType === 3 || e.nodeType === 8 || !e.style) return;
      var s,
          o,
          u,
          a = v.camelCase(n),
          f = e.style;
      n = v.cssProps[a] || (v.cssProps[a] = Qt(f, a)), u = v.cssHooks[n] || v.cssHooks[a];
      if (r === t) return u && "get" in u && (s = u.get(e, !1, i)) !== t ? s : f[n];
      o = _typeof(r), o === "string" && (s = zt.exec(r)) && (r = (s[1] + 1) * s[2] + parseFloat(v.css(e, n)), o = "number");
      if (r == null || o === "number" && isNaN(r)) return;
      o === "number" && !v.cssNumber[a] && (r += "px");
      if (!u || !("set" in u) || (r = u.set(e, r, i)) !== t) try {
        f[n] = r;
      } catch (l) {}
    },
    css: function css(e, n, r, i) {
      var s,
          o,
          u,
          a = v.camelCase(n);
      return n = v.cssProps[a] || (v.cssProps[a] = Qt(e.style, a)), u = v.cssHooks[n] || v.cssHooks[a], u && "get" in u && (s = u.get(e, !0, i)), s === t && (s = Dt(e, n)), s === "normal" && n in Vt && (s = Vt[n]), r || i !== t ? (o = parseFloat(s), r || v.isNumeric(o) ? o || 0 : s) : s;
    },
    swap: function swap(e, t, n) {
      var r,
          i,
          s = {};

      for (i in t) {
        s[i] = e.style[i], e.style[i] = t[i];
      }

      r = n.call(e);

      for (i in t) {
        e.style[i] = s[i];
      }

      return r;
    }
  }), e.getComputedStyle ? Dt = function Dt(t, n) {
    var r,
        i,
        s,
        o,
        u = e.getComputedStyle(t, null),
        a = t.style;
    return u && (r = u.getPropertyValue(n) || u[n], r === "" && !v.contains(t.ownerDocument, t) && (r = v.style(t, n)), Ut.test(r) && qt.test(n) && (i = a.width, s = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = r, r = u.width, a.width = i, a.minWidth = s, a.maxWidth = o)), r;
  } : i.documentElement.currentStyle && (Dt = function Dt(e, t) {
    var n,
        r,
        i = e.currentStyle && e.currentStyle[t],
        s = e.style;
    return i == null && s && s[t] && (i = s[t]), Ut.test(i) && !Ft.test(t) && (n = s.left, r = e.runtimeStyle && e.runtimeStyle.left, r && (e.runtimeStyle.left = e.currentStyle.left), s.left = t === "fontSize" ? "1em" : i, i = s.pixelLeft + "px", s.left = n, r && (e.runtimeStyle.left = r)), i === "" ? "auto" : i;
  }), v.each(["height", "width"], function (e, t) {
    v.cssHooks[t] = {
      get: function get(e, n, r) {
        if (n) return e.offsetWidth === 0 && It.test(Dt(e, "display")) ? v.swap(e, Xt, function () {
          return tn(e, t, r);
        }) : tn(e, t, r);
      },
      set: function set(e, n, r) {
        return Zt(e, n, r ? en(e, t, r, v.support.boxSizing && v.css(e, "boxSizing") === "border-box") : 0);
      }
    };
  }), v.support.opacity || (v.cssHooks.opacity = {
    get: function get(e, t) {
      return jt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : "";
    },
    set: function set(e, t) {
      var n = e.style,
          r = e.currentStyle,
          i = v.isNumeric(t) ? "alpha(opacity=" + t * 100 + ")" : "",
          s = r && r.filter || n.filter || "";
      n.zoom = 1;

      if (t >= 1 && v.trim(s.replace(Bt, "")) === "" && n.removeAttribute) {
        n.removeAttribute("filter");
        if (r && !r.filter) return;
      }

      n.filter = Bt.test(s) ? s.replace(Bt, i) : s + " " + i;
    }
  }), v(function () {
    v.support.reliableMarginRight || (v.cssHooks.marginRight = {
      get: function get(e, t) {
        return v.swap(e, {
          display: "inline-block"
        }, function () {
          if (t) return Dt(e, "marginRight");
        });
      }
    }), !v.support.pixelPosition && v.fn.position && v.each(["top", "left"], function (e, t) {
      v.cssHooks[t] = {
        get: function get(e, n) {
          if (n) {
            var r = Dt(e, t);
            return Ut.test(r) ? v(e).position()[t] + "px" : r;
          }
        }
      };
    });
  }), v.expr && v.expr.filters && (v.expr.filters.hidden = function (e) {
    return e.offsetWidth === 0 && e.offsetHeight === 0 || !v.support.reliableHiddenOffsets && (e.style && e.style.display || Dt(e, "display")) === "none";
  }, v.expr.filters.visible = function (e) {
    return !v.expr.filters.hidden(e);
  }), v.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (e, t) {
    v.cssHooks[e + t] = {
      expand: function expand(n) {
        var r,
            i = typeof n == "string" ? n.split(" ") : [n],
            s = {};

        for (r = 0; r < 4; r++) {
          s[e + $t[r] + t] = i[r] || i[r - 2] || i[0];
        }

        return s;
      }
    }, qt.test(e) || (v.cssHooks[e + t].set = Zt);
  });
  var rn = /%20/g,
      sn = /\[\]$/,
      on = /\r?\n/g,
      un = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
      an = /^(?:select|textarea)/i;
  v.fn.extend({
    serialize: function serialize() {
      return v.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        return this.elements ? v.makeArray(this.elements) : this;
      }).filter(function () {
        return this.name && !this.disabled && (this.checked || an.test(this.nodeName) || un.test(this.type));
      }).map(function (e, t) {
        var n = v(this).val();
        return n == null ? null : v.isArray(n) ? v.map(n, function (e, n) {
          return {
            name: t.name,
            value: e.replace(on, "\r\n")
          };
        }) : {
          name: t.name,
          value: n.replace(on, "\r\n")
        };
      }).get();
    }
  }), v.param = function (e, n) {
    var r,
        i = [],
        s = function s(e, t) {
      t = v.isFunction(t) ? t() : t == null ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t);
    };

    n === t && (n = v.ajaxSettings && v.ajaxSettings.traditional);
    if (v.isArray(e) || e.jquery && !v.isPlainObject(e)) v.each(e, function () {
      s(this.name, this.value);
    });else for (r in e) {
      fn(r, e[r], n, s);
    }
    return i.join("&").replace(rn, "+");
  };
  var ln,
      cn,
      hn = /#.*$/,
      pn = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
      dn = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
      vn = /^(?:GET|HEAD)$/,
      mn = /^\/\//,
      gn = /\?/,
      yn = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
      bn = /([?&])_=[^&]*/,
      wn = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
      En = v.fn.load,
      Sn = {},
      xn = {},
      Tn = ["*/"] + ["*"];

  try {
    cn = s.href;
  } catch (Nn) {
    cn = i.createElement("a"), cn.href = "", cn = cn.href;
  }

  ln = wn.exec(cn.toLowerCase()) || [], v.fn.load = function (e, n, r) {
    if (typeof e != "string" && En) return En.apply(this, arguments);
    if (!this.length) return this;
    var i,
        s,
        o,
        u = this,
        a = e.indexOf(" ");
    return a >= 0 && (i = e.slice(a, e.length), e = e.slice(0, a)), v.isFunction(n) ? (r = n, n = t) : n && _typeof(n) == "object" && (s = "POST"), v.ajax({
      url: e,
      type: s,
      dataType: "html",
      data: n,
      complete: function complete(e, t) {
        r && u.each(r, o || [e.responseText, t, e]);
      }
    }).done(function (e) {
      o = arguments, u.html(i ? v("<div>").append(e.replace(yn, "")).find(i) : e);
    }), this;
  }, v.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function (e, t) {
    v.fn[t] = function (e) {
      return this.on(t, e);
    };
  }), v.each(["get", "post"], function (e, n) {
    v[n] = function (e, r, i, s) {
      return v.isFunction(r) && (s = s || i, i = r, r = t), v.ajax({
        type: n,
        url: e,
        data: r,
        success: i,
        dataType: s
      });
    };
  }), v.extend({
    getScript: function getScript(e, n) {
      return v.get(e, t, n, "script");
    },
    getJSON: function getJSON(e, t, n) {
      return v.get(e, t, n, "json");
    },
    ajaxSetup: function ajaxSetup(e, t) {
      return t ? Ln(e, v.ajaxSettings) : (t = e, e = v.ajaxSettings), Ln(e, t), e;
    },
    ajaxSettings: {
      url: cn,
      isLocal: dn.test(ln[1]),
      global: !0,
      type: "GET",
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      processData: !0,
      async: !0,
      accepts: {
        xml: "application/xml, text/xml",
        html: "text/html",
        text: "text/plain",
        json: "application/json, text/javascript",
        "*": Tn
      },
      contents: {
        xml: /xml/,
        html: /html/,
        json: /json/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText"
      },
      converters: {
        "* text": e.String,
        "text html": !0,
        "text json": v.parseJSON,
        "text xml": v.parseXML
      },
      flatOptions: {
        context: !0,
        url: !0
      }
    },
    ajaxPrefilter: Cn(Sn),
    ajaxTransport: Cn(xn),
    ajax: function ajax(e, n) {
      function T(e, n, s, a) {
        var l,
            y,
            b,
            w,
            S,
            T = n;
        if (E === 2) return;
        E = 2, u && clearTimeout(u), o = t, i = a || "", x.readyState = e > 0 ? 4 : 0, s && (w = An(c, x, s));
        if (e >= 200 && e < 300 || e === 304) c.ifModified && (S = x.getResponseHeader("Last-Modified"), S && (v.lastModified[r] = S), S = x.getResponseHeader("Etag"), S && (v.etag[r] = S)), e === 304 ? (T = "notmodified", l = !0) : (l = On(c, w), T = l.state, y = l.data, b = l.error, l = !b);else {
          b = T;
          if (!T || e) T = "error", e < 0 && (e = 0);
        }
        x.status = e, x.statusText = (n || T) + "", l ? d.resolveWith(h, [y, T, x]) : d.rejectWith(h, [x, T, b]), x.statusCode(g), g = t, f && p.trigger("ajax" + (l ? "Success" : "Error"), [x, c, l ? y : b]), m.fireWith(h, [x, T]), f && (p.trigger("ajaxComplete", [x, c]), --v.active || v.event.trigger("ajaxStop"));
      }

      _typeof(e) == "object" && (n = e, e = t), n = n || {};
      var r,
          i,
          s,
          o,
          u,
          a,
          f,
          l,
          c = v.ajaxSetup({}, n),
          h = c.context || c,
          p = h !== c && (h.nodeType || h instanceof v) ? v(h) : v.event,
          d = v.Deferred(),
          m = v.Callbacks("once memory"),
          g = c.statusCode || {},
          b = {},
          w = {},
          E = 0,
          S = "canceled",
          x = {
        readyState: 0,
        setRequestHeader: function setRequestHeader(e, t) {
          if (!E) {
            var n = e.toLowerCase();
            e = w[n] = w[n] || e, b[e] = t;
          }

          return this;
        },
        getAllResponseHeaders: function getAllResponseHeaders() {
          return E === 2 ? i : null;
        },
        getResponseHeader: function getResponseHeader(e) {
          var n;

          if (E === 2) {
            if (!s) {
              s = {};

              while (n = pn.exec(i)) {
                s[n[1].toLowerCase()] = n[2];
              }
            }

            n = s[e.toLowerCase()];
          }

          return n === t ? null : n;
        },
        overrideMimeType: function overrideMimeType(e) {
          return E || (c.mimeType = e), this;
        },
        abort: function abort(e) {
          return e = e || S, o && o.abort(e), T(0, e), this;
        }
      };
      d.promise(x), x.success = x.done, x.error = x.fail, x.complete = m.add, x.statusCode = function (e) {
        if (e) {
          var t;
          if (E < 2) for (t in e) {
            g[t] = [g[t], e[t]];
          } else t = e[x.status], x.always(t);
        }

        return this;
      }, c.url = ((e || c.url) + "").replace(hn, "").replace(mn, ln[1] + "//"), c.dataTypes = v.trim(c.dataType || "*").toLowerCase().split(y), c.crossDomain == null && (a = wn.exec(c.url.toLowerCase()), c.crossDomain = !(!a || a[1] === ln[1] && a[2] === ln[2] && (a[3] || (a[1] === "http:" ? 80 : 443)) == (ln[3] || (ln[1] === "http:" ? 80 : 443)))), c.data && c.processData && typeof c.data != "string" && (c.data = v.param(c.data, c.traditional)), kn(Sn, c, n, x);
      if (E === 2) return x;
      f = c.global, c.type = c.type.toUpperCase(), c.hasContent = !vn.test(c.type), f && v.active++ === 0 && v.event.trigger("ajaxStart");

      if (!c.hasContent) {
        c.data && (c.url += (gn.test(c.url) ? "&" : "?") + c.data, delete c.data), r = c.url;

        if (c.cache === !1) {
          var N = v.now(),
              C = c.url.replace(bn, "$1_=" + N);
          c.url = C + (C === c.url ? (gn.test(c.url) ? "&" : "?") + "_=" + N : "");
        }
      }

      (c.data && c.hasContent && c.contentType !== !1 || n.contentType) && x.setRequestHeader("Content-Type", c.contentType), c.ifModified && (r = r || c.url, v.lastModified[r] && x.setRequestHeader("If-Modified-Since", v.lastModified[r]), v.etag[r] && x.setRequestHeader("If-None-Match", v.etag[r])), x.setRequestHeader("Accept", c.dataTypes[0] && c.accepts[c.dataTypes[0]] ? c.accepts[c.dataTypes[0]] + (c.dataTypes[0] !== "*" ? ", " + Tn + "; q=0.01" : "") : c.accepts["*"]);

      for (l in c.headers) {
        x.setRequestHeader(l, c.headers[l]);
      }

      if (!c.beforeSend || c.beforeSend.call(h, x, c) !== !1 && E !== 2) {
        S = "abort";

        for (l in {
          success: 1,
          error: 1,
          complete: 1
        }) {
          x[l](c[l]);
        }

        o = kn(xn, c, n, x);
        if (!o) T(-1, "No Transport");else {
          x.readyState = 1, f && p.trigger("ajaxSend", [x, c]), c.async && c.timeout > 0 && (u = setTimeout(function () {
            x.abort("timeout");
          }, c.timeout));

          try {
            E = 1, o.send(b, T);
          } catch (k) {
            if (!(E < 2)) throw k;
            T(-1, k);
          }
        }
        return x;
      }

      return x.abort();
    },
    active: 0,
    lastModified: {},
    etag: {}
  });
  var Mn = [],
      _n = /\?/,
      Dn = /(=)\?(?=&|$)|\?\?/,
      Pn = v.now();
  v.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var e = Mn.pop() || v.expando + "_" + Pn++;
      return this[e] = !0, e;
    }
  }), v.ajaxPrefilter("json jsonp", function (n, r, i) {
    var s,
        o,
        u,
        a = n.data,
        f = n.url,
        l = n.jsonp !== !1,
        c = l && Dn.test(f),
        h = l && !c && typeof a == "string" && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Dn.test(a);
    if (n.dataTypes[0] === "jsonp" || c || h) return s = n.jsonpCallback = v.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, o = e[s], c ? n.url = f.replace(Dn, "$1" + s) : h ? n.data = a.replace(Dn, "$1" + s) : l && (n.url += (_n.test(f) ? "&" : "?") + n.jsonp + "=" + s), n.converters["script json"] = function () {
      return u || v.error(s + " was not called"), u[0];
    }, n.dataTypes[0] = "json", e[s] = function () {
      u = arguments;
    }, i.always(function () {
      e[s] = o, n[s] && (n.jsonpCallback = r.jsonpCallback, Mn.push(s)), u && v.isFunction(o) && o(u[0]), u = o = t;
    }), "script";
  }), v.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /javascript|ecmascript/
    },
    converters: {
      "text script": function textScript(e) {
        return v.globalEval(e), e;
      }
    }
  }), v.ajaxPrefilter("script", function (e) {
    e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1);
  }), v.ajaxTransport("script", function (e) {
    if (e.crossDomain) {
      var n,
          r = i.head || i.getElementsByTagName("head")[0] || i.documentElement;
      return {
        send: function send(s, o) {
          n = i.createElement("script"), n.async = "async", e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function (e, i) {
            if (i || !n.readyState || /loaded|complete/.test(n.readyState)) n.onload = n.onreadystatechange = null, r && n.parentNode && r.removeChild(n), n = t, i || o(200, "success");
          }, r.insertBefore(n, r.firstChild);
        },
        abort: function abort() {
          n && n.onload(0, 1);
        }
      };
    }
  });
  var Hn,
      Bn = e.ActiveXObject ? function () {
    for (var e in Hn) {
      Hn[e](0, 1);
    }
  } : !1,
      jn = 0;
  v.ajaxSettings.xhr = e.ActiveXObject ? function () {
    return !this.isLocal && Fn() || In();
  } : Fn, function (e) {
    v.extend(v.support, {
      ajax: !!e,
      cors: !!e && "withCredentials" in e
    });
  }(v.ajaxSettings.xhr()), v.support.ajax && v.ajaxTransport(function (n) {
    if (!n.crossDomain || v.support.cors) {
      var _r;

      return {
        send: function send(i, s) {
          var o,
              u,
              a = n.xhr();
          n.username ? a.open(n.type, n.url, n.async, n.username, n.password) : a.open(n.type, n.url, n.async);
          if (n.xhrFields) for (u in n.xhrFields) {
            a[u] = n.xhrFields[u];
          }
          n.mimeType && a.overrideMimeType && a.overrideMimeType(n.mimeType), !n.crossDomain && !i["X-Requested-With"] && (i["X-Requested-With"] = "XMLHttpRequest");

          try {
            for (u in i) {
              a.setRequestHeader(u, i[u]);
            }
          } catch (f) {}

          a.send(n.hasContent && n.data || null), _r = function r(e, i) {
            var u, f, l, c, h;

            try {
              if (_r && (i || a.readyState === 4)) {
                _r = t, o && (a.onreadystatechange = v.noop, Bn && delete Hn[o]);
                if (i) a.readyState !== 4 && a.abort();else {
                  u = a.status, l = a.getAllResponseHeaders(), c = {}, h = a.responseXML, h && h.documentElement && (c.xml = h);

                  try {
                    c.text = a.responseText;
                  } catch (p) {}

                  try {
                    f = a.statusText;
                  } catch (p) {
                    f = "";
                  }

                  !u && n.isLocal && !n.crossDomain ? u = c.text ? 200 : 404 : u === 1223 && (u = 204);
                }
              }
            } catch (d) {
              i || s(-1, d);
            }

            c && s(u, f, c, l);
          }, n.async ? a.readyState === 4 ? setTimeout(_r, 0) : (o = ++jn, Bn && (Hn || (Hn = {}, v(e).unload(Bn)), Hn[o] = _r), a.onreadystatechange = _r) : _r();
        },
        abort: function abort() {
          _r && _r(0, 1);
        }
      };
    }
  });
  var qn,
      Rn,
      Un = /^(?:toggle|show|hide)$/,
      zn = new RegExp("^(?:([-+])=|)(" + m + ")([a-z%]*)$", "i"),
      Wn = /queueHooks$/,
      Xn = [Gn],
      Vn = {
    "*": [function (e, t) {
      var n,
          r,
          i = this.createTween(e, t),
          s = zn.exec(t),
          o = i.cur(),
          u = +o || 0,
          a = 1,
          f = 20;

      if (s) {
        n = +s[2], r = s[3] || (v.cssNumber[e] ? "" : "px");

        if (r !== "px" && u) {
          u = v.css(i.elem, e, !0) || n || 1;

          do {
            a = a || ".5", u /= a, v.style(i.elem, e, u + r);
          } while (a !== (a = i.cur() / o) && a !== 1 && --f);
        }

        i.unit = r, i.start = u, i.end = s[1] ? u + (s[1] + 1) * n : n;
      }

      return i;
    }]
  };
  v.Animation = v.extend(Kn, {
    tweener: function tweener(e, t) {
      v.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
      var n,
          r = 0,
          i = e.length;

      for (; r < i; r++) {
        n = e[r], Vn[n] = Vn[n] || [], Vn[n].unshift(t);
      }
    },
    prefilter: function prefilter(e, t) {
      t ? Xn.unshift(e) : Xn.push(e);
    }
  }), v.Tween = Yn, Yn.prototype = {
    constructor: Yn,
    init: function init(e, t, n, r, i, s) {
      this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = s || (v.cssNumber[n] ? "" : "px");
    },
    cur: function cur() {
      var e = Yn.propHooks[this.prop];
      return e && e.get ? e.get(this) : Yn.propHooks._default.get(this);
    },
    run: function run(e) {
      var t,
          n = Yn.propHooks[this.prop];
      return this.options.duration ? this.pos = t = v.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : Yn.propHooks._default.set(this), this;
    }
  }, Yn.prototype.init.prototype = Yn.prototype, Yn.propHooks = {
    _default: {
      get: function get(e) {
        var t;
        return e.elem[e.prop] == null || !!e.elem.style && e.elem.style[e.prop] != null ? (t = v.css(e.elem, e.prop, !1, ""), !t || t === "auto" ? 0 : t) : e.elem[e.prop];
      },
      set: function set(e) {
        v.fx.step[e.prop] ? v.fx.step[e.prop](e) : e.elem.style && (e.elem.style[v.cssProps[e.prop]] != null || v.cssHooks[e.prop]) ? v.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now;
      }
    }
  }, Yn.propHooks.scrollTop = Yn.propHooks.scrollLeft = {
    set: function set(e) {
      e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
    }
  }, v.each(["toggle", "show", "hide"], function (e, t) {
    var n = v.fn[t];

    v.fn[t] = function (r, i, s) {
      return r == null || typeof r == "boolean" || !e && v.isFunction(r) && v.isFunction(i) ? n.apply(this, arguments) : this.animate(Zn(t, !0), r, i, s);
    };
  }), v.fn.extend({
    fadeTo: function fadeTo(e, t, n, r) {
      return this.filter(Gt).css("opacity", 0).show().end().animate({
        opacity: t
      }, e, n, r);
    },
    animate: function animate(e, t, n, r) {
      var i = v.isEmptyObject(e),
          s = v.speed(t, n, r),
          o = function o() {
        var t = Kn(this, v.extend({}, e), s);
        i && t.stop(!0);
      };

      return i || s.queue === !1 ? this.each(o) : this.queue(s.queue, o);
    },
    stop: function stop(e, n, r) {
      var i = function i(e) {
        var t = e.stop;
        delete e.stop, t(r);
      };

      return typeof e != "string" && (r = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function () {
        var t = !0,
            n = e != null && e + "queueHooks",
            s = v.timers,
            o = v._data(this);

        if (n) o[n] && o[n].stop && i(o[n]);else for (n in o) {
          o[n] && o[n].stop && Wn.test(n) && i(o[n]);
        }

        for (n = s.length; n--;) {
          s[n].elem === this && (e == null || s[n].queue === e) && (s[n].anim.stop(r), t = !1, s.splice(n, 1));
        }

        (t || !r) && v.dequeue(this, e);
      });
    }
  }), v.each({
    slideDown: Zn("show"),
    slideUp: Zn("hide"),
    slideToggle: Zn("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (e, t) {
    v.fn[e] = function (e, n, r) {
      return this.animate(t, e, n, r);
    };
  }), v.speed = function (e, t, n) {
    var r = e && _typeof(e) == "object" ? v.extend({}, e) : {
      complete: n || !n && t || v.isFunction(e) && e,
      duration: e,
      easing: n && t || t && !v.isFunction(t) && t
    };
    r.duration = v.fx.off ? 0 : typeof r.duration == "number" ? r.duration : r.duration in v.fx.speeds ? v.fx.speeds[r.duration] : v.fx.speeds._default;
    if (r.queue == null || r.queue === !0) r.queue = "fx";
    return r.old = r.complete, r.complete = function () {
      v.isFunction(r.old) && r.old.call(this), r.queue && v.dequeue(this, r.queue);
    }, r;
  }, v.easing = {
    linear: function linear(e) {
      return e;
    },
    swing: function swing(e) {
      return .5 - Math.cos(e * Math.PI) / 2;
    }
  }, v.timers = [], v.fx = Yn.prototype.init, v.fx.tick = function () {
    var e,
        n = v.timers,
        r = 0;
    qn = v.now();

    for (; r < n.length; r++) {
      e = n[r], !e() && n[r] === e && n.splice(r--, 1);
    }

    n.length || v.fx.stop(), qn = t;
  }, v.fx.timer = function (e) {
    e() && v.timers.push(e) && !Rn && (Rn = setInterval(v.fx.tick, v.fx.interval));
  }, v.fx.interval = 13, v.fx.stop = function () {
    clearInterval(Rn), Rn = null;
  }, v.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  }, v.fx.step = {}, v.expr && v.expr.filters && (v.expr.filters.animated = function (e) {
    return v.grep(v.timers, function (t) {
      return e === t.elem;
    }).length;
  });
  var er = /^(?:body|html)$/i;
  v.fn.offset = function (e) {
    if (arguments.length) return e === t ? this : this.each(function (t) {
      v.offset.setOffset(this, e, t);
    });
    var n,
        r,
        i,
        s,
        o,
        u,
        a,
        f = {
      top: 0,
      left: 0
    },
        l = this[0],
        c = l && l.ownerDocument;
    if (!c) return;
    return (r = c.body) === l ? v.offset.bodyOffset(l) : (n = c.documentElement, v.contains(n, l) ? (typeof l.getBoundingClientRect != "undefined" && (f = l.getBoundingClientRect()), i = tr(c), s = n.clientTop || r.clientTop || 0, o = n.clientLeft || r.clientLeft || 0, u = i.pageYOffset || n.scrollTop, a = i.pageXOffset || n.scrollLeft, {
      top: f.top + u - s,
      left: f.left + a - o
    }) : f);
  }, v.offset = {
    bodyOffset: function bodyOffset(e) {
      var t = e.offsetTop,
          n = e.offsetLeft;
      return v.support.doesNotIncludeMarginInBodyOffset && (t += parseFloat(v.css(e, "marginTop")) || 0, n += parseFloat(v.css(e, "marginLeft")) || 0), {
        top: t,
        left: n
      };
    },
    setOffset: function setOffset(e, t, n) {
      var r = v.css(e, "position");
      r === "static" && (e.style.position = "relative");
      var i = v(e),
          s = i.offset(),
          o = v.css(e, "top"),
          u = v.css(e, "left"),
          a = (r === "absolute" || r === "fixed") && v.inArray("auto", [o, u]) > -1,
          f = {},
          l = {},
          c,
          h;
      a ? (l = i.position(), c = l.top, h = l.left) : (c = parseFloat(o) || 0, h = parseFloat(u) || 0), v.isFunction(t) && (t = t.call(e, n, s)), t.top != null && (f.top = t.top - s.top + c), t.left != null && (f.left = t.left - s.left + h), "using" in t ? t.using.call(e, f) : i.css(f);
    }
  }, v.fn.extend({
    position: function position() {
      if (!this[0]) return;
      var e = this[0],
          t = this.offsetParent(),
          n = this.offset(),
          r = er.test(t[0].nodeName) ? {
        top: 0,
        left: 0
      } : t.offset();
      return n.top -= parseFloat(v.css(e, "marginTop")) || 0, n.left -= parseFloat(v.css(e, "marginLeft")) || 0, r.top += parseFloat(v.css(t[0], "borderTopWidth")) || 0, r.left += parseFloat(v.css(t[0], "borderLeftWidth")) || 0, {
        top: n.top - r.top,
        left: n.left - r.left
      };
    },
    offsetParent: function offsetParent() {
      return this.map(function () {
        var e = this.offsetParent || i.body;

        while (e && !er.test(e.nodeName) && v.css(e, "position") === "static") {
          e = e.offsetParent;
        }

        return e || i.body;
      });
    }
  }), v.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (e, n) {
    var r = /Y/.test(n);

    v.fn[e] = function (i) {
      return v.access(this, function (e, i, s) {
        var o = tr(e);
        if (s === t) return o ? n in o ? o[n] : o.document.documentElement[i] : e[i];
        o ? o.scrollTo(r ? v(o).scrollLeft() : s, r ? s : v(o).scrollTop()) : e[i] = s;
      }, e, i, arguments.length, null);
    };
  }), v.each({
    Height: "height",
    Width: "width"
  }, function (e, n) {
    v.each({
      padding: "inner" + e,
      content: n,
      "": "outer" + e
    }, function (r, i) {
      v.fn[i] = function (i, s) {
        var o = arguments.length && (r || typeof i != "boolean"),
            u = r || (i === !0 || s === !0 ? "margin" : "border");
        return v.access(this, function (n, r, i) {
          var s;
          return v.isWindow(n) ? n.document.documentElement["client" + e] : n.nodeType === 9 ? (s = n.documentElement, Math.max(n.body["scroll" + e], s["scroll" + e], n.body["offset" + e], s["offset" + e], s["client" + e])) : i === t ? v.css(n, r, i, u) : v.style(n, r, i, u);
        }, n, o ? i : t, o, null);
      };
    });
  }), e.jQuery = e.$ = v,  true && __webpack_require__(/*! !webpack amd options */ "./node_modules/webpack/buildin/amd-options.js").jQuery && !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
    return v;
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
})(window);

/***/ }),

/***/ "./public/game-6/js/pacman.js":
/*!************************************!*\
  !*** ./public/game-6/js/pacman.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var PACMAN_DIRECTION = 3;
var PACMAN_DIRECTION_TRY = -1;
var PACMAN_DIRECTION_TRY_TIMER = null;
var PACMAN_DIRECTION_TRY_CANCEL = 1000;
var PACMAN_POSITION_X = 276;
var PACMAN_POSITION_Y = 416;
var PACMAN_POSITION_STEP = 2;
var PACMAN_MOUNTH_STATE = 3;
var PACMAN_MOUNTH_STATE_MAX = 6;
var PACMAN_SIZE = 16;
var PACMAN_MOVING = false;
var PACMAN_MOVING_TIMER = -1;
var PACMAN_MOVING_SPEED = 15;
var PACMAN_CANVAS_CONTEXT = null;
var PACMAN_EAT_GAP = 15;
var PACMAN_GHOST_GAP = 20;
var PACMAN_FRUITS_GAP = 15;
var PACMAN_KILLING_TIMER = -1;
var PACMAN_KILLING_SPEED = 70;
var PACMAN_RETRY_SPEED = 2100;
var PACMAN_DEAD = false;

function initPacman() {
  var canvas = document.getElementById('canvas-pacman');
  canvas.setAttribute('width', '550');
  canvas.setAttribute('height', '550');

  if (canvas.getContext) {
    PACMAN_CANVAS_CONTEXT = canvas.getContext('2d');
  }
}

function resetPacman() {
  stopPacman();
  PACMAN_DIRECTION = 3;
  PACMAN_DIRECTION_TRY = -1;
  PACMAN_DIRECTION_TRY_TIMER = null;
  PACMAN_POSITION_X = 276;
  PACMAN_POSITION_Y = 416;
  PACMAN_MOUNTH_STATE = 3;
  PACMAN_MOVING = false;
  PACMAN_MOVING_TIMER = -1;
  PACMAN_KILLING_TIMER = -1;
  PACMAN_DEAD = false;
  PACMAN_SUPER = false;
}

function getPacmanCanevasContext() {
  return PACMAN_CANVAS_CONTEXT;
}

function stopPacman() {
  if (PACMAN_MOVING_TIMER != -1) {
    clearInterval(PACMAN_MOVING_TIMER);
    PACMAN_MOVING_TIMER = -1;
    PACMAN_MOVING = false;
  }

  if (PACMAN_KILLING_TIMER != -1) {
    clearInterval(PACMAN_KILLING_TIMER);
    PACMAN_KILLING_TIMER = -1;
  }
}

function pausePacman() {
  if (PACMAN_DIRECTION_TRY_TIMER != null) {
    PACMAN_DIRECTION_TRY_TIMER.pause();
  }

  if (PACMAN_MOVING_TIMER != -1) {
    clearInterval(PACMAN_MOVING_TIMER);
    PACMAN_MOVING_TIMER = -1;
    PACMAN_MOVING = false;
  }
}

function resumePacman() {
  if (PACMAN_DIRECTION_TRY_TIMER != null) {
    PACMAN_DIRECTION_TRY_TIMER.resume();
  }

  movePacman();
}

function tryMovePacmanCancel() {
  if (PACMAN_DIRECTION_TRY_TIMER != null) {
    PACMAN_DIRECTION_TRY_TIMER.cancel();
    PACMAN_DIRECTION_TRY = -1;
    PACMAN_DIRECTION_TRY_TIMER = null;
  }
}

function tryMovePacman(direction) {
  PACMAN_DIRECTION_TRY = direction;
  PACMAN_DIRECTION_TRY_TIMER = new Timer('tryMovePacmanCancel()', PACMAN_DIRECTION_TRY_CANCEL);
}

function movePacman(direction) {
  if (PACMAN_MOVING === false) {
    PACMAN_MOVING = true;
    drawPacman();
    PACMAN_MOVING_TIMER = setInterval('movePacman()', PACMAN_MOVING_SPEED);
  }

  var directionTry = direction;
  var quarterChangeDirection = false;

  if (!directionTry && PACMAN_DIRECTION_TRY != -1) {
    directionTry = PACMAN_DIRECTION_TRY;
  }

  if (!directionTry || PACMAN_DIRECTION !== directionTry) {
    if (directionTry) {
      if (canMovePacman(directionTry)) {
        if (PACMAN_DIRECTION + 1 === directionTry || PACMAN_DIRECTION - 1 === directionTry || PACMAN_DIRECTION + 1 === directionTry || PACMAN_DIRECTION === 4 && directionTry === 1 || PACMAN_DIRECTION === 1 && directionTry === 4) {
          quarterChangeDirection = true;
        }

        PACMAN_DIRECTION = directionTry;
        tryMovePacmanCancel();
      } else {
        if (directionTry !== PACMAN_DIRECTION_TRY) {
          tryMovePacmanCancel();
        }

        if (PACMAN_DIRECTION_TRY === -1) {
          tryMovePacman(directionTry);
        }
      }
    }

    if (canMovePacman(PACMAN_DIRECTION)) {
      erasePacman();

      if (PACMAN_MOUNTH_STATE < PACMAN_MOUNTH_STATE_MAX) {
        PACMAN_MOUNTH_STATE++;
      } else {
        PACMAN_MOUNTH_STATE = 0;
      }

      var speedUp = 0;

      if (quarterChangeDirection) {
        speedUp = 6;
      }

      if (PACMAN_DIRECTION === 1) {
        PACMAN_POSITION_X += PACMAN_POSITION_STEP + speedUp;
      } else if (PACMAN_DIRECTION === 2) {
        PACMAN_POSITION_Y += PACMAN_POSITION_STEP + speedUp;
      } else if (PACMAN_DIRECTION === 3) {
        PACMAN_POSITION_X -= PACMAN_POSITION_STEP + speedUp;
      } else if (PACMAN_DIRECTION === 4) {
        PACMAN_POSITION_Y -= PACMAN_POSITION_STEP + speedUp;
      }

      if (PACMAN_POSITION_X === 2 && PACMAN_POSITION_Y === 258) {
        PACMAN_POSITION_X = 548;
        PACMAN_POSITION_Y = 258;
      } else if (PACMAN_POSITION_X === 548 && PACMAN_POSITION_Y === 258) {
        PACMAN_POSITION_X = 2;
        PACMAN_POSITION_Y = 258;
      }

      drawPacman();

      if (PACMAN_MOUNTH_STATE === 0 || PACMAN_MOUNTH_STATE === 3) {
        testBubblesPacman();
        testGhostsPacman();
        testFruitsPacman();
      }
    } else {
      stopPacman();
    }
  } else if (direction && PACMAN_DIRECTION === direction) {
    tryMovePacmanCancel();
  }
}

function canMovePacman(direction) {
  var positionX = PACMAN_POSITION_X;
  var positionY = PACMAN_POSITION_Y;
  if (positionX === 276 && positionY === 204 && direction === 2) return false;

  if (direction === 1) {
    positionX += PACMAN_POSITION_STEP;
  } else if (direction === 2) {
    positionY += PACMAN_POSITION_STEP;
  } else if (direction === 3) {
    positionX -= PACMAN_POSITION_STEP;
  } else if (direction === 4) {
    positionY -= PACMAN_POSITION_STEP;
  }

  for (var i = 0, imax = PATHS.length; i < imax; i++) {
    var p = PATHS[i];
    var c = p.split("-");
    var cx = c[0].split(",");
    var cy = c[1].split(",");
    var startX = cx[0];
    var startY = cx[1];
    var endX = cy[0];
    var endY = cy[1];

    if (positionX >= startX && positionX <= endX && positionY >= startY && positionY <= endY) {
      return true;
    }
  }

  return false;
}

function drawPacman() {
  var ctx = getPacmanCanevasContext();
  ctx.fillStyle = "#fff200";
  ctx.beginPath();
  var startAngle = 0;
  var endAngle = 2 * Math.PI;
  var lineToX = PACMAN_POSITION_X;
  var lineToY = PACMAN_POSITION_Y;

  if (PACMAN_DIRECTION === 1) {
    startAngle = (0.35 - PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (1.65 + PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    lineToX -= 8;
  } else if (PACMAN_DIRECTION === 2) {
    startAngle = (0.85 - PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (0.15 + PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    lineToY -= 8;
  } else if (PACMAN_DIRECTION === 3) {
    startAngle = (1.35 - PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (0.65 + PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    lineToX += 8;
  } else if (PACMAN_DIRECTION === 4) {
    startAngle = (1.85 - PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    endAngle = (1.15 + PACMAN_MOUNTH_STATE * 0.05) * Math.PI;
    lineToY += 8;
  }

  ctx.arc(PACMAN_POSITION_X, PACMAN_POSITION_Y, PACMAN_SIZE, startAngle, endAngle, false);
  ctx.lineTo(lineToX, lineToY);
  ctx.fill();
  ctx.closePath();
}

function erasePacman() {
  var ctx = getPacmanCanevasContext();
  ctx.clearRect(PACMAN_POSITION_X - 2 - PACMAN_SIZE, PACMAN_POSITION_Y - 2 - PACMAN_SIZE, PACMAN_SIZE * 2 + 5, PACMAN_SIZE * 2 + 5);
}

function killPacman() {
  playDieSound();
  LOCK = true;
  PACMAN_DEAD = true;
  stopPacman();
  stopGhosts();
  pauseTimes();
  stopBlinkSuperBubbles();
  PACMAN_KILLING_TIMER = setInterval('killingPacman()', PACMAN_KILLING_SPEED);
}

function killingPacman() {
  if (PACMAN_MOUNTH_STATE > -12) {
    erasePacman();
    PACMAN_MOUNTH_STATE--;
    drawPacman();
  } else {
    clearInterval(PACMAN_KILLING_TIMER);
    PACMAN_KILLING_TIMER = -1;
    erasePacman();

    if (LIFES > 0) {
      lifes(-1);
      setTimeout('retry()', PACMAN_RETRY_SPEED);
    } else {
      gameover();
    }
  }
}

function testGhostsPacman() {
  testGhostPacman('blinky');
  testGhostPacman('pinky');
  testGhostPacman('inky');
  testGhostPacman('clyde');
}

function testGhostPacman(ghost) {
  eval('var positionX = GHOST_' + ghost.toUpperCase() + '_POSITION_X');
  eval('var positionY = GHOST_' + ghost.toUpperCase() + '_POSITION_Y');

  if (positionX <= PACMAN_POSITION_X + PACMAN_GHOST_GAP && positionX >= PACMAN_POSITION_X - PACMAN_GHOST_GAP && positionY <= PACMAN_POSITION_Y + PACMAN_GHOST_GAP && positionY >= PACMAN_POSITION_Y - PACMAN_GHOST_GAP) {
    eval('var state = GHOST_' + ghost.toUpperCase() + '_STATE');

    if (state === 0) {
      killPacman();
    } else if (state === 1) {
      startEatGhost(ghost);
    }
  }
}

function testFruitsPacman() {
  if (FRUIT_CANCEL_TIMER != null) {
    if (FRUITS_POSITION_X <= PACMAN_POSITION_X + PACMAN_FRUITS_GAP && FRUITS_POSITION_X >= PACMAN_POSITION_X - PACMAN_FRUITS_GAP && FRUITS_POSITION_Y <= PACMAN_POSITION_Y + PACMAN_FRUITS_GAP && FRUITS_POSITION_Y >= PACMAN_POSITION_Y - PACMAN_FRUITS_GAP) {
      eatFruit();
    }
  }
}

function testBubblesPacman() {
  var r = {
    x: PACMAN_POSITION_X - PACMAN_SIZE / 2,
    y: PACMAN_POSITION_Y - PACMAN_SIZE / 2,
    width: PACMAN_SIZE * 2,
    height: PACMAN_SIZE * 2
  };

  for (var i = 0, imax = BUBBLES_ARRAY.length; i < imax; i++) {
    var bubble = BUBBLES_ARRAY[i];
    var bubbleParams = bubble.split(";");
    var testX = parseInt(bubbleParams[0].split(",")[0]);
    var testY = parseInt(bubbleParams[0].split(",")[1]);
    var p = {
      x: testX,
      y: testY
    };

    if (isPointInRect(p, r)) {
      if (bubbleParams[4] === "0") {
        var type = bubbleParams[3];
        eraseBubble(type, testX, testY);
        BUBBLES_ARRAY[i] = bubble.substr(0, bubble.length - 1) + "1";

        if (type === "s") {
          setSuperBubbleOnXY(testX, testY, "1");
          score(SCORE_SUPER_BUBBLE);
          playEatPillSound();
          affraidGhosts();
        } else {
          score(SCORE_BUBBLE);
          playEatingSound();
        }

        BUBBLES_COUNTER--;

        if (BUBBLES_COUNTER === 0) {
          win();
        }
      } else {
        stopEatingSound();
      }

      return;
    }
  }
}

/***/ }),

/***/ "./public/game-6/js/paths.js":
/*!***********************************!*\
  !*** ./public/game-6/js/paths.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var PATHS = new Array();
var PATHS_CANVAS_CONTEXT = null;

function initPaths() {
  var canvas = document.getElementById('canvas-paths');
  canvas.setAttribute('width', '550');
  canvas.setAttribute('height', '550');

  if (canvas.getContext) {
    PATHS_CANVAS_CONTEXT = canvas.getContext('2d');
  } // CENTER


  PATHS.push("128,416-422,416");
  PATHS.push("30,98-518,98");
  PATHS.push("2,258-186,258");
  PATHS.push("362,258-548,258");
  PATHS.push("186,204-362,204");
  PATHS.push("186,310-362,310");
  PATHS.push("30,522-518,522");
  PATHS.push("238,258-314,258");
  PATHS.push("276,204-276,258"); // LEFT

  PATHS.push("128,26-128,470");
  PATHS.push("30,26-244,26");
  PATHS.push("30,26-30,150");
  PATHS.push("30,150-128,150");
  PATHS.push("244,26-244,98");
  PATHS.push("186,204-186,364");
  PATHS.push("30,364-244,364");
  PATHS.push("244,364-244,416");
  PATHS.push("30,364-30,416");
  PATHS.push("30,416-70,416");
  PATHS.push("70,416-70,470");
  PATHS.push("30,470-128,470");
  PATHS.push("30,470-30,522");
  PATHS.push("244,150-244,204");
  PATHS.push("186,150-244,150");
  PATHS.push("186,98-186,150");
  PATHS.push("244,470-244,522");
  PATHS.push("186,470-244,470");
  PATHS.push("186,416-186,470"); // RIGHT

  PATHS.push("422,26-422,470");
  PATHS.push("304,26-518,26");
  PATHS.push("518,26-518,150");
  PATHS.push("304,26-304,98");
  PATHS.push("422,150-518,150");
  PATHS.push("362,204-362,364");
  PATHS.push("304,364-518,364");
  PATHS.push("304,364-304,416");
  PATHS.push("518,364-518,416");
  PATHS.push("480,416-518,416");
  PATHS.push("480,416-480,470");
  PATHS.push("422,470-518,470");
  PATHS.push("518,470-518,522");
  PATHS.push("304,150-304,204");
  PATHS.push("304,150-362,150");
  PATHS.push("362,98-362,150");
  PATHS.push("304,470-304,522");
  PATHS.push("304,470-362,470");
  PATHS.push("362,416-362,470");
}

function getPathsCanevasContext() {
  return PATHS_CANVAS_CONTEXT;
}

function drawPaths() {
  var ctx = getPathsCanevasContext();
  ctx.strokeStyle = "red";

  for (var i = 0, imax = PATHS.length; i < imax; i++) {
    var p = PATHS[i];
    var startX = p.split("-")[0].split(",")[0];
    var startY = p.split("-")[0].split(",")[1];
    var endX = p.split("-")[1].split(",")[0];
    var endY = p.split("-")[1].split(",")[1];
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(endX, endY);
    ctx.stroke();
    ctx.closePath();
  }
}

/***/ }),

/***/ "./public/game-6/js/sound.js":
/*!***********************************!*\
  !*** ./public/game-6/js/sound.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var EATING_SOUND = new buzz.sound(["./game-6/sound/eating.mp3"]);
var GHOST_EATEN_SOUND = new buzz.sound(["./game-6/sound/ghost-eaten.mp3"]);
var EXTRA_LIFE_SOUND = new buzz.sound(["./game-6/sound/extra-life.mp3"]);
var EAT_PILL_SOUND = new buzz.sound(["./game-6/sound/eat-pill.mp3"]);
var EAT_FRUIT_SOUND = new buzz.sound(["./game-6/sound/eat-fruit.mp3"]);
var EAT_GHOST_SOUND = new buzz.sound(["./game-6/sound/eat-ghost.mp3"]);
var SIREN_SOUND = new buzz.sound(["./game-6/sound/siren.mp3"]);
var WAZA_SOUND = new buzz.sound(["./game-6/sound/waza.mp3"]);
var READY_SOUND = new buzz.sound(["./game-6/sound/ready.mp3"]);
var DIE_SOUND = new buzz.sound(["./game-6/sound/die.mp3"]);
var GROUP_SOUND = new buzz.group([EATING_SOUND, SIREN_SOUND, EAT_PILL_SOUND, EAT_GHOST_SOUND, READY_SOUND, DIE_SOUND, WAZA_SOUND, GHOST_EATEN_SOUND, EXTRA_LIFE_SOUND, EAT_FRUIT_SOUND]);
var EATING_SOUND_LOOPING = false;

function isAvailableSound() {
  return !($("#sound").css("display") === "none");
}

function loadAllSound() {
  if (isAvailableSound()) GROUP_SOUND.load();
}

function playEatingSound() {
  if (isAvailableSound()) {
    if (!EATING_SOUND_LOOPING) {
      EATING_SOUND_LOOPING = true;
      EATING_SOUND.setSpeed(1.35);
      EATING_SOUND.loop();
      EATING_SOUND.play();
    }
  }
}

function stopEatingSound() {
  if (isAvailableSound()) {
    if (EATING_SOUND_LOOPING) {
      EATING_SOUND.unloop();
      EATING_SOUND_LOOPING = false;
    }
  }
}

function playExtraLifeSound() {
  if (isAvailableSound()) {
    EXTRA_LIFE_SOUND.play();
  }
}

function playEatFruitSound() {
  if (isAvailableSound()) {
    EAT_FRUIT_SOUND.play();
  }
}

function playEatPillSound() {
  if (isAvailableSound()) {
    EAT_PILL_SOUND.play();
  }
}

function playEatGhostSound() {
  if (isAvailableSound()) {
    EAT_GHOST_SOUND.play();
  }
}

function playWazaSound() {
  if (isAvailableSound()) {
    stopSirenSound();
    stopEatSound();
    WAZA_SOUND.loop();
    WAZA_SOUND.play();
  }
}

function stopWazaSound() {
  if (isAvailableSound()) {
    WAZA_SOUND.stop();
  }
}

function playGhostEatenSound() {
  if (isAvailableSound()) {
    stopSirenSound();
    stopWazaSound();
    GHOST_EATEN_SOUND.play();
    GHOST_EATEN_SOUND.loop();
  }
}

function stopEatSound() {
  if (isAvailableSound()) {
    GHOST_EATEN_SOUND.stop();
  }
}

function playSirenSound() {
  if (isAvailableSound()) {
    stopWazaSound();
    stopEatSound();
    SIREN_SOUND.loop();
    SIREN_SOUND.play();
  }
}

function stopSirenSound() {
  if (isAvailableSound()) {
    SIREN_SOUND.stop();
  }
}

function playReadySound() {
  if (isAvailableSound()) {
    READY_SOUND.play();
  }
}

function playDieSound() {
  if (isAvailableSound()) {
    GROUP_SOUND.stop();
    DIE_SOUND.play();
  }
}

function stopAllSound() {
  if (isAvailableSound()) {
    GROUP_SOUND.stop();
  }
}

/***/ }),

/***/ "./public/game-6/js/tools.js":
/*!***********************************!*\
  !*** ./public/game-6/js/tools.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function isPointInRect(point, rect) {
  if (rect.width <= 0 || rect.height <= 0) {
    return false;
  } else {
    return point.x >= rect.x && point.x <= rect.x + rect.width && point.y >= rect.y && point.y <= rect.y + rect.height;
  }
}

function Timer(callback, delay) {
  var id,
      started,
      remaining = delay,
      running;

  this.start = function () {
    running = true;
    started = new Date();
    id = setTimeout(callback, remaining);
  };

  this.resume = function () {
    running = true;
    started = new Date();
    id = setTimeout(callback, remaining);
  };

  this.cancel = function () {
    running = false;
    clearTimeout(id);
    remaining = 0;
  };

  this.pause = function () {
    running = false;
    clearTimeout(id);
    remaining -= new Date() - started;
  };

  this.remain = function () {
    if (running) {
      this.pause();
      this.start();
    }

    return remaining;
  };

  this.isRunning = function () {
    return running;
  };

  this.start();
}

function oneAxe() {
  return Math.floor(Math.random() * (2 - 1 + 1) + 1);
}

function anyGoodIdea() {
  return Math.floor(Math.random() * (4 - 1 + 1) + 1);
}

function whatsYourProblem() {
  return Math.floor(Math.random() * (6 - 1 + 1) + 1);
}

CanvasRenderingContext2D.prototype.roundRect = function (sx, sy, ex, ey, r) {
  var r2d = Math.PI / 180;

  if (ex - sx - 2 * r < 0) {
    r = (ex - sx) / 2;
  }

  if (ey - sy - 2 * r < 0) {
    r = (ey - sy) / 2;
  }

  this.beginPath();
  this.moveTo(sx + r, sy);
  this.lineTo(ex - r, sy);
  this.arc(ex - r, sy + r, r, r2d * 270, r2d * 360, false);
  this.lineTo(ex, ey - r);
  this.arc(ex - r, ey - r, r, r2d * 0, r2d * 90, false);
  this.lineTo(sx + r, ey);
  this.arc(sx + r, ey - r, r, r2d * 90, r2d * 180, false);
  this.lineTo(sx, sy + r);
  this.arc(sx + r, sy + r, r, r2d * 180, r2d * 270, false);
  this.closePath();
};

CanvasRenderingContext2D.prototype.oval = function (centerX, centerY, width, height) {
  this.beginPath();
  this.moveTo(centerX, centerY - height / 2);
  this.bezierCurveTo(centerX + width / 2, centerY - height / 2, centerX + width / 2, centerY + height / 2, centerX, centerY + height / 2);
  this.bezierCurveTo(centerX - width / 2, centerY + height / 2, centerX - width / 2, centerY - height / 2, centerX, centerY - height / 2);
};

/***/ }),

/***/ 1:
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** multi ./public/game-6/js/jquery.js ./public/game-6/js/jquery-buzz.js ./public/game-6/js/game.js ./public/game-6/js/tools.js ./public/game-6/js/board.js ./public/game-6/js/paths.js ./public/game-6/js/bubbles.js ./public/game-6/js/fruits.js ./public/game-6/js/pacman.js ./public/game-6/js/ghosts.js ./public/game-6/js/home.js ./public/game-6/js/sound.js ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/jquery.js */"./public/game-6/js/jquery.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/jquery-buzz.js */"./public/game-6/js/jquery-buzz.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/game.js */"./public/game-6/js/game.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/tools.js */"./public/game-6/js/tools.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/board.js */"./public/game-6/js/board.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/paths.js */"./public/game-6/js/paths.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/bubbles.js */"./public/game-6/js/bubbles.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/fruits.js */"./public/game-6/js/fruits.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/pacman.js */"./public/game-6/js/pacman.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/ghosts.js */"./public/game-6/js/ghosts.js");
__webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/home.js */"./public/game-6/js/home.js");
module.exports = __webpack_require__(/*! /Users/ramijegham/Code/zitouna/zitounagaming/public/game-6/js/sound.js */"./public/game-6/js/sound.js");


/***/ })

/******/ });