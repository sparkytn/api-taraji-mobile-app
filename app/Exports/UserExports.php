<?php
namespace App\Exports;
use App\Http\Resources\UserExportResource;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class UserExports implements FromCollection
{
    public function collection()
    {
        return UserExportResource::collection(
            User::where('active',1)->get()
        )
            ->sortByDesc('user_score')
            ->values();
    }
}
