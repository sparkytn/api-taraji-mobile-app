<?php

namespace App\Models;

//use App\Http\Resources\UserResource;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Fan extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $guard = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'fb_id', 'fb_email', 'email', 'phone',  'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'fb_token','fb_email','email'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */



    public function photos(){
        return $this->hasMany('App\Models\Photo');
    }

//    public function getUserScoreAttribute(){
//        return (int) Play::where('user_id', $this->id)
//            ->where('active',1)
//            ->where('score','<',25)
//            ->select('score')
//            ->sum('score');
//    }
//
//    public function getRankAttribute(){
//
//        $ranks = Play::select('user_id')
//            ->where('active',1)
//            ->where('score','<',25)
//            ->selectRaw('SUM(`score`) TotalScore')
//            ->groupBy('user_id')
//            ->orderByDesc('TotalScore')
//            ->get();
//
//
//
//        return $ranks->search(function($ranks) {
//                return $ranks->user_id == $this->id;
//            }) + 1;
//    }



    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }
}
