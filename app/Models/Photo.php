<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Photo extends Model
{


    protected $fillable = [
        'fan_id', 'trophy', 'date', 'winner', 'score'
    ];

    public function fans(){
        return $this->belongsTo('App\Models\Fan');
    }
//
//    public static function getFirstTen()
//    {
//        $firstTen = collect(Play::with('users')
//            ->where('game_id', Game::getActiveGame()->id)
//            ->orderBy('plays.score')
//            ->get())->unique('user_id')->take(11);
//
//        return $firstTen->all();
//    }
//
//    public static function getActiveGameUsersScore()
//    {
//        $users = collect(Play::with('users')
//            ->where('game_id', Game::getActiveGame()->id)
//            ->where('winner', 1)
//            ->get());
//        return $users->unique('user_id')->all();
//    }
}
