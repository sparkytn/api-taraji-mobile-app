<?php

namespace App\Models;

use App\Http\Resources\UserResource;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $guard = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'phone',  'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function plays(){
        return $this->hasMany('App\Models\Play');
    }

    public function getUserScoreAttribute(){
        return (int) Play::where('user_id', $this->id)
                    ->where('active',1)
                    ->where('score','<',25)
                    ->select('score')
                    ->sum('score');
    }

    public function getRankAttribute(){

        $ranks = Play::select('user_id')
            ->where('active',1)
            ->where('score','<',25)
            ->selectRaw('SUM(`score`) TotalScore')
            ->groupBy('user_id')
            ->orderByDesc('TotalScore')
            ->get();



        return $ranks->search(function($ranks) {
                return $ranks->user_id == $this->id;
            }) + 1;
    }



    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }
}
