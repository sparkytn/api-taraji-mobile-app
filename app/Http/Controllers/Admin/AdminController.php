<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Play;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserExports;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $usersCount = User::all()->count();
        $playes = Play::where('active',1)->get()->count();

        $users = UserResource::collection(
            User::where('active',1)->get()
        )
            ->sortByDesc('user_score')
            ->values();

        $users = $this->paginate($users,50,null,[
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);

        return view('admin.home',compact(['usersCount','playes','users']));
    }


    /**
     * Gera a paginação dos itens de um array ou collection.
     *
     * @param array|Collection $items
     * @param int $perPage
     * @param int $page
     * @param array $options
     *
     * @return LengthAwarePaginator
     */
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);


        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


    public function users()
    {
        $users = User::paginate(50);

        return view('admin.users',compact('users'));
    }

    public function resetScore()
    {
        DB::table('plays')
            ->update(['active' => 0]);
        return redirect()->route('admin.dashboard');
    }


    /**
     * Export to excel
     */
    public function export()
    {
        return Excel::download(new UserExports, 'EFR-List-Score-'.now().'.xlsx');
    }
}
