<?php

namespace App\Http\Controllers;

use App\Models\Fan;
use App\Models\Referral;
use App\Services\TunisieTelApiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReferralController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{

		$validator =  Validator::make($request->all(), [
			'phone' => ['required', 'string', 'max:8', 'unique:referrals,phone','unique:fans,phone','regex:/^[0-9]{8}$/'],
			'trophy' => ['required', 'string', 'max:2'],
		]);

		$validator->setAttributeNames([
				'phone'                     => 'Téléphone',
				'trophy'                    => 'Trophée',
		]);

		if( $validator->fails() ){
			return response()->json([
					'success' => false,
					'code' => 'form_validation',
					'message'=> implode('<br>', $validator->errors()->all()),
			]);
		}

		$api_response = TunisieTelApiService::check($request->phone);

		if( ! $api_response['success'] ){
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'api_service_call_error',
					'message' => 'Api Server Error'
			], 403);
		}

		if( $api_response['api_code'] != 0 || $api_response['api_code'] != '0'){
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'referral_phone_error',
					'message' => 'Server Error'
			], 403);
		}


		if( $api_response['api_code'] != 0 || $api_response['api_code'] != '0'){
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'referral_phone_error',
					'message' => 'Server Error'
			], 403);
		}


		if( $api_response['data']['responseMessage'] != 'OK'
				&&  $api_response['data']['isTaraji'] != 1
				&&  $api_response['data']['isFilleul'] != 1
		){
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'not_taraji_mobile',
					'message' => 'not taraji'
			], 403);
		}

		$referral = new Referral();
		$referral->fan_id = auth()->user()->id;
		$referral->phone = $request->phone;
		$referral->trophy = $request->trophy;

		if(!$referral->save()){
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'saving_referral_server_error',
					'message' => 'Server Error'
			], 500);
		}

		return response()->json([
				'success' => true,
				'api_response`' => $api_response,
				'code' => 'subscribed',
				'message' => 'Vous venez d\'activé le trophé'
		], 200);
	}
}
