<?php
//
//namespace App\Http\Controllers;
//use App\Models\User;
//use Illuminate\Http\Request;
//use Carbon\Carbon;
//use Illuminate\Support\Facades\Auth;
//use Jenssegers\Agent\Agent;
//use Illuminate\Support\Facades\Validator;
//use Maatwebsite\Excel\Facades\Excel;
//use App\Exports\UserExports;
//use Thumbnail;
//
//use App\Models\Game;
//use App\Models\Play;
//use App\Models\Vote;
//use App\Models\Photo;
//
//class GameController extends Controller
//{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//
//    }
//
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */
//    public function index(Request $request)
//    {
//
//        $game = Game::getActiveGame(1);
//        $playCount = Play::all()->count();
//        $agent = new Agent();
//
//        return view('nextGame')->with([
//            'game'       =>  $game,
//            'playsCount' => $playCount,
//            'agent'      => $agent
//        ]);
//
//    }
//
//    public function closeGame()
//    {
//        $game = Game::getActiveGame(1);
//        return view('closeGame')->with([
//            'game_id' => $game->id
//        ]);
//
//    }
//
//    public function game7()
//    {
//        $game = Game::getActiveGame(1);
//        return view('nextGame')->with([
//            'game_id' => $game->id
//        ]);
//
//    }
//
//    public function NextActiveGame(Request $request)
//    {
//        $game = Game::getActiveGame(1);
//        $date = new Carbon;
//        $winner = false;
//
//        if( $game->end_date < $date ){
//            return view('closeGame')->with([
//                'game_id' => $game->id
//            ]);
//        }
//
//        $canPlay= true;
//
//        $attempt = Play::where('user_id',auth()->user()->id)
//            ->where('game_id', $game->id )
//            ->where('winner', 0)
//            ->whereDate('updated_at','>',Carbon::yesterday())
//            ->whereDate('updated_at','<=',Carbon::today())
//            ->get()
//            ->count();
//
//        if( $attempt >= 5) {
//            $canPlay = false;
//        }
//
//        if( Play::where('user_id',auth()->user()->id)
//            ->where('winner',1)
//            ->where('game_id', $game->id )
//            ->whereDate('updated_at','>',Carbon::yesterday())
//            ->whereDate('updated_at','<=',Carbon::today())
//            ->get()->count() >= 1 ) {
//            $canPlay = false;
//            $winner = true;
//
//        }
//
//
//        $playCount = User::all()->count();
////        $agent = new Agent();
//
//        return view('nextGame')->with([
//            'game'       =>  $game,
//            'playsCount' => $playCount,
////            'agent'      => $agent,
//            'attempt' => $attempt - 5,
//            'canPlay' => $canPlay,
//            'winner' => $winner
//        ]);
//
//    }
//
//
//
//    public function saveScore(Request $request)
//    {
//
//        $score =( $request->score > 0 ) ? $request->score : 0 ;
//
//        return response()->json([
//            'success' => true,
//            'message' => 'Votre score (<b> '.$score.' </b>) est enregistré. Bon Courage',
//            'score' => $score
//        ]);
//
//        $game = Game::firstWhere('active', 1);
//
//        $play = new Play();
//        $play->score = $request->get('score');
//        $play->game_id = $game->id;
//        $play->date = Carbon::now();
//        $play->user_id = auth()->user()->id;
//
//
//        return response()->json([
//            'success' => false,
//            'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement .'
//        ]);
//
//    }
//
//    public function uploadPhoto (Request $request){
//
//        if( Photo::where('user_id',Auth::user()->id)->where('status','approved')->count()  != 0 || Photo::where('user_id',Auth::user()->id)->where('status','pending')->count() != 0){
//            return response()->json([
//                'success' => false,
//                'code' => 104,
//                'message' => 'Vous avez déjà participé, vous ne pouvez pas changer de photo.'
//            ]);
//        }
//
//        $validator = Validator::make($request->all(),[
//            'image' => 'required|mimes:jpeg,png,gif|max:4096',
//        ]);
//
//        $validator->setAttributeNames([
//            'image'                         => 'Image',
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json([
//                'success' => false,
//                'code' => 102,
//                'message' => implode('<br>', $validator->errors()->all())
//            ]);
//        }
//        if( Photo::where('user_id',Auth::user()->id)->where('status','rejected')->count()  != 0){
//            $photo = Photo::where('user_id',Auth::user()->id)->where('status','rejected')->first();
//        }else{
//            $photo = new Photo;
//            $photo->user_id   = Auth::user()->id;
//        }
//        $photo->status  = 'pending';
//
//        $photoDir = storage_path('app/public/participants/'.Auth::user()->id.'/');
//        if (!\File::isDirectory($photoDir)) {
//            \File::makeDirectory($photoDir, 755, true);
//        }
//
//
//        $photo_file = $request->file('image');
//        $photo_file_Ext = $photo_file->guessExtension();
//        $photo_file_name = 'photo___'. Auth::user()->id . '.' . $photo_file_Ext;
//
//        $photo_file->move(
//            $photoDir,
//            $photo_file_name
//        );
//
//        $photo->path = Auth::user()->id.'/'.$photo_file_name;
//
//        if ( !$photo->save() ) {
//            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
//        }
//
//        return response()->json([
//            'success'   => true,
//            'message'   => "Votre photo est en cours de validation. <br> Merci pour votre participation."
//        ]);
//    }
//
//    public function photos(){
//
//        $photos = Photo::where('status','approved')->get();
//        $userPhoto = Photo::where('user_id',Auth::user()->id)->first();
//        return view('listing-photos')->with([
//            'photos'      => $photos,
//            'userPhoto'      => $userPhoto,
//        ]);
//
//    }
//
//    public function videos(){
//
//
//
//        $videos = Photo::where('status','approved')->where('game_id',Game::where('active',1)->first()->id)->get();
//        $userPhoto = Photo::where('user_id',Auth::user()->id)->where('game_id',Game::where('active',1)->first()->id)->first();
//        return view('listing-photos')->with([
//            'videos'      => $videos,
//            'userPhoto'      => $userPhoto,
//        ]);
//
//    }
//
//    public function uploadVideo (Request $request){
//        $activeGame = Game::where('active',1)->first()->id;
//        if( Photo::where('user_id',Auth::user()->id)->where('status','approved')->where('game_id',$activeGame)->count()  != 0 || Photo::where('user_id',Auth::user()->id)->where('status','pending')->where('game_id',$activeGame)->count() != 0){
//            return response()->json([
//                'success' => false,
//                'code' => 104,
//                'message' => 'Vous avez déjà participé !'
//            ]);
//        }
//
//        $validator = Validator::make($request->all(),[
//            'video' => 'required|mimetypes:video/mp4,video/x-flv,video/x-msvideo,video/avi,video/mpeg,video/quicktime,video/MP2T,application/x-mpegURL,video/3gpp|max:90000',
//        ]);
//
//        $validator->setAttributeNames([
//            'video'                         => 'Vidéo',
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json([
//                'success' => false,
//                'code' => 102,
//                'message' => implode('<br>', $validator->errors()->all())
//            ]);
//        }
//        if( Photo::where('user_id',Auth::user()->id)->where('status','rejected')->count()  != 0){
//            $video = Photo::where('user_id',Auth::user()->id)->where('status','rejected')->first();
//        }else{
//            $video = new Photo;
//            $video->user_id   = Auth::user()->id;
//            $video->game_id   = Game::where('active',1)->first()->id;
//        }
//        $video->status  = 'pending';
//
//        $videoDir = storage_path('app/public/participants/'.Auth::user()->id.'/');
//        if (!\File::isDirectory($videoDir)) {
//            \File::makeDirectory($videoDir, 755, true);
//        }
//
//
//        $video_file = $request->file('video');
//        $video_file_Ext = $video_file->guessExtension();
//        $video_file_name = 'tiktok_'. Auth::user()->id . '.' . $video_file_Ext;
//
//        $video_file->move(
//            $videoDir,
//            $video_file_name
//        );
//
//        $video->path = Auth::user()->id.'/'.$video_file_name;
//
////        $thumbnail_path   = storage_path('app/public/participants/'.$participant->organism_email.'/');
//
//        $video_path       = $videoDir.'/'.$video_file_name;
//
//        // set thumbnail image name
//        $thumbnail_image  = "Thumb.jpg";
//
//        // get video length and process it
//        // assign the value to time_to_image (which will get screenshot of video at that specified seconds)
//        $time_to_image    =3;
//
//
//        Thumbnail::getThumbnail($video_path,$videoDir,$thumbnail_image,$time_to_image);
//
//        if ( !$video->save() ) {
//            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
//        }
//
//        return response()->json([
//            'success'   => true,
//            'message'   => "Votre vidéo est en cours de validation. <br> Merci pour votre participation."
//        ]);
//    }
//
//    public function photoDetails(Request $request){
//
//        $id = $request->input('id');
//        $photo= Photo::where('id', $id)->where('status','approved')->first();
//
//        $voteCount  = Vote::where('photo_id', $id )->count();
//
//        $votedFor = false;
//        if(session('user_id')){
//            $votedFor = Vote::where('photo_id', $id )->where('user_id',session('user_id'))->count();
//        }
//
//        if($photo) {
//            response()->json([
//                'success' => true,
//                'photo'      => $photo,
//                'voteCount' => $voteCount,
//                'votedFor'  => $votedFor
//            ]);
//
//        }
//        response()->json([
//            'success' => false
//        ]);
//    }
//
//
//    public function vote(Request $request){
//        $activGame = Game::where('active',1)->first()->id;
//
//        if (
//            Vote::where('user_id', Auth::user()->id )
//                ->where('photo_id', $request->input('vote') )
//                ->where('game_id', $activGame )
//                ->count() != 0
//        ) {
//            return response()->json([
//                'success' => true,
//                'already_voted' => true,
//                'message' => 'Vous avez déja voté pour ce projet !',
//            ]);
//        }
//
//        $vote = new Vote;
//        $vote->user_id     = Auth::user()->id;
//        $vote->photo_id    = $request->input('vote');
//        $vote->game_id     = $activGame;
//        $vote->ip_address  = $request->ip();
//
//        if(!$vote->save()){
//            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
//        }
//
//        return response()->json([
//            'success' => true,
//            'already_voted' => false,
//            'message' => 'Votre vote est pris en compte! Vous pouvez voter pour d\'autres images',
//        ]);
//    }
//
//
//
//
//    /**
//     * Show raking page.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */
//
//    public function ranking()
//    {
//        $game = Game::getActiveGame(1);
//
//        $photos = Play::getActiveGameUsersScore();
//
//        return view('ranking')->with([
//            'game' =>  $game,
//            'photos' => $photos
//        ]);
//    }
//
//
//    /**
//     * Export to excel
//     */
//    public function exportExcel()
//    {
//        return Excel::download(new UserExports, 'list.xlsx');
//    }
//
//    /**
//     * Export to excel
//     */
//    public function startGame()
//    {
//        $game = Game::firstWhere('active', 1);
//
//        $attempt = Play::where('user_id',auth()->user()->id)
//            ->where('game_id', $game->id )
//            ->where('winner', 0)
//            ->whereDate('updated_at','>',Carbon::yesterday())
//            ->whereDate('updated_at','<=',Carbon::today())
//            ->get()
//            ->count();
//
//
//        if( session()->has('play_id')){
//            if(Play::find(session('play_id'))->where('winner', null) != null){
//                return response()->json([
//                    'success' => true,
//                    'ui' => auth()->user()->id,
//                    'pi' => session('play_id'),
//                    'si' => session()->getId(),
//                    'attempt'=>  5 - $attempt
//                ]);
//            }
//        }
//
//        $play = new Play();
//        $play->score = 0;
//        $play->game_id = $game->id;
//        $play->date = Carbon::now();
//        $play->winner = null;
//        $play->user_id = auth()->user()->id;
//
//        if(!$play->save()){
//            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
//        }
//
//        session([ 'play_id' => $play->id ]);
//
//        return response()->json([
//            'success' => true,
//            'ui' => auth()->user()->id,
//            'pi' => $play->id,
//            'si' => session()->getId(),
//            'attempt'=>  5 - $attempt
//        ]);
//    }
//
//
//    public function endGame(Request $request)
//    {
//        if(!session('play_id')){
//            return 'not Session';
//        }
//        if( session('play_id') != $request->input('pi') || session()->getId() !=  $request->input('si')){
//            return 'oupps';
//        }
//
//        $play = Play::find(intval(session('play_id')));
//        $play->winner = $request->input('o');
//        $play->score = $request->input('SCORE');
//
//        if(!$play->save()){
//            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
//        }
//
//        session()->forget('play_id');
//
//        if($request->input('o') == 0){
//            return response()->json([
//                'success' => true,
//                'winner' => false,
//            ]);
//        }
//
//        return response()->json([
//            'success' => true,
//            'winner' => true,
//        ]);
//
//
//    }
//
//
//    /**
//     * Return rank of First Ten
//     *
//     * @return Json
//     */
//
//    public function getFistTen()
//    {
//        return response()->json([
//            'success' => true,
//            'firstTen' => User::getFirstTen()
//        ]);
//    }
//
//
//    /**
//     * Save Auth User score
//     *
//     * @return Json
//     */
//
//    public function linkSocialProfiles(Request $request)
//    {
//
//        $user = User::firstWhere('id', auth()->user()->id);
//
//        $user->facebook = $request->get('facebook');
//        $user->instagram = $request->get('instagram');
//
//        if($user->save()){
//            return response()->json([
//                'success' => true,
//                'message' => 'Votre participation a été enregistrée avec succès.<br>Votre nom est inscrit dans le tirage au sort du 2ème Jeu. <br>   Bonne chance',
//            ]);
//        }
//
//        return response()->json([
//            'success' => false,
//            'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement .'
//        ]);
//
//    }
//
//}
