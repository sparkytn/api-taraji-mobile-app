<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PhotoController extends Controller
{
	public function store(Request $request)
	{
		// Get the last submitted record
		$lastSubmission = Photo::where('fan_id',auth()->user()->id)
				->where('trophy', $request->trophy)
				->orderByDesc('id')->first();
		// Check if the last submission is less than 3 minutes ago

		if ($lastSubmission && $lastSubmission->created_at->diffInMinutes(Carbon::now()) < 3) {
			return response()->json([
					'success' => false,
					'message'=> 'multiple_submissions',
					'trophy' => $request->trophy,
					'image' => Storage::disk('public')->url($lastSubmission->photo_path),
			],403);
		}



		$validator = Validator::make($request->all(),
		[
				'photo' => 'required',
				'trophy' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json([
					'success' => false,
					'message'=> implode('<br>', $validator->errors()->all()),
			]);
		}

//		$image_64 = $request->photo; //your base64 encoded data
//		$extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
//		$replace = substr($image_64, 0, strpos($image_64, ',')+1);
//
//		$image = str_replace($replace, '', $image_64);
//		$image = str_replace(' ', '+', $image);



		$uploadFolder = 'photos/'. auth()->user()->id;
		$photo = $request->file('photo') ;

		$path = $photo->store($uploadFolder,'public');

		$photo = new Photo();
		$photo->fan_id = auth()->user()->id;
		$photo->photo_path = $path;
		$photo->trophy = $request->trophy;

		if(!$photo->save()){
			return response()->json([
					'success' => false,
					'message' => 'Server Error'
			], 500);
		}

		return response()->json([
				'success' => true,
				'message' => 'File Uploaded Successfully',
				'photo_url' => Storage::disk('public')->url($path),
				'photo_id' => $photo->id,
		]);

	}



	public function confirmShare(Request $request)
	{
		$validator = Validator::make($request->all(),
				[
						'photo_id' => 'required',
						'fan_id' => 'required',
				]);


		if ($validator->fails()) {
			return response()->json([
					'success' => false,
					'message'=> implode('<br>', $validator->errors()->all()),
			]);
		}


		try {
			$photo = Photo::findOrFail($request->photo_id);
		} catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return response([
					'success' => false,
					'message' => '404 not found'
			], 404);
		}


		if(auth()->user()->id != $request->fan_id && $photo->id !=  $request->photo_id ){
			return response()->json([
					'success' => false,
					'message' => 'Not Authorised'
			], 401);
		}

		$photo->shared = true;

		if(!$photo->save()){
			return response()->json([
					'success' => false,
					'message' => 'Server Error'
			], 500);
		}

		return response()->json([
				'success' => true,
				'message' => 'Merci pour le partage',
		]);
	}
}
