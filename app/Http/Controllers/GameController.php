<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\Play;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class GameController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = UserResource::collection(
            User::where('active',1)->get()
        )
            ->sortByDesc('user_score')
            ->values()
            ->take(5);

        return response()->json([
                'users' => $users
            ]
            , 200);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //
        $play = new Play();
        $play->score = 0;
        $play->game_id = 1;
        $play->user_agent = $request->header('User-Agent');
        $play->date = Carbon::now();
        $play->user_id = auth()->user()->id;

        if(!$play->save()){
            return response()->json([
                'success' => false,
                'message' => 'Server Error'
            ], 500);
        }


        return response()->json([
            'success' => true,
            'message' => 'Début du jeu',
            'play_id' => $play->id
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $dataRaw = explode(".",auth()->tokenById(1));
        $dataRaw =  base64_decode ($dataRaw[1]);
        $dataRaw = json_decode($dataRaw);
        dd($dataRaw);
//        if($dataRaw->exp < $now){
            return now()  ." No_Need " . Carbon::parse($dataRaw->exp) ;
//        }

        return response()->json(
            new UserResource( auth()->user() )
        , 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $play = Play::findOrFail($id);

        if( $play->user_id !== auth()->user()->id){
            return response()->json([
                'success' => false,
                'message' => 'Not Yours '
            ], 401);
        }

        if(! $play){
            return response()->json([
                'success' => false,
                'message' => 'Not Found'
            ], 404);
        }

        if( $play->score != 0){
            return response()->json([
                'success' => false,
                'message' => 'Score déjà enregistré !'
            ], 200);
        }

        if($request->input('score')>25){
            $play->winner = 8;
            User::where( 'id', $play->user_id )->update([
                'active'=>0,
                'api_token'=> 'cheated',
                ]);
        }

        $play->score = $request->input('score');
        $play->user_agent = $request->header('User-Agent');


        if(!$play->save()){
            return response()->json([
                'success' => false,
                'message' => 'Server Error'
            ], 500);
        }


        return response()->json([
            'success' => true,
            'message' => 'Score enregistré'
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
