<?php

namespace App\Http\Controllers;

use App\Services\TunisieTelApiService;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use App\Models\Fan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
	public $loginAfterSignUp = true;


	public function checkRegistredUser(Request $request)
	{

		$validator = Validator::make($request->only(['fb_id','fb_email']), [
				'fb_email' => 'required|email',
				'fb_id' => 'required',
		]);


		$validator->setAttributeNames([
				'fb_id' => 'ID Facebook',
				'fb_email' => 'Email de Facebook',
		]);


		if ($validator->fails()) {
			return response()->json([
					'success' => false,
					'code' => 'form_validation',
					'message' => implode('<br>', $validator->errors()->all()),
			], 403);
		}

		$fan = Fan::where('fb_id', '=', $request->only(['fb_id']))
				->where('fb_email', '=', $request->only(['fb_email']))
				->first();

		if ($fan) {
			return $this->login($request);
		} else {
			return response()->json([
					'registered' => false,
					'code' => 'not_registered',
			]);
		}


	}
	public function checkPhone(Request $request)
	{

		$validator = Validator::make($request->only(['phone']), [
				'phone' => 'required'
		]);


		$validator->setAttributeNames([
				'phone' => 'Numéro de Téléphone',
		]);


		if ($validator->fails()) {
			return response()->json([
					'success' => false,
					'code' => 'form_validation',
					'message' => implode('<br>', $validator->errors()->all()),
			], 403);
		}


		$api_response = TunisieTelApiService::check($request->phone);

		if (!$api_response['success']) {
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'api_service_call_error',
					'message' => 'Api Server Error'
			], 403);
		}

		if ($api_response['api_code'] == 0 || $api_response['api_code'] == '0') {
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'user_phone_error',
					'message' => 'Server Error'
			], 403);
		}


		return response()->json([
				'success' => true,
				'code' => 'check_api_resp',
				'api_response' => $api_response,
		]);



	}


//
//
//	public function register_v2(Request $request)
//	{
//		$validator = Validator::make(
//				$request->all(), [
//				'full_name' => ['required', 'string', 'max:190'],
//				'phone' => ['required', 'string', 'max:8', 'unique:fans', 'regex:/^[0-9]{8}$/'],
//				'fb_email' => 'required|max:255|unique:fans,fb_email',
//				'fb_id' => ['required', 'string', 'unique:fans'],
//		]);
//
//
//		$validator->setAttributeNames(
//				[
//						'full_name' => 'Nom et prénom',
//						'phone' => 'Téléphone',
//						'fb_id' => 'ID Facebook',
//						'fb_email' => 'Email de Facebook',
//				]);
//
//		if ($validator->fails()) {
//			return response()->json(
//					[
//							'success' => false,
//							'message' => implode('<br>', $validator->errors()->all()),
//					]);
//		}
//
//
//		// ToDo : Replace with TT api to verify Phone number.
//		if (!$this->isTarajiMobile($request->phone)) {
//			return response()->json(
//					[
//							'success' => false,
//							'taraji_mobile' => false,
//							'message' => 'Votre numéro n\'est pas Taraji mobile',
//					], 401);
//		}
//
//		$fbToken = $request->fb_token;
//
//
//		$fbUser = Socialite::driver('facebook')->userFromToken($fbToken);
//
//		$fan = Fan::where('fb_id', $fbUser->getId())->first();
//
//		if (!$fan) {
//			$fan = Fan::create(
//					[
//							'full_name' => $request->full_name,
//							'email' => $request->fb_email,
//							'fb_email' => $request->fb_email,
//							'phone' => $request->phone,
//							'fb_id' => $request->fb_id,
//					]);
//		}
//
//		try {
//			if (!$fanToken = JWTAuth::fromUser($fan)) {
//				return response()->json(
//						[
//								'success' => false,
//								'message' => 'Unauthorized'
//						], 401);
//			}
//		} catch (JWTException $e) {
//			return response()->json(
//					[
//							'success' => false,
//							'message' => 'could_not_create_token',
//					], 500);
//		}
//
//		return $this->respondWithToken($fanToken);
//	}
//


	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
				'full_name' => ['required', 'string', 'max:190'],
				'phone' => ['required', 'string', 'max:8', 'unique:fans', 'regex:/^[0-9]{8}$/'],
				'fb_email' => 'required|max:255|unique:fans,fb_email',
				'fb_id' => ['required', 'string', 'unique:fans'],
		]);


		$validator->setAttributeNames([
				'full_name' => 'Nom et prénom',
				'phone' => 'Téléphone',
				'fb_id' => 'ID Facebook',
				'fb_email' => 'Email de Facebook',
		]);


		if ($validator->fails()) {
			return response()->json([
					'success' => false,
					'code' => 'form_validation',
					'message' => implode('<br>', $validator->errors()->all()),
			], 403);
		}


		$api_response = TunisieTelApiService::check($request->phone);

		if (!$api_response['success']) {
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'api_service_call_error',
					'message' => 'Api Server Error'
			], 403);
		}

		if ($api_response['api_code'] == 0 || $api_response['api_code'] == '0') {
			return response()->json([
					'success' => false,
					'api_response`' => $api_response,
					'code' => 'user_phone_error',
					'message' => 'Server Error'
			], 403);
		}

		$fan = Fan::create([
				'full_name' => $request->full_name,
				'email' => $request->fb_email,
				'fb_email' => $request->fb_email,
				'phone' => $request->phone,
				'fb_id' => $request->fb_id,
		]);

		try {
			if (!$fanToken = JWTAuth::fromUser($fan)) {
				return response()->json([
						'success' => false,
						'code' => 'jwt_error',
						'message' => 'Unauthorized'
				], 401);
			}
		} catch (JWTException $e) {
			return response()->json([
					'success' => false,
					'code' => 'could_not_create_token',
					'message' => 'could_not_create_token',
			], 500);
		}

		return $this->respondWithToken($fanToken,$api_response);
	}

	public function login(Request $request)
	{

		$fan = Fan::where('fb_id', '=', $request->only(['fb_id']))
				->where('fb_email', '=', $request->only(['fb_email']))
				->first();


		if (!$fan) {
			return response()->json([
					'success' => false,
					'code' => 'not_found',
					'message' => 'not_found',
			], 404);
		}

		try {
			// verify the credentials and create a token for the user
			if (!$fanToken = JWTAuth::fromUser($fan)) {
				return response()->json([
						'success' => false,
						'code' => 'unauthorized',
						'message' => 'User Unauthorized login'
				], 401);
			}
		} catch (JWTException $e) {
			// something went wrong
			return response()->json([
					'success' => false,
					'code' => 'could_not_create_token',
					'message' => 'could_not_create_token',
			], 500);
		}

		return $this->respondWithToken($fanToken,false);
	}


	public function getAuthUser(Request $request)
	{
		return response()->json(auth('api')->user());
	}


	public function logout()
	{
		auth()->logout();
		return response()->json(['message' => 'Successfully logged out', 'code' => 'logged_out',]);
	}


	protected function respondWithToken($token,$api_response)
	{
		return response()->json([
				'success' => true,
				'code' => 'logged_in',
				'access_token' => $token,
				'token_type' => 'bearer',
				'api_response' => $api_response,
				'expires_in' => null,
				'user' => auth('api')->user()
		]);
	}

}
