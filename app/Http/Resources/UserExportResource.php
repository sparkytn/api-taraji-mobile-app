<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserExportResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'lastname' => $this->lastname,
            'score'=>   $this->user_score,
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }
}
