<?php

namespace App\Http\Middleware;

use App\Exceptions\MissingOrInvalidApiToken;
use Closure;

class HasApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( $request->hasHeader('API-Secret') && $request->header('API-Secret') === config('app.api_secret') ){
            return $next($request);
        }

        throw  new MissingOrInvalidApiToken();
    }
}
