<?php

namespace App\Exceptions;

use Exception;

class MissingOrInvalidApiToken extends Exception
{
    public function __construct()
    {
//        parent::__construct(trans('invaid_api_token'), 403);
        parent::__construct('API token is missing or invalid', 403);
    }
}
