<?php

namespace App\Services;

use Exception;
use SoapClient;
use SoapFault;
use SoapHeader;
use SoapVar;

class TunisieTelApiService
{


	public static function check($phone)
	{
		$options = array(
				'Username' => 'taraji',
				'Password' => 'taraji_1909',
				'trace' => 1
		);

		try {
			$headerNS = 'http://docs.oasisopen.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

			//Create vars for username and password
			$usernameNode = new SoapVar('taraji', XSD_STRING, null, null, 'Username', $headerNS);
			$passwordNode = new SoapVar('taraji_1909', XSD_STRING, null, null, 'Password', $headerNS);

			// Create Username token node and add vars
			$UsernameTokenNode = new SoapVar([$usernameNode, $passwordNode], SOAP_ENC_OBJECT, null, null, 'UsernameToken', $headerNS);

			// Create security node
			$securityNode = new SoapVar([$UsernameTokenNode], SOAP_ENC_OBJECT, null, null, 'Security', $headerNS);

			// Now create a header with all above data
			$header = [new SoapHeader($headerNS, 'Security', $securityNode, false)];

			// Create your \SoapClient and add header to client
			$client = new SoapClient('tttn2.taraji.wsdl', $options);
			$client->__setSoapHeaders($header);

			$response = $client->__soapCall('getSubscriberDetails', array('getSubscriberDetails' => ['msisdn' => $phone]), null);

			//$myXMLData = html_entity_decode($client->__getLastResponse ());
			$myXMLData = $client->__getLastResponse();

			$myXMLData = str_replace(["soap:", "tns:"], ["", ""], $myXMLData);


			$xml = simplexml_load_string($myXMLData);
			$responseArray = @json_decode(@json_encode($xml), 1) ;


		} catch (SoapFault $e) {
			// Handle SOAP Fault here
			return [
					'phone' => $phone,
					'status' => 'error',
					'success' => false,
					'code' => 'api-soap-fault',
					'message' => 'SOAP Fault: ' . $e->getMessage(),
			];
		} catch (Exception $e) {
			// Handle any other exceptions here
			return [
					'phone' => $phone,
					'status' => 'error',
					'success' => false,
					'code' => 'server-fault',
					'message' => 'Error: ' . $e->getMessage(),
			];
		}


		// Extracting the required data
		$subscriberDetails = $responseArray['Body']['getSubscriberDetailsResponse']['result'] ?? null;
		$api_code = $responseArray['Body']['getSubscriberDetailsResponse']['result']['responseCode'] ?? null;
		$api_message = $responseArray['Body']['getSubscriberDetailsResponse']['result']['responseCode'] ?? 'false';

		// This is where the successful return should be placed
		return [
				'phone' => $phone,
				'status' => $subscriberDetails ? 'success' : 'error',
				'api_code' => $api_code,
				'api_message' => $api_message,
				'success' => $subscriberDetails ? true : false,
				'code' => $subscriberDetails ? 'data-fetched-successfully' : 'no-data',
				'data' => $subscriberDetails,
		];


	}

	function object2array($object)
	{
		return ;
	}


}